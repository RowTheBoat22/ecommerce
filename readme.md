# Toko Phincon
![Showcase Mobile](assets/showcase.png)

## Demo
See the demo [here.](https://youtu.be/OoWGb3vEDJo)

## Build With
- [Kotlin](https://kotlinlang.org/), a modern statically typed programming language.
- [Kotlin Coroutines](https://kotlinlang.org/docs/coroutines-overview.html)
- [Flow](https://kotlinlang.org/docs/flow.html)
- [Jetpack Compose](https://developer.android.com/jetpack/compose), a modern toolkit for building native Android UI with Kotlin **(See compose branch, currently only on DetailFragment)**.
- [LiveData](https://developer.android.com/topic/libraries/architecture/livedata), an observable data holder.
- [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel), class for storing and manage UI-related data in a lifecycle conscious way.
- [Room](https://developer.android.com/training/data-storage/room), a persistence library that provides an abstraction layer over SQLite.
- [DataStore](https://developer.android.com/topic/libraries/architecture/datastore)
- [Hilt](https://developer.android.com/training/dependency-injection/hilt-android),  a dependency injection library for Android that reduces the boilerplate of doing manual dependency injection in your project that's built on top of [Dagger](https://dagger.dev/)
- [Retrofit](https://square.github.io/retrofit/), a type-safe HTTP client for Android and Java
- [OkHttp](https://square.github.io/okhttp/), an HTTP & HTTP/2 client for Android and Java applications
- [Gson](https://github.com/google/gson), a Java serialization/deserialization library to convert Java Objects into JSON and vice versa.
- [Glide](https://bumptech.github.io/glide/), a fast and efficient image loading library for Android.
- [Shimmer](https://github.com/facebook/shimmer-android), an Android library that provides an easy way to add a shimmer effect
- [Lottie](https://lottiefiles.com/blog/working-with-lottie/getting-started-with-lottie-animations-in-android-app), is a mobile library for Android and iOS that parses Adobe After Effects animations exported as json with Bodymovin and renders them natively on mobile
- Unit Test
- Firebase
   - [Analytics](https://firebase.google.com/docs/analytics)
   - [Crashlytic](https://firebase.google.com/docs/crashlytics)
   - [Remote Config](https://firebase.google.com/docs/remote-config)
   - [Cloud Messaging](https://firebase.google.com/docs/cloud-messaging)
- [Detekt](https://detekt.dev/), Static code analyzer for [Kotlin](https://kotlinlang.org/)