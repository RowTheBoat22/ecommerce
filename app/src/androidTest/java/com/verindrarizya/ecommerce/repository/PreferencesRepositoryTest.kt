package com.verindrarizya.ecommerce.repository

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStoreFile
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.verindrarizya.ecommerce.data.PreferencesRepository
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

private const val TEST_DATASTORE_NAME: String = "test_datastore"

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(AndroidJUnit4::class)
class PreferencesRepositoryTest {

    private val testContext: Context =
        InstrumentationRegistry.getInstrumentation().targetContext

    private val testCoroutineDispatcher = UnconfinedTestDispatcher()

    private val testCoroutineScope = TestScope(testCoroutineDispatcher + Job())

    private val testDataStore: DataStore<Preferences> =
        PreferenceDataStoreFactory.create(
            scope = testCoroutineScope,
            produceFile =
            { testContext.preferencesDataStoreFile(TEST_DATASTORE_NAME) }
        )

    private val preferencesRepository: PreferencesRepository = PreferencesRepository(testDataStore)

    @Before
    fun setup() {
        Dispatchers.setMain(testCoroutineDispatcher)
    }

    @Test
    fun userOnBoardAndVerify() = runTest {
        preferencesRepository.setOnBoarded()

        val isUserOnBoarded = preferencesRepository.isUserAlreadyOnBoard.first()

        assertEquals(true, isUserOnBoarded)
    }

    @Test
    fun setUsernameAndVerifyValue() = runTest {
        val username = "bambang"

        preferencesRepository.setUserName(username)

        val usernameValue = preferencesRepository.username.first()

        assertEquals(username, usernameValue)
    }

    @Test
    fun setUsernameAndVerify() = runTest {
        val username = "bambang"

        preferencesRepository.setUserName(username)

        val isUsernameSet = preferencesRepository.isUserNameAlreadySet.first()

        assertEquals(true, isUsernameSet)
    }

    @Test
    fun setAccessTokenAndVerifyValue() = runTest {
        val accessToken = "123"

        preferencesRepository.setAccessToken(accessToken)

        val accessTokenValue = preferencesRepository.accessToken.first()

        assertEquals(accessToken, accessTokenValue)
    }

    @Test
    fun setRefreshTokenAndVerifyValue() = runTest {
        val refreshToken = "123"

        preferencesRepository.setRefreshToken(refreshToken)

        val refreshTokenValue = preferencesRepository.refreshToken.first()

        assertEquals(refreshToken, refreshTokenValue)
    }

    @Test
    fun setAccessTokenAndVerifyIsLogin() = runTest {
        val accessToken = "123"

        preferencesRepository.setAccessToken(accessToken)

        val isLoginValue = preferencesRepository.isLogIn.first()

        assertEquals(true, isLoginValue)
    }

    @Test
    fun clearDataAndVerifyUsernameCleared() = runTest {
        setData()

        preferencesRepository.clearData()

        val userNameValue = preferencesRepository.username.first()

        assertEquals(true, userNameValue.isBlank())
    }

    @Test
    fun clearDataAndVerifyAccessTokenCleared() = runTest {
        setData()

        preferencesRepository.clearData()

        val accessTokenValue = preferencesRepository.accessToken.first()

        assertEquals(true, accessTokenValue.isBlank())
    }

    @Test
    fun clearDataAndVerifyRefreshTokenCleared() = runTest {
        setData()

        preferencesRepository.clearData()

        val refreshTokenValue = preferencesRepository.refreshToken.first()

        assertEquals(true, refreshTokenValue.isBlank())
    }

    private suspend fun setData() {
        preferencesRepository.setAccessToken("123")
        preferencesRepository.setRefreshToken("123")
        preferencesRepository.setUserName("bambang")
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testCoroutineScope.runTest {
            testDataStore.edit { it.clear() }
        }
        testCoroutineScope.cancel()
    }

}