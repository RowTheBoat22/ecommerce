package com.verindrarizya.ecommerce.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.verindrarizya.ecommerce.data.local.StoreDatabase
import com.verindrarizya.ecommerce.data.local.dao.WishlistProductDao
import com.verindrarizya.ecommerce.data.local.entity.WishlistProductEntity
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class WishlistProductDaoTest {

    private lateinit var wishlistProductDao: WishlistProductDao
    private lateinit var storeDatabase: StoreDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        storeDatabase = Room.inMemoryDatabaseBuilder(
            context,
            StoreDatabase::class.java
        ).build()

        wishlistProductDao = storeDatabase.wishlistProductDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        storeDatabase.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertWishlistAndRead() = runTest {
        val wishlists = WishlistDummy.wishlists
        wishlists.forEach { wishlistProductDao.insert(it) }

        val wishlistEntities = wishlistProductDao.getAll().first()

        assertEquals(wishlists, wishlistEntities)
    }

    @Test
    @Throws(Exception::class)
    fun checkIsInWishlistAndVerify() = runTest {
        val wishlist = WishlistDummy.wishlists[0]

        wishlistProductDao.insert(wishlist)

        val isInWishlist = wishlistProductDao.isInWishlist(wishlist.productId).first()

        assertEquals(true, isInWishlist)
    }

    @Test
    @Throws(Exception::class)
    fun deleteWishlistAndVerify() = runTest {
        val wishlists = WishlistDummy.wishlists
        wishlists.forEach { wishlistProductDao.insert(it) }

        val deletedWishlist = wishlists[0]
        wishlistProductDao.delete(deletedWishlist)

        val wishlistEntities = wishlistProductDao.getAll().first()

        val isItemExist = wishlistEntities.any { it.productId == deletedWishlist.productId }

        assertEquals(false, isItemExist)
    }

    @Test
    @Throws(Exception::class)
    fun deleteAllWishlistAndVerify() = runTest {
        val wishlists = WishlistDummy.wishlists
        wishlists.forEach { wishlistProductDao.insert(it) }

        wishlistProductDao.deleteAllRecords()

        val wishlistEntities = wishlistProductDao.getAll().first()

        assertEquals(0, wishlistEntities.size)
    }

    object WishlistDummy {
        val wishlists = (1..3).map {
            WishlistProductEntity(
                productId = it.toString(),
                image = "image $it",
                productName = "productName $it",
                productVariantName = "productVariantName $it",
                productPrice = it,
                store = "store $it",
                stock = it,
                productRating = it.toDouble(),
                sale = it
            )
        }
    }
}