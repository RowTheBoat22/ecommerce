package com.verindrarizya.ecommerce.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.verindrarizya.ecommerce.data.local.StoreDatabase
import com.verindrarizya.ecommerce.data.local.dao.CartProductDao
import com.verindrarizya.ecommerce.data.local.entity.CartProductEntity
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class CartProductDaoTest {

    private lateinit var cartProductDao: CartProductDao
    private lateinit var storeDatabase: StoreDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        storeDatabase = Room.inMemoryDatabaseBuilder(
            context,
            StoreDatabase::class.java
        ).build()

        cartProductDao = storeDatabase.cartProductDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        storeDatabase.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertAndRead() = runTest {
        val carts = CartProductDummy.carts
        carts.forEach { cartProductDao.insertToCart(it) }

        val cartEntities = cartProductDao.getCartProductById(carts[0].productId)

        assertEquals(carts[0], cartEntities)
    }

    @Test
    @Throws(Exception::class)
    fun updateAndRead() = runTest {
        val cart = CartProductDummy.carts[0]
        cartProductDao.insertToCart(cart)

        val updatedCart = cart.copy(isSelected = true)
        cartProductDao.updateProduct(updatedCart)

        val updatedCartEntity = cartProductDao.getCartProductById(updatedCart.productId)

        assertEquals(updatedCart, updatedCartEntity)
    }

    @Test
    @Throws(Exception::class)
    fun updateAllSelectedAndRead() = runTest {
        val carts = CartProductDummy.carts
        carts.forEach { cartProductDao.insertToCart(it) }

        cartProductDao.updateAllSelectedProduct(true)
        val cartEntities = cartProductDao.getAll().first()

        val isSelectedAll = cartEntities.all { it.isSelected }

        assertEquals(true, isSelectedAll)
    }

    @Test
    @Throws(Exception::class)
    fun deleteAndVerify() = runTest {
        val carts = CartProductDummy.carts
        carts.forEach { cartProductDao.insertToCart(it) }

        val cart = carts[0]
        cartProductDao.deleteCartProducts(cart)

        val cartEntities = cartProductDao.getAll().first()

        assertEquals(carts.size - 1, cartEntities.size)
    }

    @Test
    @Throws(Exception::class)
    fun deleteAllRecordsAndVerify() = runTest {
        val carts = CartProductDummy.carts
        carts.forEach { cartProductDao.insertToCart(it) }

        cartProductDao.deleteAllRecords()
        val cartEntities = cartProductDao.getAll().first()

        assertEquals(0, cartEntities.size)
    }

    object CartProductDummy {
        val carts = (1..3).map {
            CartProductEntity(
                productId = it.toString(),
                image = "image $it",
                productName = "productName $it",
                productVariantName = "productVariantName $it",
                stock = it,
                price = it,
            )
        }
    }
}