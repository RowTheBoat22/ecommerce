package com.verindrarizya.ecommerce.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.verindrarizya.ecommerce.data.local.StoreDatabase
import com.verindrarizya.ecommerce.data.local.dao.NotifDao
import com.verindrarizya.ecommerce.data.local.entity.NotificationEntity
import junit.framework.TestCase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class NotifDaoTest {

    private lateinit var notifDao: NotifDao
    private lateinit var storeDatabase: StoreDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        storeDatabase = Room.inMemoryDatabaseBuilder(
            context,
            StoreDatabase::class.java
        ).build()

        notifDao = storeDatabase.notifDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        storeDatabase.close()
    }


    @Test
    @Throws(Exception::class)
    fun getAll_returnAllNotification() = runTest {
        val notifs = NotifDummy.notifs

        notifs.forEach { notifDao.insert(it) }

        val allNotifs = notifDao.getAll().first()

        TestCase.assertEquals(allNotifs, notifs)
    }

    @Test
    @Throws(Exception::class)
    fun insertNotifAndReadInList() = runTest {
        val notif = NotifDummy.notifs[0]

        notifDao.insert(notif)

        val notifSize = notifDao.getAll().first().size

        TestCase.assertEquals(1, notifSize)
    }

    @Test
    @Throws(Exception::class)
    fun updateAndRead() = runTest {
        val notif = NotifDummy.notifs[0]
        notifDao.insert(notif)

        val updatedNotif = notif.copy(isRead = true)
        notifDao.update(updatedNotif)

        val notifEntity = notifDao.getAll().first()[0]

        TestCase.assertEquals(updatedNotif, notifEntity)
    }

    @Test
    @Throws(Exception::class)
    fun deleteAllAndVerify() = runTest {
        val notifs = NotifDummy.notifs
        notifs.forEach { notifDao.insert(it) }

        notifDao.deleteAllRecords()

        val notifEntities = notifDao.getAll().first()

        TestCase.assertEquals(0, notifEntities.size)
    }

    object NotifDummy {
        val notifs = (1..3).map {
            NotificationEntity(
                id = it,
                title = "title $it",
                desc = "desc $it",
                image = "image $it",
                time = "time $it",
                date = "date $it",
                type = "type $it"
            )
        }
    }
}