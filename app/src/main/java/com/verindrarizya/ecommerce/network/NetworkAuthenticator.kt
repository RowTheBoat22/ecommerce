package com.verindrarizya.ecommerce.network

import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.verindrarizya.ecommerce.BuildConfig
import com.verindrarizya.ecommerce.data.PreferencesRepository
import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.data.remote.request.RefreshRequest
import com.verindrarizya.ecommerce.data.remote.response.AuthResponse
import com.verindrarizya.ecommerce.di.BASE_URl
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class NetworkAuthenticator @Inject constructor(
    private val preferencesRepository: PreferencesRepository,
    private val chuckerInterceptor: ChuckerInterceptor
) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        val token = runBlocking { preferencesRepository.refreshToken.first() }
        return runBlocking {
            try {
                val newData = getNewToken(token)
                val accessToken = newData.data.accessToken
                val refreshToken = newData.data.refreshToken

                preferencesRepository.setAccessToken(accessToken)
                preferencesRepository.setRefreshToken(refreshToken)

                return@runBlocking response.request.newBuilder()
                    .header("Authorization", "Bearer $accessToken")
                    .build()
            } catch (e: Exception) {
                preferencesRepository.clearData()
                return@runBlocking null
            }
        }
    }

    private suspend fun getNewToken(refreshToken: String): AuthResponse {
        val interceptor = Interceptor.invoke {
            val originalRequest = it.request()
            val builder = originalRequest.newBuilder()
            builder.addHeader("API_KEY", BuildConfig.API_KEY)
            val newRequest = builder.build()

            it.proceed(newRequest)
        }

        val client = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(chuckerInterceptor)
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        val apiService = retrofit.create(ApiService::class.java)

        val refreshRequest = RefreshRequest(refreshToken)

        return apiService.refresh(refreshRequest)
    }
}
