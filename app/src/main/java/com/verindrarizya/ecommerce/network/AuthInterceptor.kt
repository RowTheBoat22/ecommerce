package com.verindrarizya.ecommerce.network

import com.verindrarizya.ecommerce.BuildConfig
import com.verindrarizya.ecommerce.data.PreferencesRepository
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(
    private val preferencesRepository: PreferencesRepository
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val builder = originalRequest.newBuilder()

        if (
            originalRequest.url.encodedPath == "/register" ||
            originalRequest.url.encodedPath == "/login" ||
            originalRequest.url.encodedPath == "/refresh"
        ) {
            builder.addHeader("Content-Type", "application/json")
            builder.addHeader("API_KEY", BuildConfig.API_KEY)
        } else {
            val accessToken = runBlocking { preferencesRepository.accessToken.first() }
            builder.addHeader("Authorization", "Bearer $accessToken")
        }

        val newRequest = builder.build()

        return chain.proceed(newRequest)
    }
}
