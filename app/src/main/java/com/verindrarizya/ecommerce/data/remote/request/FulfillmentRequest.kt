package com.verindrarizya.ecommerce.data.remote.request

import com.google.gson.annotations.SerializedName

data class FulfillmentRequest(

    @field:SerializedName("payment")
    val payment: String,

    @field:SerializedName("items")
    val items: List<FulfillmentItem>
)

data class FulfillmentItem(

    @field:SerializedName("quantity")
    val quantity: Int,

    @field:SerializedName("productId")
    val productId: String,

    @field:SerializedName("variantName")
    val variantName: String
)
