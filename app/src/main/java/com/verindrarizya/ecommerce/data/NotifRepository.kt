package com.verindrarizya.ecommerce.data

import com.verindrarizya.ecommerce.data.local.dao.NotifDao
import com.verindrarizya.ecommerce.data.local.entity.NotificationEntity
import javax.inject.Inject

class NotifRepository @Inject constructor(
    private val notifDao: NotifDao
) {

    val allNotification = notifDao.getAll()

    suspend fun updateNotif(notificationEntity: NotificationEntity) {
        notifDao.update(notificationEntity)
    }

    suspend fun deleteAllRecords() {
        notifDao.deleteAllRecords()
    }
}
