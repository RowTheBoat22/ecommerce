package com.verindrarizya.ecommerce.data

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.MutablePreferences
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

private val ON_BOARDING_KEY = booleanPreferencesKey("ON_BOARDING_KEY")
private val USER_NAME = stringPreferencesKey("USER_NAME")

private val ACCESS_TOKEN = stringPreferencesKey("ACCESS_TOKEN")
private val REFRESH_TOKEN = stringPreferencesKey("REFRESH_TOKEN")

private val DARK_MODE_KEY = booleanPreferencesKey("DARK_MODE_KEY")

class PreferencesRepository @Inject constructor(
    private val dataStore: DataStore<Preferences>
) {

    val isUserAlreadyOnBoard = dataStore.data.map { preferences ->
        preferences[ON_BOARDING_KEY] ?: false
    }

    val isUserNameAlreadySet = dataStore.data.map { preferences ->
        preferences[USER_NAME]?.isNotBlank() ?: false
    }

    val isLogIn = dataStore.data.map { preferences ->
        preferences[ACCESS_TOKEN]?.isNotBlank() ?: false
    }

    val accessToken = dataStore.data.map { preferences ->
        preferences[ACCESS_TOKEN] ?: ""
    }

    val refreshToken = dataStore.data.map { preferences ->
        preferences[REFRESH_TOKEN] ?: ""
    }

    val username = dataStore.data.map { preferences ->
        preferences[USER_NAME] ?: ""
    }

    val isDarkMode = dataStore.data.map { preferences ->
        preferences[DARK_MODE_KEY] ?: false
    }

    suspend fun toggleDarkMode(value: Boolean) {
        if (value == isDarkMode.first()) return

        dataStore.edit { preferences: MutablePreferences ->
            preferences[DARK_MODE_KEY] = value
        }
    }

    suspend fun setOnBoarded() {
        runBlocking {
            dataStore.edit { preferences: MutablePreferences ->
                preferences[ON_BOARDING_KEY] = true
            }
        }
    }

    suspend fun setUserName(name: String) {
        dataStore.edit { preferences: MutablePreferences ->
            preferences[USER_NAME] = name
        }
    }

    suspend fun setAccessToken(token: String) {
        dataStore.edit { preferences: MutablePreferences ->
            preferences[ACCESS_TOKEN] = token
        }
    }

    suspend fun setRefreshToken(token: String) {
        dataStore.edit { preferences: MutablePreferences ->
            preferences[REFRESH_TOKEN] = token
        }
    }

    suspend fun clearData() {
        dataStore.edit { preferences: MutablePreferences ->
            preferences.remove(ACCESS_TOKEN)
            preferences.remove(USER_NAME)
            preferences.remove(REFRESH_TOKEN)
        }
    }
}
