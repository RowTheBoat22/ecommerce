package com.verindrarizya.ecommerce.data.remote.request

import com.google.gson.annotations.SerializedName

data class RatingRequest(

    @field:SerializedName("review")
    val review: String? = null,

    @field:SerializedName("rating")
    val rating: Int? = null,

    @field:SerializedName("invoiceId")
    val invoiceId: String
)
