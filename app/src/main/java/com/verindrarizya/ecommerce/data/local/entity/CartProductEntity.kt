package com.verindrarizya.ecommerce.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * tableName = cart product
 *
 * column ->
 * productId: String
 * productName: String,
 * image: String
 * productVariantName: String,
 * stock: Int,
 * price: Int,
 * quantity: Int default value to 0,
 * is selected: Boolean
 */
@Entity(tableName = "cart_product")
data class CartProductEntity(
    @PrimaryKey val productId: String,
    @ColumnInfo("image") val image: String,
    @ColumnInfo("product_name") val productName: String,
    @ColumnInfo("product_variant_name") val productVariantName: String,
    @ColumnInfo("stock") val stock: Int,
    @ColumnInfo("price") val price: Int,
    @ColumnInfo(name = "quantity", defaultValue = "1") val quantity: Int = 1,
    @ColumnInfo("is_selected", defaultValue = "false") val isSelected: Boolean = false
)
