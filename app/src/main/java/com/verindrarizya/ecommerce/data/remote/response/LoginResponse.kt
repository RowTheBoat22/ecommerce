package com.verindrarizya.ecommerce.data.remote.response

import com.google.gson.annotations.SerializedName

data class LoginResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: LoginData,

    @field:SerializedName("message")
    val message: String
)

data class LoginData(

    @field:SerializedName("userImage")
    val userImage: String,

    @field:SerializedName("userName")
    val userName: String,

    @field:SerializedName("accessToken")
    val accessToken: String,

    @field:SerializedName("expiresAt")
    val expiresAt: Int,

    @field:SerializedName("refreshToken")
    val refreshToken: String
)
