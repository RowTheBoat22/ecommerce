package com.verindrarizya.ecommerce.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.data.remote.response.Product

class ProductPagingSource(
    private val apiService: ApiService,
    private val search: String?,
    private val brand: String?,
    private val lowest: Int?,
    private val highest: Int?,
    private val sort: String?,
) : PagingSource<Int, Product>() {
    override fun getRefreshKey(state: PagingState<Int, Product>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Product> {
        try {
            val page = params.key ?: 1

            val response = apiService.products(
                search = search,
                brand = brand,
                lowest = lowest,
                highest = highest,
                sort = sort,
                limit = params.loadSize,
                page = page
            )

            val prevKey = if (page > 1) page - 1 else null
            val nextKey = if (page < response.data.totalPages) page + 1 else null

            return LoadResult.Page(
                data = response.data.items,
                prevKey = prevKey,
                nextKey = nextKey
            )
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }
}
