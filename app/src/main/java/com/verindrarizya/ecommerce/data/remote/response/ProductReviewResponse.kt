package com.verindrarizya.ecommerce.data.remote.response

import com.google.gson.annotations.SerializedName

data class ProductReviewResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: List<ProductReview>,

    @field:SerializedName("message")
    val message: String
)

data class ProductReview(

    @field:SerializedName("userImage")
    val userImage: String,

    @field:SerializedName("userName")
    val userName: String,

    @field:SerializedName("userReview")
    val userReview: String?,

    @field:SerializedName("userRating")
    val userRating: Int?
)
