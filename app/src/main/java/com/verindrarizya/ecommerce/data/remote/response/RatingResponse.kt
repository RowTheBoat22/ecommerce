package com.verindrarizya.ecommerce.data.remote.response

import com.google.gson.annotations.SerializedName

data class RatingResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("message")
    val message: String
)
