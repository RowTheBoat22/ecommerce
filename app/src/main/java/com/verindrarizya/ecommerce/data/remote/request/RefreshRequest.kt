package com.verindrarizya.ecommerce.data.remote.request

import com.google.gson.annotations.SerializedName

data class RefreshRequest(
    @field:SerializedName("token")
    val token: String
)
