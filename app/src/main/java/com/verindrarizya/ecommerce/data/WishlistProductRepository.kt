package com.verindrarizya.ecommerce.data

import com.verindrarizya.ecommerce.data.local.dao.WishlistProductDao
import com.verindrarizya.ecommerce.data.local.entity.WishlistProductEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class WishlistProductRepository @Inject constructor(
    private val wishlistProductDao: WishlistProductDao
) {

    fun getAllWishlistProduct(): Flow<List<WishlistProductEntity>> {
        return wishlistProductDao.getAll()
    }

    fun checkIsInWishlist(productId: String): Flow<Boolean> {
        return wishlistProductDao.isInWishlist(productId)
    }

    suspend fun saveToWishlist(wishlistProductEntity: WishlistProductEntity) {
        wishlistProductDao.insert(wishlistProductEntity)
    }

    suspend fun deleteFromWishlist(wishlistProductEntity: WishlistProductEntity) {
        wishlistProductDao.delete(wishlistProductEntity)
    }

    suspend fun deleteAllRecords() {
        wishlistProductDao.deleteAllRecords()
    }
}
