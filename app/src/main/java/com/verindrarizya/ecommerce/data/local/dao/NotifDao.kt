package com.verindrarizya.ecommerce.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.verindrarizya.ecommerce.data.local.entity.NotificationEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NotifDao {

    @Query("SELECT * FROM notif")
    fun getAll(): Flow<List<NotificationEntity>>

    @Insert
    fun insert(notificationEntity: NotificationEntity)

    @Update
    suspend fun update(notificationEntity: NotificationEntity)

    @Query("DELETE FROM notif")
    suspend fun deleteAllRecords()
}
