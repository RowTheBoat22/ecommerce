package com.verindrarizya.ecommerce.data.remote.response

import com.google.gson.annotations.SerializedName
import com.verindrarizya.ecommerce.data.local.entity.CartProductEntity
import com.verindrarizya.ecommerce.data.local.entity.WishlistProductEntity

data class ProductDetailResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: ProductDetail,

    @field:SerializedName("message")
    val message: String
)

data class ProductVariantItem(

    @field:SerializedName("variantPrice")
    val variantPrice: Int,

    @field:SerializedName("variantName")
    val variantName: String
)

data class ProductDetail(

    @field:SerializedName("image")
    val image: List<String>,

    @field:SerializedName("productId")
    val productId: String,

    @field:SerializedName("description")
    val description: String,

    @field:SerializedName("totalRating")
    val totalRating: Int,

    @field:SerializedName("store")
    val store: String,

    @field:SerializedName("productName")
    val productName: String,

    @field:SerializedName("totalSatisfaction")
    val totalSatisfaction: Int,

    @field:SerializedName("sale")
    val sale: Int,

    @field:SerializedName("productVariant")
    val productVariant: List<ProductVariantItem>,

    @field:SerializedName("stock")
    val stock: Int,

    @field:SerializedName("productRating")
    val productRating: Double,

    @field:SerializedName("brand")
    val brand: String,

    @field:SerializedName("productPrice")
    val productPrice: Int,

    @field:SerializedName("totalReview")
    val totalReview: Int
)

fun ProductDetail.toWishlistProductEntity(): WishlistProductEntity =
    WishlistProductEntity(
        productId = productId,
        image = image[0],
        productName = productName,
        productPrice = productPrice,
        store = store,
        productRating = productRating,
        sale = sale,
        stock = stock,
        productVariantName = productVariant[0].variantName
    )

fun ProductDetail.toCartProductEntity(): CartProductEntity =
    CartProductEntity(
        productId = productId,
        image = image[0],
        productName = productName,
        productVariantName = productVariant[0].variantName,
        stock = stock,
        price = productPrice,
    )
