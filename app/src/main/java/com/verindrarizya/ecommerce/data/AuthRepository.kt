package com.verindrarizya.ecommerce.data

import com.google.firebase.messaging.FirebaseMessaging
import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.data.remote.request.UserRequest
import com.verindrarizya.ecommerce.data.remote.response.AuthResponse
import com.verindrarizya.ecommerce.data.remote.response.LoginResponse
import com.verindrarizya.ecommerce.data.remote.response.ProfileResponse
import com.verindrarizya.ecommerce.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.tasks.await
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val apiService: ApiService,
    private val firebaseMessaging: FirebaseMessaging
) {

    fun register(email: String, password: String): Flow<Resource<AuthResponse>> = flow {
        emit(Resource.Loading)

        try {
            val firebaseToken = firebaseMessaging.token.await()
            val userRequest = UserRequest(email, password, firebaseToken)
            val response = apiService.register(userRequest)
            emit(Resource.Success(data = response))
        } catch (e: Throwable) {
            emit(Resource.Failure(throwable = e))
        }
    }.flowOn(Dispatchers.IO)

    fun login(email: String, password: String): Flow<Resource<LoginResponse>> = flow {
        emit(Resource.Loading)

        try {
            val firebaseToken = firebaseMessaging.token.await()
            val userRequest = UserRequest(email, password, firebaseToken)
            val response = apiService.login(userRequest)
            emit(Resource.Success(data = response))
        } catch (e: Throwable) {
            emit(Resource.Failure(e))
        }
    }.flowOn(Dispatchers.IO)

    fun profile(
        userName: String,
        userImage: File?
    ): Flow<Resource<ProfileResponse>> = flow {
        emit(Resource.Loading)

        try {
            val usernameRequestBody = userName.toRequestBody("text/plain".toMediaTypeOrNull())

            var filePart: MultipartBody.Part? = null

            userImage?.let {
                val requestBody = it.asRequestBody("image/*".toMediaTypeOrNull())
                filePart = MultipartBody.Part.createFormData(
                    "userImage",
                    "profile.png",
                    requestBody
                )
            }

            val response = apiService.profile(
                usernameRequestBody,
                filePart
            )

            emit(Resource.Success(data = response))
        } catch (e: Throwable) {
            emit(Resource.Failure(e))
        }
    }.flowOn(Dispatchers.IO)
}
