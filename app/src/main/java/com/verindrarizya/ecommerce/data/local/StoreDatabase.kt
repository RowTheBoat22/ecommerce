package com.verindrarizya.ecommerce.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.verindrarizya.ecommerce.data.local.dao.CartProductDao
import com.verindrarizya.ecommerce.data.local.dao.NotifDao
import com.verindrarizya.ecommerce.data.local.dao.WishlistProductDao
import com.verindrarizya.ecommerce.data.local.entity.CartProductEntity
import com.verindrarizya.ecommerce.data.local.entity.NotificationEntity
import com.verindrarizya.ecommerce.data.local.entity.WishlistProductEntity

@Database(
    entities = [WishlistProductEntity::class, CartProductEntity::class, NotificationEntity::class],
    version = 1
)
abstract class StoreDatabase : RoomDatabase() {
    abstract fun wishlistProductDao(): WishlistProductDao

    abstract fun cartProductDao(): CartProductDao

    abstract fun notifDao(): NotifDao
}
