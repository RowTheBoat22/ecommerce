package com.verindrarizya.ecommerce.data.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.verindrarizya.ecommerce.data.local.entity.CartProductEntity
import kotlinx.coroutines.flow.Flow

/**
 *
 * CartProduct Specification
 * table name: cart_product
 *
 * 1. ekspos flow untuk mendapatkan semua product yang ada di cart
 * 2. ekspos flow untuk cek apakah semua record untuk field check is true
 * 3. ekspos flow untuk menghitung total harga dari field yang check is true
 * 4. suspend function untuk update field is check
 * 5. suspend function untuk delete product dari cart
 * 6. suspend function untuk update quantity dari product
 * 7. suspend function untuk delete semua produk dari table
 *
 * 4 5 6 bisa digabung
 */
@Dao
interface CartProductDao {

    @Query("SELECT * FROM cart_product")
    fun getAll(): Flow<List<CartProductEntity>>

    @Query("SELECT * FROM cart_product WHERE productId = :productId")
    suspend fun getCartProductById(productId: String): CartProductEntity

    /**
     * update quantity, update selected
     */
    @Update
    suspend fun updateProduct(cartProductEntity: CartProductEntity)

    @Query("UPDATE cart_product SET is_selected = :flag")
    suspend fun updateAllSelectedProduct(flag: Boolean)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertToCart(cartProductEntity: CartProductEntity): Long

    @Delete
    suspend fun deleteCartProducts(vararg cartProductEntity: CartProductEntity)

    @Query("DELETE FROM cart_product")
    suspend fun deleteAllRecords()
}
