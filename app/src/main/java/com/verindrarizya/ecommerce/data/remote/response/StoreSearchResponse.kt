package com.verindrarizya.ecommerce.data.remote.response

import com.google.gson.annotations.SerializedName

data class StoreSearchResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: List<String>,

    @field:SerializedName("message")
    val message: String
)
