package com.verindrarizya.ecommerce.data

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.gson.Gson
import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.data.remote.request.FulfillmentItem
import com.verindrarizya.ecommerce.data.remote.request.FulfillmentRequest
import com.verindrarizya.ecommerce.data.remote.response.PaymentSectionItem
import com.verindrarizya.ecommerce.data.remote.response.TransactionResponse
import com.verindrarizya.ecommerce.ui.main.model.Fulfillment
import com.verindrarizya.ecommerce.ui.main.model.Product
import com.verindrarizya.ecommerce.ui.main.model.toFulfillment
import com.verindrarizya.ecommerce.ui.main.model.toFulfillmentItem
import com.verindrarizya.ecommerce.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class PaymentRepository @Inject constructor(
    private val apiService: ApiService,
    private val firebaseRemoteConfig: FirebaseRemoteConfig
) {

    fun getPayment(): List<PaymentSectionItem> {
        val paymentJson = firebaseRemoteConfig.getString("payment")

        val gson = Gson()
        val paymentSectionItem: List<PaymentSectionItem> =
            gson.fromJson(paymentJson, Array<PaymentSectionItem>::class.java).toList()

        return paymentSectionItem
    }

    fun fulfillment(
        paymentMethod: String,
        checkoutProducts: List<Product>
    ): Flow<Resource<Fulfillment>> = flow {
        emit(Resource.Loading)

        val fulfillmentItems: List<FulfillmentItem> =
            checkoutProducts.map { it.toFulfillmentItem() }

        val fulfillmentRequest = FulfillmentRequest(
            payment = paymentMethod,
            items = fulfillmentItems
        )
        try {
            val fulfillmentResponse = apiService.fulfillment(fulfillmentRequest)
            val fulfillment = fulfillmentResponse.fulfillmentData.toFulfillment()
            emit(Resource.Success(fulfillment))
        } catch (e: Throwable) {
            emit(Resource.Failure(e))
        }
    }.flowOn(Dispatchers.IO)

    fun transactionHistory(): Flow<Resource<TransactionResponse>> = flow {
        emit(Resource.Loading)

        try {
            val transactionResponse = apiService.transaction()
            emit(Resource.Success(transactionResponse))
        } catch (e: Throwable) {
            emit(Resource.Failure(e))
        }
    }.flowOn(Dispatchers.IO)
}
