package com.verindrarizya.ecommerce.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "wishlist_product")
data class WishlistProductEntity(
    @PrimaryKey val productId: String,
    @ColumnInfo("image") val image: String,
    @ColumnInfo("product_name") val productName: String,
    @ColumnInfo("product_variant_name") val productVariantName: String,
    @ColumnInfo("product_price") val productPrice: Int,
    @ColumnInfo("store") val store: String,
    @ColumnInfo("stock") val stock: Int,
    @ColumnInfo("product_rating") val productRating: Double,
    @ColumnInfo("sale") val sale: Int,
)

fun WishlistProductEntity.toCartProduct() =
    CartProductEntity(
        productId = productId,
        image = image,
        productName = productName,
        productVariantName = productVariantName,
        price = productPrice,
        stock = stock
    )
