package com.verindrarizya.ecommerce.data.remote.response

import com.google.gson.annotations.SerializedName

data class FulfillmentResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val fulfillmentData: FulfillmentData,

    @field:SerializedName("message")
    val message: String
)

data class FulfillmentData(

    @field:SerializedName("date")
    val date: String,

    @field:SerializedName("total")
    val total: Int,

    @field:SerializedName("invoiceId")
    val invoiceId: String,

    @field:SerializedName("payment")
    val payment: String,

    @field:SerializedName("time")
    val time: String,

    @field:SerializedName("status")
    val status: Boolean
)
