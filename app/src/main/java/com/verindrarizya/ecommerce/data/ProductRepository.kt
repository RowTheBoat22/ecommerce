package com.verindrarizya.ecommerce.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.data.remote.request.RatingRequest
import com.verindrarizya.ecommerce.data.remote.response.Product
import com.verindrarizya.ecommerce.data.remote.response.ProductDetailResponse
import com.verindrarizya.ecommerce.data.remote.response.ProductReviewResponse
import com.verindrarizya.ecommerce.data.remote.response.RatingResponse
import com.verindrarizya.ecommerce.data.remote.response.StoreSearchResponse
import com.verindrarizya.ecommerce.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class ProductRepository @Inject constructor(
    private val apiService: ApiService
) {

    fun products(
        search: String?,
        brand: String?,
        lowest: Int?,
        highest: Int?,
        sort: String?
    ): Flow<PagingData<Product>> = Pager(
        PagingConfig(pageSize = 10, initialLoadSize = 10, prefetchDistance = 1)
    ) {
        ProductPagingSource(
            apiService = apiService,
            search = search,
            brand = brand,
            lowest = lowest,
            highest = highest,
            sort = sort
        )
    }.flow

    fun search(productName: String): Flow<Resource<StoreSearchResponse>> = flow {
        emit(Resource.Loading)

        try {
            val storeSearchResponse = apiService.search(productName)
            emit(Resource.Success(data = storeSearchResponse))
        } catch (e: Throwable) {
            emit(Resource.Failure(e))
        }
    }.flowOn(Dispatchers.IO)

    fun productDetail(id: String): Flow<Resource<ProductDetailResponse>> = flow {
        emit(Resource.Loading)

        try {
            val productDetailResponse = apiService.productDetail(id)
            emit(Resource.Success(data = productDetailResponse))
        } catch (e: Throwable) {
            emit(Resource.Failure(e))
        }
    }.flowOn(Dispatchers.IO)

    fun productReview(id: String): Flow<Resource<ProductReviewResponse>> = flow {
        emit(Resource.Loading)

        try {
            val productReviewResponse = apiService.productReview(id)
            emit(Resource.Success(data = productReviewResponse))
        } catch (e: Throwable) {
            emit(Resource.Failure(e))
        }
    }.flowOn(Dispatchers.IO)

    fun rating(
        invoiceId: String,
        rating: Int,
        review: String,
    ): Flow<Resource<RatingResponse>> = flow {
        emit(Resource.Loading)

        try {
            val ratingBody = if (rating == 0) null else rating

            // beda antara blank dan empty
            // jika ada string dengan karakter spasi "  "
            // isEmpty -> false, isBlank -> true
            val reviewBody: String? = review.ifBlank { null }
            val ratingRequest = RatingRequest(
                invoiceId = invoiceId,
                rating = ratingBody,
                review = reviewBody
            )

            val ratingResponse = apiService.rating(ratingRequest)
            emit(Resource.Success(ratingResponse))
        } catch (e: Throwable) {
            emit(Resource.Failure(e))
        }
    }.flowOn(Dispatchers.IO)
}
