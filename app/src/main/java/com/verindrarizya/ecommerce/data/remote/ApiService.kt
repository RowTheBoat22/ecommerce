package com.verindrarizya.ecommerce.data.remote

import com.verindrarizya.ecommerce.data.remote.request.FulfillmentRequest
import com.verindrarizya.ecommerce.data.remote.request.RatingRequest
import com.verindrarizya.ecommerce.data.remote.request.RefreshRequest
import com.verindrarizya.ecommerce.data.remote.request.UserRequest
import com.verindrarizya.ecommerce.data.remote.response.AuthResponse
import com.verindrarizya.ecommerce.data.remote.response.FulfillmentResponse
import com.verindrarizya.ecommerce.data.remote.response.LoginResponse
import com.verindrarizya.ecommerce.data.remote.response.ProductDetailResponse
import com.verindrarizya.ecommerce.data.remote.response.ProductReviewResponse
import com.verindrarizya.ecommerce.data.remote.response.ProductsResponse
import com.verindrarizya.ecommerce.data.remote.response.ProfileResponse
import com.verindrarizya.ecommerce.data.remote.response.RatingResponse
import com.verindrarizya.ecommerce.data.remote.response.StoreSearchResponse
import com.verindrarizya.ecommerce.data.remote.response.TransactionResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @POST("register")
    suspend fun register(@Body user: UserRequest): AuthResponse

    @POST("login")
    suspend fun login(@Body user: UserRequest): LoginResponse

    @Multipart
    @POST("profile")
    suspend fun profile(
        @Part("userName") userName: RequestBody,
        @Part userImage: MultipartBody.Part?
    ): ProfileResponse

    @POST("refresh")
    suspend fun refresh(
        @Body request: RefreshRequest
    ): AuthResponse

    @POST("search")
    suspend fun search(
        @Query("query") query: String
    ): StoreSearchResponse

    @POST("products")
    suspend fun products(
        @Query("search") search: String? = null,
        @Query("brand") brand: String? = null,
        @Query("lowest") lowest: Int? = null,
        @Query("highest") highest: Int? = null,
        @Query("sort") sort: String? = null,
        @Query("limit") limit: Int,
        @Query("page") page: Int
    ): ProductsResponse

    @GET("products/{id}")
    suspend fun productDetail(@Path("id") id: String): ProductDetailResponse

    @GET("review/{id}")
    suspend fun productReview(@Path("id") id: String): ProductReviewResponse

    @POST("fulfillment")
    suspend fun fulfillment(@Body fulfillmentRequest: FulfillmentRequest): FulfillmentResponse

    @GET("transaction")
    suspend fun transaction(): TransactionResponse

    @POST("rating")
    suspend fun rating(@Body ratingRequest: RatingRequest): RatingResponse
}
