package com.verindrarizya.ecommerce.data.remote.response

import com.google.gson.annotations.SerializedName

data class PaymentResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: List<PaymentSectionItem>,

    @field:SerializedName("message")
    val message: String
)

data class PaymentItem(

    @field:SerializedName("image")
    val image: String,

    @field:SerializedName("label")
    val label: String,

    @field:SerializedName("status")
    val status: Boolean
)

data class PaymentSectionItem(

    @field:SerializedName("item")
    val item: List<PaymentItem>,

    @field:SerializedName("title")
    val title: String
)
