package com.verindrarizya.ecommerce.data.remote.response

import com.google.gson.annotations.SerializedName

data class AuthResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: AuthData,

    @field:SerializedName("message")
    val message: String
)

data class AuthData(

    @field:SerializedName("accessToken")
    val accessToken: String,

    @field:SerializedName("expiresAt")
    val expiresAt: Int,

    @field:SerializedName("refreshToken")
    val refreshToken: String
)
