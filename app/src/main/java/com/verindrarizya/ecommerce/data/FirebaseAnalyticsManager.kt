package com.verindrarizya.ecommerce.data

import android.os.Bundle
import androidx.core.os.bundleOf
import com.google.firebase.analytics.FirebaseAnalytics
import com.verindrarizya.ecommerce.data.local.entity.CartProductEntity
import com.verindrarizya.ecommerce.data.local.entity.WishlistProductEntity
import com.verindrarizya.ecommerce.data.remote.response.Product
import com.verindrarizya.ecommerce.data.remote.response.ProductDetail
import com.verindrarizya.ecommerce.ui.main.dashboard.store.FilterState
import com.verindrarizya.ecommerce.ui.main.model.Fulfillment
import com.verindrarizya.ecommerce.ui.main.model.ListProduct
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FirebaseAnalyticsManager @Inject constructor(
    private val firebaseAnalytics: FirebaseAnalytics
) {

    fun loginEvent() {
        val eventKey = FirebaseAnalytics.Event.LOGIN
        val bundle = bundleOf(
            FirebaseAnalytics.Param.METHOD to "email"
        )

        logEvent(eventKey, bundle)
    }

    fun registerEvent() {
        val eventKey = FirebaseAnalytics.Event.SIGN_UP
        val bundle = bundleOf(
            FirebaseAnalytics.Param.METHOD to "email"
        )

        logEvent(eventKey, bundle)
    }

    fun searchResultEvent(searchResult: String) {
        val eventKey = FirebaseAnalytics.Event.VIEW_SEARCH_RESULTS
        val bundle = bundleOf(
            FirebaseAnalytics.Param.SEARCH_TERM to searchResult
        )

        logEvent(eventKey, bundle)
    }

    fun selectItemFilterEvent(filterState: FilterState) {
        val eventKey = FirebaseAnalytics.Event.SELECT_ITEM

        val bundle = Bundle().apply {
            filterState.category?.let { putString("brand", it) }
            filterState.sort?.let { putString("sort", it.value) }
            filterState.lowestPrice?.let { putInt("lowest_price", it) }
            filterState.highestPrice?.let { putInt("highest_price", it) }
        }

        logEvent(eventKey, bundle)
    }

    fun viewStoreItemListEvent(products: List<Product>) {
        val eventKey = FirebaseAnalytics.Event.VIEW_ITEM_LIST

        val productsBundle: ArrayList<Bundle> = ArrayList()

        products.forEach { product ->
            val productBundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, product.productId)
                putString(FirebaseAnalytics.Param.ITEM_NAME, product.productName)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, product.brand)
                putString(FirebaseAnalytics.Param.PRICE, product.productPrice.toString())
            }
            productsBundle.add(productBundle)
        }

        val bundle = bundleOf(
            FirebaseAnalytics.Param.ITEMS to productsBundle
        )
        logEvent(eventKey, bundle)
    }

    fun selectProductEvent(product: Product) {
        val eventKey = FirebaseAnalytics.Event.SELECT_ITEM

        val productBundle = Bundle().apply {
            putString(FirebaseAnalytics.Param.ITEM_ID, product.productId)
            putString(FirebaseAnalytics.Param.ITEM_NAME, product.productName)
            putString(FirebaseAnalytics.Param.ITEM_BRAND, product.brand)
            putString(FirebaseAnalytics.Param.PRICE, product.productPrice.toString())
        }

        val bundle = bundleOf(
            FirebaseAnalytics.Param.ITEMS to arrayOf(productBundle)
        )

        logEvent(eventKey, bundle)
    }

    fun addToCartEvent(cartProductEntity: CartProductEntity) {
        val eventKey = FirebaseAnalytics.Event.ADD_TO_CART

        val productBundle = Bundle().apply {
            putString(FirebaseAnalytics.Param.ITEM_ID, cartProductEntity.productId)
            putString(FirebaseAnalytics.Param.ITEM_ID, cartProductEntity.productId)
            putString(FirebaseAnalytics.Param.ITEM_NAME, cartProductEntity.productName)
            putString(FirebaseAnalytics.Param.PRICE, cartProductEntity.price.toString())
        }

        val bundle = bundleOf(
            FirebaseAnalytics.Param.CURRENCY to "IDR",
            FirebaseAnalytics.Param.VALUE to cartProductEntity.quantity * cartProductEntity.price,
            FirebaseAnalytics.Param.ITEMS to arrayOf(productBundle)
        )

        logEvent(eventKey, bundle)
    }

    fun removeFromCart(cartProducts: Array<CartProductEntity>) {
        val eventKey = FirebaseAnalytics.Event.REMOVE_FROM_CART

        val cartBundles: ArrayList<Bundle> = ArrayList()

        cartProducts.forEach { cart ->
            val cartBundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, cart.productId)
                putString(FirebaseAnalytics.Param.ITEM_NAME, cart.productName)
                putString(FirebaseAnalytics.Param.PRICE, (cart.price * cart.quantity).toString())
            }

            cartBundles.add(cartBundle)
        }

        val bundle = bundleOf(
            FirebaseAnalytics.Param.CURRENCY to "IDR",
            FirebaseAnalytics.Param.VALUE to cartProducts.sumOf { it.quantity * it.price },
            FirebaseAnalytics.Param.ITEMS to cartBundles
        )

        logEvent(eventKey, bundle)
    }

    fun viewDetailProductEvent(productDetail: ProductDetail) {
        val eventKey = FirebaseAnalytics.Event.VIEW_ITEM

        val productBundle = Bundle().apply {
            putString(FirebaseAnalytics.Param.ITEM_ID, productDetail.productId)
            putString(FirebaseAnalytics.Param.ITEM_ID, productDetail.productId)
            putString(FirebaseAnalytics.Param.ITEM_NAME, productDetail.productName)
            putString(FirebaseAnalytics.Param.PRICE, productDetail.productPrice.toString())
            putString(FirebaseAnalytics.Param.ITEM_BRAND, productDetail.brand)
        }

        val bundle = bundleOf(
            FirebaseAnalytics.Param.CURRENCY to "IDR",
            FirebaseAnalytics.Param.VALUE to productDetail.productPrice,
            FirebaseAnalytics.Param.ITEMS to arrayOf(productBundle)
        )

        logEvent(eventKey, bundle)
    }

    fun viewCartEvent(cartProductEntities: List<CartProductEntity>) {
        val eventKey = FirebaseAnalytics.Event.VIEW_CART

        val cartProductBundles = ArrayList<Bundle>()
        cartProductEntities.forEach { cartProductEntity ->
            val productBundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, cartProductEntity.productId)
                putString(FirebaseAnalytics.Param.ITEM_NAME, cartProductEntity.productName)
                putString(
                    FirebaseAnalytics.Param.PRICE,
                    "${cartProductEntity.price * cartProductEntity.quantity}"
                )
            }

            cartProductBundles.add(productBundle)
        }

        val bundle = bundleOf(
            FirebaseAnalytics.Param.CURRENCY to "IDR",
            FirebaseAnalytics.Param.VALUE to cartProductEntities.sumOf { it.price * it.quantity }
                .toString(),
            FirebaseAnalytics.Param.ITEMS to cartProductBundles
        )

        logEvent(eventKey, bundle)
    }

    fun addToWishlistEvent(wishlistProductEntity: WishlistProductEntity) {
        val eventKey = FirebaseAnalytics.Event.ADD_TO_WISHLIST

        val wishlistBundle = Bundle().apply {
            putString(FirebaseAnalytics.Param.ITEM_ID, wishlistProductEntity.productId)
            putString(FirebaseAnalytics.Param.ITEM_NAME, wishlistProductEntity.productName)
            putString(
                FirebaseAnalytics.Param.ITEM_VARIANT,
                wishlistProductEntity.productVariantName
            )
            putString(FirebaseAnalytics.Param.PRICE, wishlistProductEntity.productPrice.toString())
        }

        val bundle = bundleOf(
            FirebaseAnalytics.Param.CURRENCY to "IDR",
            FirebaseAnalytics.Param.VALUE to wishlistProductEntity.productPrice,
            FirebaseAnalytics.Param.ITEMS to arrayOf(wishlistBundle)
        )

        logEvent(eventKey, bundle)
    }

    fun beginCheckoutEvent(listProduct: ListProduct) {
        val products = listProduct.data
        val eventKey = FirebaseAnalytics.Event.BEGIN_CHECKOUT

        val productBundles = ArrayList<Bundle>()
        products.forEach { product ->
            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, product.productId)
                putString(FirebaseAnalytics.Param.ITEM_NAME, product.productName)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, product.productVariantName)
                putString(FirebaseAnalytics.Param.PRICE, "${product.price * product.quantity}")
            }

            productBundles.add(bundle)
        }

        val bundle = bundleOf(
            FirebaseAnalytics.Param.CURRENCY to "IDR",
            FirebaseAnalytics.Param.VALUE to products.sumOf { it.price * it.quantity }.toString(),
            FirebaseAnalytics.Param.ITEMS to productBundles
        )

        logEvent(eventKey, bundle)
    }

    fun addPaymentInfoEvent(paymentName: String) {
        val eventKey = FirebaseAnalytics.Event.ADD_PAYMENT_INFO

        val bundle = bundleOf(
            FirebaseAnalytics.Param.PAYMENT_TYPE to paymentName
        )

        logEvent(eventKey, bundle)
    }

    fun purchaseEvent(fulfillment: Fulfillment) {
        val eventKey = FirebaseAnalytics.Event.PURCHASE

        val bundle = bundleOf(
            FirebaseAnalytics.Param.TRANSACTION_ID to fulfillment.invoiceId,
            FirebaseAnalytics.Param.CURRENCY to "IDR",
            FirebaseAnalytics.Param.VALUE to fulfillment.total,
        )

        logEvent(eventKey, bundle)
    }

    fun buttonClickEvent(buttonName: String) {
        val eventKey = "button_clicked"

        val bundle = bundleOf("button_name" to buttonName)

        logEvent(eventKey, bundle)
    }

    private fun logEvent(eventKey: String, param: Bundle) {
        firebaseAnalytics.logEvent(eventKey, param)
    }
}
