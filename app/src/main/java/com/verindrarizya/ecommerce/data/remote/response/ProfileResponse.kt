package com.verindrarizya.ecommerce.data.remote.response

import com.google.gson.annotations.SerializedName

data class ProfileResponse(

    @field:SerializedName("code")
    val code: Int,

    @field:SerializedName("data")
    val data: ProfileData,

    @field:SerializedName("message")
    val message: String
)

data class ProfileData(

    @field:SerializedName("userImage")
    val userImage: String,

    @field:SerializedName("userName")
    val userName: String
)
