package com.verindrarizya.ecommerce.data.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.verindrarizya.ecommerce.data.local.entity.WishlistProductEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface WishlistProductDao {

    @Query("SELECT * FROM wishlist_product")
    fun getAll(): Flow<List<WishlistProductEntity>>

    @Query("SELECT EXISTS(SELECT * FROM wishlist_product WHERE productId = :productId)")
    fun isInWishlist(productId: String): Flow<Boolean>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(wishlistProductEntity: WishlistProductEntity)

    @Delete
    suspend fun delete(wishlistProductEntity: WishlistProductEntity)

    @Query("DELETE FROM wishlist_product")
    suspend fun deleteAllRecords()
}
