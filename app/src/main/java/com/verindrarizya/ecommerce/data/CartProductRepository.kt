package com.verindrarizya.ecommerce.data

import com.verindrarizya.ecommerce.data.local.dao.CartProductDao
import com.verindrarizya.ecommerce.data.local.entity.CartProductEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CartProductRepository @Inject constructor(
    private val cartProductDao: CartProductDao,
) {

    fun getAllCartProduct(): Flow<List<CartProductEntity>> = cartProductDao.getAll()

    suspend fun updateProduct(cartProductEntity: CartProductEntity) =
        cartProductDao.updateProduct(cartProductEntity)

    suspend fun deleteAllRecords() = cartProductDao.deleteAllRecords()

    suspend fun insertToCart(cartProductEntity: CartProductEntity) {
        /**
         * rowId value:
         * 1 -> cartProduct success inserted to database
         * -1 -> cartProduct failed inserted to database due to the record already exist
         */
        val rowId = cartProductDao.insertToCart(cartProductEntity)

        val cartProductRecord = cartProductDao.getCartProductById(cartProductEntity.productId)
        // If record already exist and quantity still less than stock, update the quantity instead
        if (rowId == -1L && cartProductRecord.quantity < cartProductRecord.stock) {
            val updatedCartProduct =
                cartProductRecord.copy(quantity = cartProductRecord.quantity + 1)
            updateProduct(updatedCartProduct)
        }
    }

    suspend fun deleteCartProducts(cartProductEntities: Array<CartProductEntity>) {
        cartProductDao.deleteCartProducts(*cartProductEntities)
    }

    suspend fun updateAllSelectedProduct(flag: Boolean) =
        cartProductDao.updateAllSelectedProduct(flag)
}
