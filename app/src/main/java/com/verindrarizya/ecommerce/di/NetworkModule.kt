package com.verindrarizya.ecommerce.di

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.verindrarizya.ecommerce.data.PreferencesRepository
import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.network.AuthInterceptor
import com.verindrarizya.ecommerce.network.NetworkAuthenticator
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

private const val BASE_IP = "172.17.20.101"
const val BASE_URl = "http://$BASE_IP:8080/"

@InstallIn(SingletonComponent::class)
@Module
object NetworkModule {

    @Provides
    fun provideNetworkAuthenticator(
        preferencesRepository: PreferencesRepository,
        chuckerInterceptor: ChuckerInterceptor
    ): NetworkAuthenticator {
        return NetworkAuthenticator(preferencesRepository, chuckerInterceptor)
    }

    @Provides
    fun provideAuthInterceptor(preferencesRepository: PreferencesRepository): AuthInterceptor {
        return AuthInterceptor(preferencesRepository)
    }

    @Provides
    fun provideChuckerInterceptor(@ApplicationContext context: Context): ChuckerInterceptor {
        return ChuckerInterceptor.Builder(context)
            .collector(ChuckerCollector(context))
            .maxContentLength(250000L)
            .redactHeaders(emptySet())
            .alwaysReadResponseBody(true)
            .build()
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(
        chuckerInterceptor: ChuckerInterceptor,
        authInterceptor: AuthInterceptor,
        networkAuthenticator: NetworkAuthenticator
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(chuckerInterceptor)
            .addInterceptor(authInterceptor)
            .authenticator(networkAuthenticator)
            .build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun provideApiService(
        retrofit: Retrofit
    ): ApiService {
        return retrofit.create(ApiService::class.java)
    }
}
