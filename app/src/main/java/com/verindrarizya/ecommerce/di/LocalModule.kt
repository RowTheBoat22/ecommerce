package com.verindrarizya.ecommerce.di

import android.content.Context
import androidx.room.Room
import com.verindrarizya.ecommerce.data.local.StoreDatabase
import com.verindrarizya.ecommerce.data.local.dao.CartProductDao
import com.verindrarizya.ecommerce.data.local.dao.NotifDao
import com.verindrarizya.ecommerce.data.local.dao.WishlistProductDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class LocalModule {

    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext context: Context
    ): StoreDatabase {
        return Room.databaseBuilder(
            context,
            StoreDatabase::class.java,
            "ecommerce_store_database"
        ).build()
    }

    @Singleton
    @Provides
    fun provideWishlistProductDao(
        storeDatabase: StoreDatabase
    ): WishlistProductDao = storeDatabase.wishlistProductDao()

    @Singleton
    @Provides
    fun provideCartProductDao(
        storeDatabase: StoreDatabase
    ): CartProductDao = storeDatabase.cartProductDao()

    @Singleton
    @Provides
    fun provideNotifDao(
        storeDatabase: StoreDatabase
    ): NotifDao = storeDatabase.notifDao()
}
