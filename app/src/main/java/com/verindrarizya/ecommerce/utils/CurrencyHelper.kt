package com.verindrarizya.ecommerce.utils

import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.Locale

object CurrencyHelper {

    private val locale = Locale("id", "ID")
    private val formatter: NumberFormat = NumberFormat.getCurrencyInstance(locale).apply {
        maximumFractionDigits = 0
    }

    fun convertToRupiah(price: Int): String = formatter.format(price)

    fun convertRupiahToInt(rupiahValue: String): Int {
        val format =
            NumberFormat.getInstance(Locale("id", "ID")) // Locale for Indonesian Rupiah (IDR)
        val removedPrefixValue = rupiahValue
            .replace("Rp", "")
        val parsed = format.parse(removedPrefixValue)
        return parsed.toInt()
    }

    fun addThousandSeparator(number: Int): String {
        val decimalFormat = DecimalFormat("#,###.###")
        return decimalFormat.format(number).replace(",", ".")
    }

    fun removeThousandSeparator(formattedNumber: String): Int {
        val numberString = formattedNumber.replace(".", "")
        return numberString.toInt()
    }
}
