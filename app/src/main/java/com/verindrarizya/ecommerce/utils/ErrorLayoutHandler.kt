package com.verindrarizya.ecommerce.utils

import android.content.Context
import com.google.gson.Gson
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.remote.response.ErrorResponse
import com.verindrarizya.ecommerce.databinding.ErrorLayoutBinding
import retrofit2.HttpException
import java.io.IOException

fun errorLayoutHandler(
    context: Context,
    binding: ErrorLayoutBinding,
    error: Throwable,
    buttonText: String,
    onButtonClick: () -> Unit
) {
    binding.btnAction.text = buttonText
    when (error) {
        is HttpException -> {
            val errorResponse = error.getErrorResponse()

            val title: String
            val description: String

            if (errorResponse.code == 404) {
                title = context.getString(R.string.empty)
                description = context.getString(R.string.empty_description)
                binding.btnAction.setOnClickListener {
                    onButtonClick()
                }
            } else {
                title = errorResponse.code.toString()
                description = errorResponse.message
                binding.btnAction.setOnClickListener {
                    onButtonClick()
                }
            }

            binding.tvErrorTitle.text = title
            binding.tvErrorDesc.text = description
        }

        is IOException -> {
            binding.tvErrorTitle.text = context.getString(R.string.connection)
            binding.tvErrorDesc.text = context.getString(R.string.your_connection_is_unavailable)
            binding.btnAction.text = context.getString(R.string.refresh)
            binding.btnAction.setOnClickListener {
                onButtonClick()
            }
        }
    }
}

fun HttpException.getErrorResponse(): ErrorResponse {
    val gson = Gson()
    val responseJsonString = response()?.errorBody()?.string()

    val errorResponse: ErrorResponse? = gson.fromJson(responseJsonString, ErrorResponse::class.java)

    val code = errorResponse?.code ?: this.code()
    val message = errorResponse?.message ?: this.message()

    return ErrorResponse(
        code = code,
        message = message
    )
}
