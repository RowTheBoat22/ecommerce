package com.verindrarizya.ecommerce.utils

sealed class Resource<out T> {
    data class Success<T>(val data: T) : Resource<T>()

    data class Failure(val throwable: Throwable) : Resource<Nothing>()

    object Loading : Resource<Nothing>()
}
