package com.verindrarizya.ecommerce

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.CartProductRepository
import com.verindrarizya.ecommerce.data.NotifRepository
import com.verindrarizya.ecommerce.data.PreferencesRepository
import com.verindrarizya.ecommerce.data.WishlistProductRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val preferencesRepository: PreferencesRepository,
    private val cartProductRepository: CartProductRepository,
    private val wishlistProductRepository: WishlistProductRepository,
    private val notifRepository: NotifRepository
) : ViewModel() {

    private val isLogin = preferencesRepository.isLogIn

    private val isUsernameSet = preferencesRepository.isUserNameAlreadySet

    val isDarkMode = preferencesRepository.isDarkMode.distinctUntilChanged()

    val auth = combine(isLogin, isUsernameSet) { l, us ->
        Pair(l, us)
    }
        .distinctUntilChanged()

    val username = preferencesRepository.username.asLiveData()

    fun deleteData() {
        viewModelScope.launch {
            preferencesRepository.clearData()
            cartProductRepository.deleteAllRecords()
            wishlistProductRepository.deleteAllRecords()
            notifRepository.deleteAllRecords()
        }
    }
}
