package com.verindrarizya.ecommerce.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.local.dao.NotifDao
import com.verindrarizya.ecommerce.data.local.entity.NotificationEntity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class StoreService : FirebaseMessagingService() {

    @Inject
    lateinit var notifDao: NotifDao

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d("onNewTokenTag", "onNewToken: $token")
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        val title = message.data["title"] ?: ""
        val desc = message.data["description"] ?: ""
        val image = message.data["image"] ?: ""
        val date = message.data["date"] ?: ""
        val time = message.data["time"] ?: ""
        val type = message.data["type"] ?: ""

        val notificationEntity = NotificationEntity(
            title = title,
            desc = desc,
            image = image,
            date = date,
            time = time,
            type = type,
        )
        notifDao.insert(notificationEntity)

        showNotification(title, desc)
    }

    private fun showNotification(title: String, desc: String) {
        val channelId = "1"
        val channelName = "Store"

        val pendingIntent = NavDeepLinkBuilder(this)
            .setGraph(R.navigation.main_nav_graph)
            .setDestination(R.id.notificationFragment)
            .createPendingIntent()

        val builder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.notification_ecommerce)
            .setContentTitle(title)
            .setContentText(desc)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(desc)
            )
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        val notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(channelId, channelName, importance)
            // Register the channel with the system
            notificationManager.createNotificationChannel(channel)
        }

        val notificationId = System.currentTimeMillis().toInt()

        notificationManager.notify(notificationId, builder.build())
    }
}
