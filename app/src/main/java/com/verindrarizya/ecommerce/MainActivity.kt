package com.verindrarizya.ecommerce

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.fragment.NavHostFragment
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.verindrarizya.ecommerce.databinding.ActivityMainBinding
import com.verindrarizya.ecommerce.utils.launchAndCollectIn
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    @Inject
    lateinit var firebaseMessaging: FirebaseMessaging

    @Inject
    lateinit var firebaseRemoteConfig: FirebaseRemoteConfig

    override fun onCreate(savedInstanceState: Bundle?) {
        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        firebaseRemoteConfig.fetchAndActivate()



        viewModel.isDarkMode.launchAndCollectIn(this) {
            AppCompatDelegate.setDefaultNightMode(
                if (it) {
                    AppCompatDelegate.MODE_NIGHT_YES
                } else AppCompatDelegate.MODE_NIGHT_NO
            )
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
            splashScreen.setKeepOnScreenCondition { true }
            Handler(Looper.getMainLooper()).postDelayed({
                splashScreen.setKeepOnScreenCondition { false }
            }, 2_000)
        }

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController

        viewModel.auth.launchAndCollectIn(this) {
            val isLogin = it.first
            val isUsernameSet = it.second

            if (isLogin) {
                firebaseMessaging.subscribeToTopic("promo")
                if (!isUsernameSet && navController.currentDestination?.id != R.id.profileFragment) {
                    navController.navigate(R.id.action_global_profileFragment)
                }
            } else {
                firebaseMessaging.unsubscribeFromTopic("promo")
                viewModel.deleteData()
                navController.navigate(R.id.action_global_loginFragment)
            }
        }
    }
}
