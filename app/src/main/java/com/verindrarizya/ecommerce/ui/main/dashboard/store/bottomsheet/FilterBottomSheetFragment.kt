package com.verindrarizya.ecommerce.ui.main.dashboard.store.bottomsheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.databinding.FragmentFilterBottomSheetListDialogBinding
import com.verindrarizya.ecommerce.ui.main.dashboard.store.FilterState
import com.verindrarizya.ecommerce.ui.main.dashboard.store.SortCategory
import com.verindrarizya.ecommerce.ui.main.dashboard.store.StoreViewModel
import com.verindrarizya.ecommerce.utils.launchAndCollectIn
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FilterBottomSheetFragment : BottomSheetDialogFragment() {

    private var _binding: FragmentFilterBottomSheetListDialogBinding? = null
    private val binding get() = _binding!!

    private val viewModel: StoreViewModel by viewModels(
        ownerProducer = { requireParentFragment() }
    )

    @Inject
    lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val contextThemeWrapper =
            ContextThemeWrapper(requireActivity(), R.style.Base_Theme_ECommerce)
        _binding = FragmentFilterBottomSheetListDialogBinding.inflate(
            inflater.cloneInContext(contextThemeWrapper),
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val modalBottomSheetBehavior = (dialog as BottomSheetDialog).behavior
        dialog?.setOnShowListener {
            modalBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        setInitState()

        binding.btnShowProduct.setOnClickListener {
            firebaseAnalyticsManager.buttonClickEvent("button set new filter")
            actionOnButtonShowProductClicked()
        }

        binding.btnReset.setOnClickListener {
            firebaseAnalyticsManager.buttonClickEvent("button reset choosen filter")
            actionOnButtonResetClicked()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setInitState() {
        viewModel.filterSelectedState.launchAndCollectIn(viewLifecycleOwner) { filterState: FilterState? ->
            setResetButtonVisibility(filterState)

            filterState?.let { state ->
                state.sort?.let {
                    when (it) {
                        SortCategory.Review -> binding.chipReview.isChecked = true
                        SortCategory.Sale -> binding.chipSold.isChecked = true
                        SortCategory.LowestPrice -> binding.chipLowestPrice.isChecked = true
                        SortCategory.HighestPrice -> binding.chipHighestPrices.isChecked = true
                    }
                }

                state.category?.let {
                    when (it) {
                        "Asus" -> binding.chipAsus.isChecked = true
                        "Lenovo" -> binding.chipLenovo.isChecked = true
                        "Apple" -> binding.chipApple.isChecked = true
                        "Dell" -> binding.chipDell.isChecked = true
                    }
                }

                state.lowestPrice?.let {
                    binding.edLowestPrice.setText(it.toString())
                }

                state.highestPrice?.let {
                    binding.edHighestPrice.setText(it.toString())
                }
            }
        }
    }

    private fun actionOnButtonShowProductClicked() {
        val chosenSortCategory: SortCategory? = when (binding.cgSort.checkedChipId) {
            binding.chipReview.id -> SortCategory.Review
            binding.chipSold.id -> SortCategory.Sale
            binding.chipLowestPrice.id -> SortCategory.LowestPrice
            binding.chipHighestPrices.id -> SortCategory.HighestPrice
            else -> null
        }

        val chosenBrand: String? = when (binding.cgCategory.checkedChipId) {
            binding.chipAsus.id -> "Asus"
            binding.chipLenovo.id -> "Lenovo"
            binding.chipApple.id -> "Apple"
            binding.chipDell.id -> "Dell"
            else -> null
        }

        val lowestPrice: Int? = if (binding.edLowestPrice.text.toString().isBlank()) {
            null
        } else {
            binding.edLowestPrice.text.toString().toIntOrNull()
        }

        val highestPrice: Int? = if (binding.edHighestPrice.text.toString().isBlank()) {
            null
        } else {
            binding.edHighestPrice.text.toString().toIntOrNull()
        }

        viewModel.setFilter(
            sortCategory = chosenSortCategory,
            brand = chosenBrand,
            lowestPrice = lowestPrice,
            highestPrice = highestPrice
        )
        dismiss()
    }

    private fun actionOnButtonResetClicked() {
        binding.apply {
            cgSort.clearCheck()
            cgCategory.clearCheck()

            edLowestPrice.clearFocus()
            edLowestPrice.text = null

            edHighestPrice.clearFocus()
            edHighestPrice.text = null
        }
    }

    private fun setResetButtonVisibility(filterState: FilterState?) {
        binding.btnReset.isVisible =
            filterState?.sort != null || filterState?.category != null ||
                    filterState?.highestPrice != null || filterState?.lowestPrice != null
    }

    companion object {
        const val TAG = "FilterBottomMenu"
    }
}
