package com.verindrarizya.ecommerce.ui.main.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.CartProductRepository
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.data.NotifRepository
import com.verindrarizya.ecommerce.data.ProductRepository
import com.verindrarizya.ecommerce.data.WishlistProductRepository
import com.verindrarizya.ecommerce.data.remote.response.ProductDetailResponse
import com.verindrarizya.ecommerce.data.remote.response.ProductVariantItem
import com.verindrarizya.ecommerce.data.remote.response.toCartProductEntity
import com.verindrarizya.ecommerce.data.remote.response.toWishlistProductEntity
import com.verindrarizya.ecommerce.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val productRepository: ProductRepository,
    private val wishlistProductRepository: WishlistProductRepository,
    private val cartProductRepository: CartProductRepository,
    private val firebaseAnalyticsManager: FirebaseAnalyticsManager,
    savedStateHandle: SavedStateHandle,
    notifRepository: NotifRepository
) : ViewModel() {

    val id = savedStateHandle.get<String>("id")!!

    private val _productDetail: MutableLiveData<Resource<ProductDetailResponse>> =
        MutableLiveData(Resource.Loading)
    val productDetail: LiveData<Resource<ProductDetailResponse>>
        get() = _productDetail

    private val _selectedProductVariantItem: MutableLiveData<ProductVariantItem> = MutableLiveData()
    val selectedProductVariantItem: LiveData<ProductVariantItem>
        get() = _selectedProductVariantItem

    val isInWishlist = wishlistProductRepository.checkIsInWishlist(id)

    val cartProductCount = cartProductRepository.getAllCartProduct().map {
        it.size
    }.asLiveData()

    val unreadNotif = notifRepository.allNotification
        .map {
            it.filter { notif ->
                !notif.isRead
            }
        }
        .map { it.size }
        .asLiveData()

    init {
        getProductDetail()
    }

    fun getProductDetail() {
        viewModelScope.launch {
            productRepository.productDetail(id).collect {
                _productDetail.value = it
                if (it is Resource.Success) {
                    val productDetail = it.data.data
                    _selectedProductVariantItem.value = productDetail.productVariant[0]
                }
            }
        }
    }

    fun saveToWishlist() {
        viewModelScope.launch {
            if (productDetail.value is Resource.Success) {
                val productDetail =
                    (productDetail.value as Resource.Success<ProductDetailResponse>).data.data
                val wishlistProduct = productDetail.toWishlistProductEntity()
                firebaseAnalyticsManager.addToWishlistEvent(wishlistProduct)
                wishlistProductRepository.saveToWishlist(wishlistProduct)
            }
        }
    }

    fun deleteFromWishlist() {
        viewModelScope.launch {
            if (productDetail.value is Resource.Success) {
                val productDetail =
                    (productDetail.value as Resource.Success<ProductDetailResponse>).data.data
                val wishlistProduct = productDetail.toWishlistProductEntity()
                wishlistProductRepository.deleteFromWishlist(wishlistProduct)
            }
        }
    }

    fun insertToCart() {
        viewModelScope.launch {
            if (productDetail.value is Resource.Success) {
                val productDetail =
                    (productDetail.value as Resource.Success<ProductDetailResponse>).data.data
                val cartProduct = productDetail.toCartProductEntity()
                firebaseAnalyticsManager.addToCartEvent(cartProduct)
                cartProductRepository.insertToCart(cartProduct)
            }
        }
    }

    fun selectProductVariantItem(productVariantItem: ProductVariantItem) {
        _selectedProductVariantItem.value = productVariantItem
    }
}
