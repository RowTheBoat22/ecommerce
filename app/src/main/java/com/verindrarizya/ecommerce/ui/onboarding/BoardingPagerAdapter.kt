package com.verindrarizya.ecommerce.ui.onboarding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.verindrarizya.ecommerce.databinding.OnBoardingItemBinding

class BoardingPagerAdapter(
    private val onBoardImages: List<Int>
) : RecyclerView.Adapter<BoardingPagerAdapter.ViewHolder>() {

    private val listOfString: List<String> = (1..3).map {
        "On Boarding Item $it"
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding =
            OnBoardingItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(onBoardImages[position])
    }

    override fun getItemCount(): Int {
        return listOfString.size
    }

    class ViewHolder(private val binding: OnBoardingItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(drawable: Int) {
            Glide.with(itemView.context)
                .load(drawable)
                .into(binding.ivOnboard)
        }
    }
}
