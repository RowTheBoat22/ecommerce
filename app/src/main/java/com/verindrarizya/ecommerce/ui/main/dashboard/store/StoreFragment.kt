package com.verindrarizya.ecommerce.ui.main.dashboard.store

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.chip.Chip
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.data.remote.response.Product
import com.verindrarizya.ecommerce.databinding.FragmentStoreBinding
import com.verindrarizya.ecommerce.ui.main.dashboard.DashboardFragmentDirections
import com.verindrarizya.ecommerce.ui.main.dashboard.store.adapter.ProductLoadStateAdapter
import com.verindrarizya.ecommerce.ui.main.dashboard.store.adapter.ProductPagingAdapter
import com.verindrarizya.ecommerce.ui.main.dashboard.store.bottomsheet.FilterBottomSheetFragment
import com.verindrarizya.ecommerce.ui.main.search.StoreSearchFragment
import com.verindrarizya.ecommerce.utils.CurrencyHelper
import com.verindrarizya.ecommerce.utils.errorLayoutHandler
import com.verindrarizya.ecommerce.utils.launchAndCollectIn
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import retrofit2.HttpException
import javax.inject.Inject

@AndroidEntryPoint
class StoreFragment : Fragment() {

    private val viewModel: StoreViewModel by viewModels()

    private var _binding: FragmentStoreBinding? = null
    private val binding: FragmentStoreBinding
        get() = _binding!!

    private lateinit var productPagingAdapter: ProductPagingAdapter

    private lateinit var layoutManager: GridLayoutManager

    @Inject
    lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStoreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRvAdapterAndLayoutManager()
        setProductRecyclerViewObserver()

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.product.collectLatest { value: PagingData<Product> ->
                binding.rvStore.scrollToPosition(0)
                productPagingAdapter.submitData(value)
            }
        }

        productPagingAdapter.addOnPagesUpdatedListener {
            val listOfProduct = productPagingAdapter.snapshot().items
            firebaseAnalyticsManager.viewStoreItemListEvent(listOfProduct)
        }

        binding.swipeRefresh.apply {
            setOnRefreshListener {
                productPagingAdapter.refresh()
                isRefreshing = false
            }
        }

        observePagingAdapterLoadState()

        binding.ivListToggle.setOnClickListener {
            viewModel.toggleDisplayedList()
        }

        binding.chipFilter.setOnClickListener {
            val filterBottomSheetFragment = FilterBottomSheetFragment()
            filterBottomSheetFragment.show(childFragmentManager, FilterBottomSheetFragment.TAG)
        }

        setSearchResultListener()

        binding.edSearch.setOnClickListener {
            val parentNavController = requireActivity().findNavController(R.id.nav_host_fragment)
            parentNavController.navigate(
                DashboardFragmentDirections.actionDashboardFragmentToStoreSearchFragment(
                    binding.edSearch.text.toString()
                )
            )
        }

        viewModel.searchedProductName.launchAndCollectIn(viewLifecycleOwner) {
            if (it.isNotBlank()) {
                binding.tfSearch.hint = null
                binding.edSearch.setText(it)
            } else {
                binding.edSearch.text = null
                binding.tfSearch.hint = getString(R.string.search)
            }
        }

        observeSelectedFilterState()
    }

    private fun observeSelectedFilterState() {
        viewModel.filterSelectedState.launchAndCollectIn(viewLifecycleOwner) { filterState: FilterState? ->
            clearFilterChipGroup()

            filterState?.let { state ->
                state.sort?.let {
                    val newChip = createFilterChip(getString(it.displayedName))
                    binding.cgFilter.addView(newChip)
                }

                state.category?.let {
                    val newChip = createFilterChip(it)
                    binding.cgFilter.addView(newChip)
                }

                state.lowestPrice?.let {
                    val newChip = createFilterChip("< ${CurrencyHelper.convertToRupiah(it)}")
                    binding.cgFilter.addView(newChip)
                }

                state.highestPrice?.let {
                    val newChip = createFilterChip("> ${CurrencyHelper.convertToRupiah(it)}")
                    binding.cgFilter.addView(newChip)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun setProductRecyclerViewObserver() {
        viewModel.isDisplayedList.launchAndCollectIn(viewLifecycleOwner) { value ->
            if (value) {
                binding.ivListToggle.setBackgroundResource(R.drawable.format_list_bulleted)
                productPagingAdapter.setViewType(1)
                layoutManager.spanCount = 1
            } else {
                binding.ivListToggle.setBackgroundResource(R.drawable.grid_view)
                productPagingAdapter.setViewType(2)
                layoutManager.spanCount = 2
            }
        }
    }

    private fun initRvAdapterAndLayoutManager() {
        val loadStateAdapter = ProductLoadStateAdapter()
        layoutManager = GridLayoutManager(context, 1).apply {
            spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if ((position == productPagingAdapter.itemCount) && loadStateAdapter.itemCount > 0) {
                        spanCount
                    } else {
                        1
                    }
                }
            }
        }

        productPagingAdapter = ProductPagingAdapter(onItemClick = {
            firebaseAnalyticsManager.selectProductEvent(it)
            val parentNavController = requireActivity().findNavController(R.id.nav_host_fragment)
            parentNavController.navigate(
                DashboardFragmentDirections.actionDashboardFragmentToDetailFragment(it.productId)
            )
        })
        val adapterWithLoadStateFooter = productPagingAdapter.withLoadStateFooter(loadStateAdapter)

        binding.rvStore.layoutManager = layoutManager
        binding.rvStore.adapter = adapterWithLoadStateFooter
    }

    private fun createFilterChip(text: String): Chip {
        val newChip = Chip(context)
        newChip.text = text
        newChip.isCheckable = false
        newChip.isClickable = false

        return newChip
    }

    private fun clearFilterChipGroup() {
        binding.cgFilter.removeAllViews()
    }

    private fun setSearchResultListener() {
        requireActivity().supportFragmentManager.setFragmentResultListener(
            StoreSearchFragment.REQUEST_KEY,
            viewLifecycleOwner
        ) { _, bundle ->
            val resultProductName = bundle.getString(StoreSearchFragment.RESULT_PRODUCT_NAME_KEY)
            viewModel.searchProduct(resultProductName.toString())
        }
    }

    private fun observePagingAdapterLoadState() {
        viewLifecycleOwner.lifecycleScope.launch {
            productPagingAdapter.loadStateFlow.collectLatest { loadStates: CombinedLoadStates ->
                setFilterAndLayoutToggleVisibility(loadStates.refresh !is LoadState.Loading)

                binding.loadingShimmerList.isVisible =
                    loadStates.refresh is LoadState.Loading && viewModel.isDisplayedList.value == true
                binding.loadingShimmerGrid.isVisible =
                    loadStates.refresh is LoadState.Loading && viewModel.isDisplayedList.value == false

                binding.rvStore.isVisible =
                    loadStates.refresh is LoadState.NotLoading // state success
                binding.errorLayout.root.isVisible = loadStates.refresh is LoadState.Error

                if (loadStates.refresh is LoadState.Error) {
                    val error = (loadStates.refresh as LoadState.Error).error
                    handleError(error)
                }
            }
        }
    }

    private fun handleError(error: Throwable) {
        context?.let {
            errorLayoutHandler(
                context = it,
                binding = binding.errorLayout,
                error = error,
                buttonText = if (error is HttpException && error.code() == 404) {
                    getString(R.string.reset)
                } else {
                    getString(R.string.refresh)
                },
                onButtonClick = {
                    if (error is HttpException && error.code() == 404) {
                        viewModel.resetData()
                    }
                    productPagingAdapter.refresh()
                },
            )
        }
    }

    private fun setFilterAndLayoutToggleVisibility(isVisible: Boolean) {
        binding.apply {
            chipFilter.isVisible = isVisible
            cgFilter.isVisible = isVisible
            divider.isVisible = isVisible
            ivListToggle.isVisible = isVisible
        }
    }
}
