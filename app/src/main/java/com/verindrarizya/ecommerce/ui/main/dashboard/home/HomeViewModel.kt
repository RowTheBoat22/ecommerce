package com.verindrarizya.ecommerce.ui.main.dashboard.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.PreferencesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val preferencesRepository: PreferencesRepository
) : ViewModel() {

    val isDarkMode = preferencesRepository.isDarkMode.distinctUntilChanged()

    fun logout() {
        viewModelScope.launch {
            preferencesRepository.clearData()
        }
    }

    fun toggleDarkMode(value: Boolean) {
        viewModelScope.launch {
            preferencesRepository.toggleDarkMode(value)
        }
    }
}
