package com.verindrarizya.ecommerce.ui.main.review

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.ProductRepository
import com.verindrarizya.ecommerce.data.remote.response.ProductReviewResponse
import com.verindrarizya.ecommerce.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ReviewViewModel @Inject constructor(
    private val productRepository: ProductRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    val id: String = savedStateHandle.get<String>("id") ?: ""

    private val _productReviewResponse: MutableLiveData<Resource<ProductReviewResponse>> =
        MutableLiveData()
    val productReviewResponse: LiveData<Resource<ProductReviewResponse>>
        get() = _productReviewResponse

    init {
        getProductReview()
    }

    fun getProductReview() {
        viewModelScope.launch {
            productRepository.productReview(id).collect {
                _productReviewResponse.value = it
            }
        }
    }
}
