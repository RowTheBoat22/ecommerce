package com.verindrarizya.ecommerce.ui.main.choosepayment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.verindrarizya.ecommerce.data.PaymentRepository
import com.verindrarizya.ecommerce.data.remote.response.PaymentSectionItem
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ChoosePaymentViewModel @Inject constructor(
    private val paymentRepository: PaymentRepository
) : ViewModel() {

    private val _payments: MutableLiveData<List<PaymentSectionItem>> = MutableLiveData()
    val payments: LiveData<List<PaymentSectionItem>>
        get() = _payments

    init {
        getPayment()
    }

    fun getPayment() {
        val payments = paymentRepository.getPayment()
        _payments.value = payments
    }
}
