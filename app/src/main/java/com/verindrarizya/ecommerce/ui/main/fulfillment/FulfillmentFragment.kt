package com.verindrarizya.ecommerce.ui.main.fulfillment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.databinding.FragmentFulfillmentBinding
import com.verindrarizya.ecommerce.utils.CurrencyHelper
import com.verindrarizya.ecommerce.utils.Resource
import com.verindrarizya.ecommerce.utils.getErrorResponse
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class FulfillmentFragment : Fragment() {

    private var _binding: FragmentFulfillmentBinding? = null
    private val binding: FragmentFulfillmentBinding
        get() = _binding!!

    private val viewModel: FulfillmentViewModel by viewModels()

    private val navArgs: FulfillmentFragmentArgs by navArgs()

    @Inject
    lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    private val setFulfillmentFragmentResult: (Boolean) -> Unit = {
        requireActivity()
            .supportFragmentManager
            .setFragmentResult(FULFILLMENT_REQUEST_KEY, bundleOf())
        findNavController().navigateUp()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFulfillmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnBackPressed()
        fillLayoutData()

        viewModel.ratingFlow.observe(viewLifecycleOwner) {
            binding.progressBar.isVisible = it is Resource.Loading
            binding.svFulfillment.isVisible = it !is Resource.Loading

            if (it is Resource.Success) {
                showSnackbar(it.data.message)

                if (navArgs.isFromCheckout) {
                    findNavController().navigate(
                        FulfillmentFragmentDirections.actionGlobalDashboardFragment()
                    )
                } else {
                    setFulfillmentFragmentResult(true)
                    findNavController().navigateUp()
                }
            } else if (it is Resource.Failure) {
                when (val error = it.throwable) {
                    is HttpException -> {
                        val errorResponse = error.getErrorResponse()
                        showSnackbar(errorResponse.message)
                    }

                    is IOException -> showSnackbar(getString(R.string.your_connection_is_unavailable))
                    else -> showSnackbar(getString(R.string.general_error_statement))
                }
            }
        }

        binding.btnDone.setOnClickListener {
            firebaseAnalyticsManager.buttonClickEvent("button fulfillment done")
            viewModel.postRating(
                review = binding.edReview.text.toString()
            )
        }

        binding.rbPayment.setOnRatingBarChangeListener { _, fl, _ ->
            viewModel.updateRating(fl.toInt())
        }
    }

    private fun showSnackbar(snackbarMessage: String) {
        Snackbar.make(binding.root, snackbarMessage, Snackbar.LENGTH_SHORT)
            .show()
    }

    private fun fillLayoutData() {
        viewModel.fulfillmentData.observe(viewLifecycleOwner) { fulfillmentData ->
            binding.tvTransactionIdValue.text = fulfillmentData.invoiceId
            binding.tvTransactionStatusValue.text = if (fulfillmentData.status) {
                getString(R.string.success)
            } else {
                getString(R.string.failed)
            }
            binding.tvTransactionDateValue.text = fulfillmentData.date
            binding.tvTransactionTimeValue.text = fulfillmentData.time
            binding.tvTransactionPaymentMethodValue.text = fulfillmentData.payment
            binding.tvTransactionTotalPaymentValue.text =
                CurrencyHelper.convertToRupiah(fulfillmentData.total)

            fulfillmentData.rating?.let { binding.rbPayment.rating = it.toFloat() }
            fulfillmentData.review?.let { binding.edReview.setText(it) }
        }
    }

    private fun setOnBackPressed() {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (navArgs.isFromCheckout) {
                        findNavController().navigate(
                            FulfillmentFragmentDirections.actionGlobalDashboardFragment()
                        )
                    } else {
                        findNavController().navigateUp()
                    }
                }
            }
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val FULFILLMENT_REQUEST_KEY = "fulfillment_request_key"
    }
}
