package com.verindrarizya.ecommerce.ui.main.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.verindrarizya.ecommerce.ui.theme.ECommerceTheme
import com.verindrarizya.ecommerce.utils.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReviewFragment : Fragment() {

    private val viewModel: ReviewViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeView(requireContext()).apply {
        setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)

        setContent {
            val resourceProductReviewResponse by viewModel.productReviewResponse.observeAsState(
                Resource.Loading
            )

            ECommerceTheme {
                ReviewScreen(
                    onNavigationIconClick = { findNavController().navigateUp() },
                    resourceProductReviewResponse = resourceProductReviewResponse,
                    onRefreshClick = { viewModel.getProductReview() }
                )
            }
        }
    }
}
