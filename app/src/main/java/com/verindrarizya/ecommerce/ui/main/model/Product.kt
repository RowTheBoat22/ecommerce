package com.verindrarizya.ecommerce.ui.main.model

import android.os.Parcelable
import androidx.annotation.Keep
import com.verindrarizya.ecommerce.data.local.entity.CartProductEntity
import com.verindrarizya.ecommerce.data.remote.request.FulfillmentItem
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class Product(
    val productId: String,
    val productName: String,
    val image: String,
    val productVariantName: String,
    val stock: Int,
    val price: Int,
    val quantity: Int = 1,
    val isSelected: Boolean = false
) : Parcelable

fun CartProductEntity.toProduct() =
    Product(
        productId = productId,
        productName = productName,
        image = image,
        productVariantName = productVariantName,
        stock = stock,
        price = price,
        quantity = quantity,
        isSelected = isSelected
    )

fun Product.toCartProductEntity() =
    CartProductEntity(
        productId = productId,
        image = image,
        productName = productName,
        productVariantName = productVariantName,
        stock = stock,
        price = price,
        quantity = quantity,
        isSelected = isSelected
    )

fun Product.toFulfillmentItem() =
    FulfillmentItem(
        quantity = quantity,
        productId = productId,
        variantName = productVariantName
    )
