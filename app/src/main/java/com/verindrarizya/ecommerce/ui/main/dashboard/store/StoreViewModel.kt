package com.verindrarizya.ecommerce.ui.main.dashboard.store

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.data.ProductRepository
import com.verindrarizya.ecommerce.data.remote.response.Product
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import javax.inject.Inject

data class FilterState(
    val sort: SortCategory? = null,
    val category: String? = null,
    val lowestPrice: Int? = null,
    val highestPrice: Int? = null,
)

@HiltViewModel
class StoreViewModel @Inject constructor(
    private val productRepository: ProductRepository,
    private val firebaseAnalyticsManager: FirebaseAnalyticsManager,
) : ViewModel() {

    private val _searchedProductName: MutableStateFlow<String> = MutableStateFlow("")
    val searchedProductName: StateFlow<String> = _searchedProductName.asStateFlow()

    private val _filterSelectedState: MutableStateFlow<FilterState?> = MutableStateFlow(null)
    val filterSelectedState: StateFlow<FilterState?> = _filterSelectedState.asStateFlow()

    private val _isDisplayedList: MutableStateFlow<Boolean> = MutableStateFlow(true)
    val isDisplayedList: StateFlow<Boolean> = _isDisplayedList.asStateFlow()

    private val productNameAndFilterPair = combine(
        _searchedProductName,
        _filterSelectedState
    ) { productName, filter ->

        filter?.let { firebaseAnalyticsManager.selectItemFilterEvent(it) }

        Pair(productName, filter)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    val product: Flow<PagingData<Product>> = productNameAndFilterPair.flatMapLatest {
        val productName = it.first
        val filter = it.second

        productRepository.products(
            search = productName.ifBlank { null },
            brand = filter?.category,
            lowest = filter?.lowestPrice,
            highest = filter?.highestPrice,
            sort = filter?.sort?.value
        )
    }.cachedIn(viewModelScope)

    fun toggleDisplayedList() {
        _isDisplayedList.value = !_isDisplayedList.value!!
    }

    fun setFilter(
        sortCategory: SortCategory?,
        brand: String?,
        lowestPrice: Int?,
        highestPrice: Int?
    ) {
        _filterSelectedState.value = FilterState(
            sortCategory,
            brand,
            lowestPrice,
            highestPrice
        )
    }

    fun searchProduct(productName: String) {
        if (productName.isNotBlank()) {
            firebaseAnalyticsManager.searchResultEvent(productName)
        }
        _searchedProductName.value = productName
    }

    private fun resetFilter() {
        _filterSelectedState.value = null
    }

    fun resetData() {
        _searchedProductName.value = ""
        resetFilter()
    }
}
