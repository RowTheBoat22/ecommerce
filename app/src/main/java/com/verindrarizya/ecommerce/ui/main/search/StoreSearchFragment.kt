package com.verindrarizya.ecommerce.ui.main.search

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.verindrarizya.ecommerce.databinding.FragmentStoreSearchBinding
import com.verindrarizya.ecommerce.utils.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StoreSearchFragment : Fragment() {

    private var _binding: FragmentStoreSearchBinding? = null
    private val binding: FragmentStoreSearchBinding
        get() = _binding!!

    private val viewModel: StoreSearchViewModel by viewModels()

    private val setSearchFragmentResult: (String) -> Unit = { productName: String ->
        requireActivity()
            .supportFragmentManager
            .setFragmentResult(REQUEST_KEY, bundleOf(RESULT_PRODUCT_NAME_KEY to productName))
        findNavController().navigateUp()
    }

    private val storeSearchAdapter: StoreSearchAdapter = StoreSearchAdapter { itemTitle: String ->
        setSearchFragmentResult(itemTitle)
    }

    private val args: StoreSearchFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStoreSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getProductNameFromArgs()
        requestSearchFocusOpenKeyboard()

        setRecyclerView()

        binding.edSearch.doOnTextChanged { text, _, _, _ ->
            viewModel.searchStore(text.toString())
            if (text.isNullOrBlank()) storeSearchAdapter.submitList(null)
        }

        viewModel.storeSearchResponse.observe(viewLifecycleOwner) {
            binding.progressBar.isVisible = it is Resource.Loading
            binding.rvSearch.isVisible = it !is Resource.Loading

            if (it is Resource.Success) {
                storeSearchAdapter.submitList(it.data.data)
            } else if (it is Resource.Failure) {
                storeSearchAdapter.submitList(null)
            }
        }

        setListenOnKeyEventSearchSubmit()
    }

    private fun setRecyclerView() {
        binding.rvSearch.layoutManager = LinearLayoutManager(context)
        binding.rvSearch.adapter = storeSearchAdapter
    }

    private fun setListenOnKeyEventSearchSubmit() {
        binding.edSearch.setOnEditorActionListener { textView, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideKeyboard()
                setSearchFragmentResult(textView.text.toString())
            }
            true
        }
    }

    private fun hideKeyboard() {
        // Get the activity context
        val context: Context = requireActivity()

        // Get the input method manager
        val inputMethodManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        // Hide the keyboard
        if (activity?.currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, 0)
        }
    }

    private fun requestSearchFocusOpenKeyboard() {
        binding.edSearch.requestFocus()
        val imgr =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imgr.showSoftInput(binding.edSearch, InputMethodManager.SHOW_IMPLICIT)
    }

    private fun getProductNameFromArgs() {
        val productName = args.productName
        binding.edSearch.setText(productName)
        viewModel.searchStore(productName)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val REQUEST_KEY = "store_search_fragment_reset_key"
        const val RESULT_PRODUCT_NAME_KEY = "result_product_name_key"
    }
}
