package com.verindrarizya.ecommerce.ui.main.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.ProductRepository
import com.verindrarizya.ecommerce.data.remote.response.StoreSearchResponse
import com.verindrarizya.ecommerce.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class StoreSearchViewModel @Inject constructor(
    private val productRepository: ProductRepository
) : ViewModel() {

    private val _storeSearchResponse: MutableLiveData<Resource<StoreSearchResponse>> =
        MutableLiveData()
    val storeSearchResponse: LiveData<Resource<StoreSearchResponse>>
        get() = _storeSearchResponse

    private var _storeSearchJob: Job? = null

    fun searchStore(productName: String) {
        _storeSearchJob?.cancel()
        if (productName.isBlank()) {
            if (_storeSearchJob?.isCancelled == true) {
                _storeSearchResponse.value =
                    Resource.Failure(Throwable())
            }
            return
        }
        _storeSearchJob = viewModelScope.launch {
            delay(2000)
            productRepository.search(productName).collect {
                _storeSearchResponse.value = it
            }
        }
    }
}
