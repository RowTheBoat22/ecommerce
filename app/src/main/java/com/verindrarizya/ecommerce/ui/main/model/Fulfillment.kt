package com.verindrarizya.ecommerce.ui.main.model

import android.os.Parcelable
import androidx.annotation.Keep
import com.verindrarizya.ecommerce.data.remote.response.FulfillmentData
import com.verindrarizya.ecommerce.data.remote.response.TransactionData
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class Fulfillment(
    val date: String,
    val total: Int,
    val invoiceId: String,
    val payment: String,
    val time: String,
    val status: Boolean,
    val rating: Int? = null,
    val review: String? = null
) : Parcelable

fun FulfillmentData.toFulfillment() = Fulfillment(
    date = date,
    total = total,
    invoiceId = invoiceId,
    payment = payment,
    time = payment,
    status = status
)

fun TransactionData.toFulfillment() = Fulfillment(
    date = date,
    total = total,
    invoiceId = invoiceId,
    payment = payment,
    time = time,
    status = status,
    rating = rating,
    review = review
)
