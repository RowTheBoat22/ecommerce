package com.verindrarizya.ecommerce.ui.main.dashboard.wishlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.databinding.FragmentWishlistBinding
import com.verindrarizya.ecommerce.ui.main.dashboard.DashboardFragmentDirections
import com.verindrarizya.ecommerce.utils.launchAndCollectIn
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class WishlistFragment : Fragment() {

    private var _binding: FragmentWishlistBinding? = null
    private val binding: FragmentWishlistBinding
        get() = _binding!!

    private val viewModel: WishlistViewModel by viewModels()

    private lateinit var rvAdapter: WishlistItemAdapter
    private lateinit var layoutManager: GridLayoutManager

    @Inject
    lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentWishlistBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRvAdapterAndLayoutManager()
        setProductRecyclerViewObserver()
        populateErrorLayout()

        viewModel.wishlistItemCount.launchAndCollectIn(viewLifecycleOwner) {
            binding.tvProductCount.text = getString(R.string.format_wishlist_item_count, it)
        }

        viewModel.allWishlist.launchAndCollectIn(viewLifecycleOwner) {
            setVisibilityItemCountAndDisplayToggle(isVisible = it.isNotEmpty())
            binding.errorLayout.root.isVisible = it.isEmpty()
            rvAdapter.submitList(it)
        }

        binding.ivListToggle.setOnClickListener {
            viewModel.toggleDisplayedList()
        }
    }

    private fun initRvAdapterAndLayoutManager() {
        rvAdapter = WishlistItemAdapter(
            onDeleteButtonClick = {
                firebaseAnalyticsManager.buttonClickEvent("button delete from wishlist")
                viewModel.deleteFromWishlist(it)
                Snackbar.make(
                    binding.anchor,
                    getString(R.string.deleted_from_wishlist),
                    Snackbar.LENGTH_SHORT
                )
                    .setAnchorView(binding.anchor)
                    .show()
            },
            onItemClick = {
                val parentNavController =
                    requireActivity().findNavController(R.id.nav_host_fragment)
                parentNavController.navigate(
                    DashboardFragmentDirections.actionDashboardFragmentToDetailFragment(it)
                )
            },
            onActionAddToCart = {
                firebaseAnalyticsManager.buttonClickEvent("button add to cart")
                viewModel.addToCart(it)
                Snackbar.make(
                    binding.anchor,
                    getString(R.string.added_to_cart),
                    Snackbar.LENGTH_SHORT
                )
                    .setAnchorView(binding.anchor)
                    .show()
            }
        )
        layoutManager = GridLayoutManager(context, 1)

        binding.rvWishlistProduct.adapter = rvAdapter
        binding.rvWishlistProduct.layoutManager = layoutManager
    }

    private fun setProductRecyclerViewObserver() {
        viewModel.isDisplayedList.observe(viewLifecycleOwner) { value ->
            if (value) {
                binding.ivListToggle.setBackgroundResource(R.drawable.format_list_bulleted)
                rvAdapter.setViewType(1)
                layoutManager.spanCount = 1
            } else {
                binding.ivListToggle.setBackgroundResource(R.drawable.grid_view)
                rvAdapter.setViewType(2)
                layoutManager.spanCount = 2
            }
        }
    }

    private fun setVisibilityItemCountAndDisplayToggle(isVisible: Boolean) {
        binding.ivListToggle.isVisible = isVisible
        binding.divider.isVisible = isVisible
        binding.tvProductCount.isVisible = isVisible
    }

    private fun populateErrorLayout() {
        binding.errorLayout.tvErrorTitle.text = getString(R.string.empty)
        binding.errorLayout.tvErrorDesc.text = getString(R.string.empty_description)
        binding.errorLayout.btnAction.visibility = View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
