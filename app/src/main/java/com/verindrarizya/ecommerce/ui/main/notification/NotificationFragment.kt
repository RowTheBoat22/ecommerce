package com.verindrarizya.ecommerce.ui.main.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.databinding.FragmentNotificationBinding
import com.verindrarizya.ecommerce.utils.launchAndCollectIn
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NotificationFragment : Fragment() {

    private var _binding: FragmentNotificationBinding? = null
    private val binding: FragmentNotificationBinding
        get() = _binding!!

    private val viewModel: NotificationViewModel by viewModels()

    private lateinit var rvAdapter: NotificationAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentNotificationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
        initErrorLayout()
        initRv()

        viewModel.allNotification.launchAndCollectIn(viewLifecycleOwner) { notificationList ->
            binding.rvNotification.isVisible = notificationList.isNotEmpty()
            binding.errorLayout.root.isVisible = notificationList.isEmpty()

            rvAdapter.submitList(notificationList)
        }
    }

    private fun initRv() {
        rvAdapter = NotificationAdapter(onActionItemClick = {
            viewModel.setReadNotification(it)
        })
        binding.rvNotification.adapter = rvAdapter
        binding.rvNotification.itemAnimator = null
        binding.rvNotification.layoutManager = LinearLayoutManager(context).apply {
            reverseLayout = true
            stackFromEnd = true
        }
    }

    private fun initErrorLayout() {
        binding.errorLayout.tvErrorTitle.text = getString(R.string.empty)
        binding.errorLayout.tvErrorDesc.text = getString(R.string.empty_description)
        binding.errorLayout.btnAction.visibility = View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
