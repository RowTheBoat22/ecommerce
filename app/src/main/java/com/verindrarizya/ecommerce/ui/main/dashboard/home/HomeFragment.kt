package com.verindrarizya.ecommerce.ui.main.dashboard.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.LocaleListCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject

enum class Language(val tag: String) {
    ENGLISH("en"), INDONESIA("in")
}

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    private val binding: FragmentHomeBinding
        get() = _binding!!

    private val viewModel: HomeViewModel by viewModels()

    @Inject
    lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnLogout.setOnClickListener {
            firebaseAnalyticsManager.buttonClickEvent("button logout")
            viewModel.logout()
        }

        binding.switchLanguage.setOnCheckedChangeListener { _, b ->
            val appLocale: LocaleListCompat = if (b) {
                LocaleListCompat.forLanguageTags(Language.INDONESIA.tag)
            } else {
                LocaleListCompat.forLanguageTags(Language.ENGLISH.tag)
            }

            AppCompatDelegate.setApplicationLocales(appLocale)
        }

        binding.switchDarkMode.setOnCheckedChangeListener { _, b ->
            viewModel.toggleDarkMode(b)
        }

        lifecycleScope.launch {
            binding.switchDarkMode.isChecked = viewModel.isDarkMode.first()
        }

        binding.switchLanguage.isChecked =
            AppCompatDelegate.getApplicationLocales().get(0)?.language == Language.INDONESIA.tag
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
