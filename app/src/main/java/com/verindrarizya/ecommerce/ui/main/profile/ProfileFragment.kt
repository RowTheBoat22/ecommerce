package com.verindrarizya.ecommerce.ui.main.profile

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.databinding.FragmentProfileBinding
import com.verindrarizya.ecommerce.utils.Resource
import com.verindrarizya.ecommerce.utils.getErrorResponse
import com.verindrarizya.ecommerce.utils.launchAndCollectIn
import com.verindrarizya.ecommerce.utils.showShortToast
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.HttpException
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment : Fragment() {

    private val viewModel: ProfileViewModel by viewModels()

    private var _binding: FragmentProfileBinding? = null
    private val binding
        get() = _binding!!

    private var imageUri: Uri? = null

    private val galleryActivityResultLauncher = registerForActivityResult(
        ActivityResultContracts.PickVisualMedia()
    ) { uri ->
        if (uri != null) {
            imageUri = uri
            imageUri?.let { viewModel.setImageUri(it) }
        }
    }

    private val cameraActivityResultLauncher = registerForActivityResult(
        ActivityResultContracts.TakePicture()
    ) { isSuccess: Boolean ->
        if (isSuccess) {
            imageUri?.let { viewModel.setImageUri(it) }
        }
    }

    @Inject
    lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.isUsernameSet.launchAndCollectIn(viewLifecycleOwner) {
            if (it == true) {
                findNavController().navigate(
                    ProfileFragmentDirections.actionGlobalDashboardFragment()
                )
            }
        }

        setListenerForTextField()
        setValidationForTextField()
        setButtonClickListener()
        setProfileViewClickListener()

        viewModel.imageProfileStringUri.observe(viewLifecycleOwner) {
            setProfileImage(it)
        }

        setProfileStateObserver()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setValidationForTextField() {
        viewModel.isNameValid.observe(viewLifecycleOwner) {
            binding.btnDone.isEnabled = it
        }
    }

    private fun setListenerForTextField() {
        binding.edName.doOnTextChanged { text, _, _, _ ->
            viewModel.checkName(text.toString())
        }
    }

    private fun setButtonClickListener() {
        binding.btnDone.setOnClickListener {
            firebaseAnalyticsManager.buttonClickEvent("button set profile")
            val userImageFile = getImageFile(imageUri)
            viewModel.setProfile(userImageFile)
        }
    }

    private fun setProfileViewClickListener() {
        binding.ivProfile.setOnClickListener {
            showDialog()
        }
    }

    private fun setProfileStateObserver() {
        viewModel.profileState.observe(viewLifecycleOwner) { resource ->
            binding.btnDone.isEnabled = resource !is Resource.Loading
            binding.progressBar.isVisible = resource is Resource.Loading

            if (resource is Resource.Success) {
                Toast.makeText(context, "Welcome!", Toast.LENGTH_SHORT).show()
            }

            if (resource is Resource.Failure) {
                when (val error = resource.throwable) {
                    is HttpException -> {
                        if (error.code() == 400) {
                            val errorResponse = error.getErrorResponse()
                            binding.tfName.error = errorResponse.message
                            context?.let { showShortToast(it, errorResponse.message) }
                        }
                    }

                    is IOException -> {
                        context?.let {
                            showShortToast(it, getString(R.string.your_connection_is_unavailable))
                        }
                    }

                    else -> {
                        context?.let {
                            showShortToast(
                                it,
                                getString(R.string.general_error_statement)
                            )
                        }
                    }
                }
            }
        }
    }

    private fun showDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.profile_image_dialog_title))
            .setItems(
                arrayOf(
                    resources.getString(R.string.camera),
                    resources.getString(R.string.gallery)
                )
            ) { _, which ->
                when (which) {
                    // camera
                    0 -> {
                        val tempFile = File.createTempFile(
                            "tmp_image_profile",
                            ".png",
                            requireActivity().filesDir
                        ).apply {
                            createNewFile()
                            deleteOnExit()
                        }
                        imageUri = FileProvider.getUriForFile(
                            requireContext(),
                            "photoImage.provider",
                            tempFile
                        )
                        cameraActivityResultLauncher.launch(imageUri)
                    }
                    // something else, in this case the gallery
                    else -> {
                        val mediaType = ActivityResultContracts.PickVisualMedia.ImageOnly
                        val request = PickVisualMediaRequest.Builder()
                            .setMediaType(mediaType)
                            .build()
                        galleryActivityResultLauncher.launch(request)
                    }
                }
            }
            .show()
    }

    private fun setProfileImage(uriString: String) {
        val uri = Uri.parse(uriString)
        binding.icProfile.isVisible = false
        Glide.with(requireContext())
            .load(uri)
            .into(binding.ivProfile)
    }

    private fun getImageFile(uri: Uri?): File? {
        if (uri == null) {
            return null
        }

        val applicationContext = requireActivity().applicationContext
        val parcelFileDescriptor = applicationContext.contentResolver.openFileDescriptor(
            uri,
            "r",
            null
        )

        val file = File(
            applicationContext.cacheDir,
            applicationContext.contentResolver.getFileName(uri)
        )

        val inputStream = FileInputStream(parcelFileDescriptor?.fileDescriptor)
        val outputStream = FileOutputStream(file)
        inputStream.copyTo(outputStream)
        parcelFileDescriptor?.close()
        return file
    }

    @SuppressLint("Range")
    private fun ContentResolver.getFileName(uri: Uri): String {
        var name = ""
        val cursor = query(uri, null, null, null, null)
        cursor?.use {
            it.moveToFirst()
            name = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
        }
        return name
    }
}
