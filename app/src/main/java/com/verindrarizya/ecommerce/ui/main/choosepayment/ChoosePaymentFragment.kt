package com.verindrarizya.ecommerce.ui.main.choosepayment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.remoteconfig.ConfigUpdate
import com.google.firebase.remoteconfig.ConfigUpdateListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigException
import com.verindrarizya.ecommerce.databinding.FragmentChoosePaymentBinding
import com.verindrarizya.ecommerce.ui.main.choosepayment.adapter.PaymentSectionItemAdapter
import com.verindrarizya.ecommerce.ui.main.model.toPayment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ChoosePaymentFragment : Fragment() {

    private var _binding: FragmentChoosePaymentBinding? = null
    private val binding: FragmentChoosePaymentBinding
        get() = _binding!!

    private val viewModel: ChoosePaymentViewModel by viewModels()

    private lateinit var rvAdapter: PaymentSectionItemAdapter

    @Inject
    lateinit var firebaseRemoteConfig: FirebaseRemoteConfig

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChoosePaymentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        firebaseRemoteConfig.addOnConfigUpdateListener(object : ConfigUpdateListener {
            override fun onUpdate(configUpdate: ConfigUpdate) {
                Log.d(TAG, "Updated keys: " + configUpdate.updatedKeys)

                if (configUpdate.updatedKeys.contains("payment")) {
                    firebaseRemoteConfig.fetchAndActivate().addOnCompleteListener {
                        if (it.isSuccessful) {
                            Log.d(TAG, "Config params updated: " + it.result)
                            viewModel.getPayment()
                        } else {
                            Log.d(TAG, "Config params update failed: " + it.exception)
                            context?.let { context ->
                                Toast.makeText(
                                    context,
                                    "Failed to get newest payment",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                }
            }

            override fun onError(error: FirebaseRemoteConfigException) {
                Log.w(TAG, "Config update error with code: " + error.code, error)
            }
        })

        setToolbarAction()
        initRvAdapterAndLayoutManager()

        viewModel.payments.observe(viewLifecycleOwner) {
            rvAdapter.submitList(it)
        }
    }

    private fun initRvAdapterAndLayoutManager() {
        rvAdapter = PaymentSectionItemAdapter(
            onPaymentItemClick = {
                val bundleResult = Bundle().apply {
                    putParcelable(PAYMENT_RESULT_KEY, it.toPayment())
                }
                setFragmentResult(REQUEST_PAYMENT_KEY, bundleResult)

                findNavController().navigateUp()
            }
        )

        binding.rvPayment.layoutManager = LinearLayoutManager(context)
        binding.rvPayment.adapter = rvAdapter
    }

    private fun setToolbarAction() {
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val REQUEST_PAYMENT_KEY = "request_payment_key"
        const val PAYMENT_RESULT_KEY = "payment_result_key"
        private const val TAG = "RemoteConfigTag"
    }
}
