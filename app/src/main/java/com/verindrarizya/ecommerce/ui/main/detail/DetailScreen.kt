package com.verindrarizya.ecommerce.ui.main.detail

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material.icons.filled.Share
import androidx.compose.material3.Badge
import androidx.compose.material3.BadgedBox
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.InputChip
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SuggestionChip
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.remote.response.ProductDetail
import com.verindrarizya.ecommerce.data.remote.response.ProductDetailResponse
import com.verindrarizya.ecommerce.data.remote.response.ProductVariantItem
import com.verindrarizya.ecommerce.ui.theme.ECommerceTheme
import com.verindrarizya.ecommerce.ui.theme.ErrorLayout
import com.verindrarizya.ecommerce.ui.theme.Poppins
import com.verindrarizya.ecommerce.ui.theme.textStyleRemoveFontPadding
import com.verindrarizya.ecommerce.utils.CurrencyHelper
import com.verindrarizya.ecommerce.utils.Resource
import kotlinx.coroutines.launch

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun DetailScreen(
    modifier: Modifier = Modifier,
    appBarTitle: String = stringResource(R.string.product_detail),
    resourceProductResponse: Resource<ProductDetailResponse>,
    onNavigationIconClick: () -> Unit,
    onNotificationIconClick: () -> Unit,
    onCartIconClick: () -> Unit,
    onMenuIconClick: () -> Unit,
    onButtonBuyNowClick: () -> Unit,
    onButtonAddToCartClick: () -> Unit,
    onButtonReviewSeeAllClick: () -> Unit,
    onShareClicked: (String, String, String) -> Unit,
    wishlistChecked: Boolean,
    onWishlistCheckedChange: () -> Unit,
    selectedProductVariantItem: ProductVariantItem,
    onSelectedProductVariantItemChange: (ProductVariantItem) -> Unit,
    cartProductCount: Int?,
    unreadNotifCount: Int?,
    onRefreshClick: () -> Unit
) {
    val snackbarHostState = remember { SnackbarHostState() }
    val scope = rememberCoroutineScope()
    val context = LocalContext.current

    Scaffold(
        modifier = modifier,
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) },
        topBar = {
            DetailAppBar(
                title = appBarTitle,
                onNavigationIconClick = onNavigationIconClick,
                onNotificationIconClick = onNotificationIconClick,
                onCartIconClick = onCartIconClick,
                onMenuIconClick = onMenuIconClick,
                cartProductCount = cartProductCount,
                unreadNotifCount = unreadNotifCount
            )
        },
        bottomBar = {
            if (resourceProductResponse is Resource.Success) {
                BottomButtonMenu(
                    onButtonBuyNowClick = onButtonBuyNowClick,
                    onButtonAddToCartClick = {
                        onButtonAddToCartClick()
                        scope.launch {
                            snackbarHostState.showSnackbar(context.getString(R.string.added_to_cart))
                        }
                    }
                )
            }
        }
    ) { paddingValues: PaddingValues ->
        when (resourceProductResponse) {
            is Resource.Failure -> {
                val error = resourceProductResponse.throwable

                Box(
                    modifier = Modifier
                        .padding(paddingValues)
                        .fillMaxSize(),
                    contentAlignment = Alignment.Center
                ) {
                    ErrorLayout(
                        error = error,
                        buttonText = stringResource(R.string.refresh),
                        onRetryClick = onRefreshClick
                    )
                }
            }

            is Resource.Loading -> {
                Box(
                    modifier = modifier
                        .fillMaxSize()
                        .padding(paddingValues),
                    contentAlignment = Alignment.Center
                ) {
                    CircularProgressIndicator()
                }
            }

            is Resource.Success -> {
                val productDetail = resourceProductResponse.data.data
                Column(
                    modifier = modifier
                        .fillMaxSize()
                        .padding(paddingValues)
                        .verticalScroll(rememberScrollState())
                ) {
                    ImageProductPager(
                        productImages = productDetail.image
                    )
                    TitleProductSection(
                        modifier = Modifier.fillMaxWidth(),
                        wishlistChecked = wishlistChecked,
                        onWishlistCheckedChange = {
                            scope.launch {
                                snackbarHostState.showSnackbar(
                                    message = if (wishlistChecked) {
                                        context.getString(R.string.deleted_from_wishlist)
                                    } else {
                                        context.getString(R.string.saved_to_wishlist)
                                    }
                                )
                            }
                            onWishlistCheckedChange()
                        },
                        onShareClicked = onShareClicked,
                        productName = productDetail.productName,
                        productRating = productDetail.productRating,
                        totalReview = productDetail.totalReview,
                        productPrice = productDetail.productPrice + selectedProductVariantItem.variantPrice,
                        productSold = productDetail.sale,
                        productId = productDetail.productId
                    )
                    Divider(
                        modifier = Modifier.fillMaxWidth(),
                    )
                    VariantProductSection(
                        modifier = Modifier.fillMaxWidth(),
                        sectionTitleText = stringResource(R.string.choose_variant),
                        productVariantItems = productDetail.productVariant,
                        selectedProductVariantItem = selectedProductVariantItem,
                        onSelectedProductVariantItemChange = onSelectedProductVariantItemChange
                    )
                    Divider(
                        modifier = Modifier.fillMaxWidth(),
                    )
                    DescriptionProductSection(
                        modifier = Modifier.fillMaxWidth(),
                        productDescription = productDetail.description
                    )
                    Divider(
                        modifier = Modifier.fillMaxWidth(),
                    )
                    ReviewProductSection(
                        modifier = Modifier.fillMaxWidth(),
                        totalReview = productDetail.totalReview,
                        productRating = productDetail.productRating,
                        totalSatisfaction = productDetail.totalSatisfaction,
                        totalRating = productDetail.totalRating,
                        onButtonReviewSeeAllClick = onButtonReviewSeeAllClick
                    )
                }
            }
        }
    }
}

@Composable
@OptIn(ExperimentalMaterial3Api::class)
fun DetailAppBar(
    modifier: Modifier = Modifier,
    title: String,
    onNavigationIconClick: () -> Unit,
    onNotificationIconClick: () -> Unit,
    onCartIconClick: () -> Unit,
    onMenuIconClick: () -> Unit,
    cartProductCount: Int?,
    unreadNotifCount: Int?
) {
    val badgeModifier = Modifier
        .size(16.dp)
        .offset(x = (-8).dp, y = 8.dp)

    Column(modifier = modifier) {
        TopAppBar(
            modifier = modifier,
            title = {
                Text(
                    text = title,
                    fontFamily = Poppins,
                    fontSize = 22.sp,
                    fontWeight = FontWeight.Normal,
                    style = textStyleRemoveFontPadding
                )
            },
            navigationIcon = {
                IconButton(onClick = onNavigationIconClick) {
                    Icon(
                        painter = painterResource(R.drawable.arrow_back),
                        contentDescription = "Back"
                    )
                }
            },
            actions = {
                IconButton(onClick = onNotificationIconClick) {
                    BadgedBox(
                        badge = {
                            if (unreadNotifCount != null && unreadNotifCount != 0) {
                                Badge(
                                    modifier = badgeModifier,
                                ) {
                                    Text(
                                        text = unreadNotifCount.toString(),
                                        fontFamily = Poppins,
                                    )
                                }
                            }
                        },
                        content = {
                            Icon(
                                painter = painterResource(R.drawable.ic_notification),
                                contentDescription = "Notification"
                            )
                        }
                    )
                }
                IconButton(onClick = onCartIconClick) {
                    BadgedBox(
                        badge = {
                            if (cartProductCount != null && cartProductCount != 0) {
                                Badge(
                                    modifier = badgeModifier,
                                ) {
                                    Text(
                                        text = cartProductCount.toString(),
                                        fontFamily = Poppins,
                                    )
                                }
                            }
                        },
                        content = {
                            Icon(
                                painter = painterResource(R.drawable.ic_shopping_cart),
                                contentDescription = "Cart"
                            )
                        }
                    )
                }
                IconButton(onClick = onMenuIconClick) {
                    Icon(
                        painter = painterResource(R.drawable.ic_menu),
                        contentDescription = "Menu"
                    )
                }
            }
        )
        Divider()
    }
}

@Composable
fun BottomButtonMenu(
    modifier: Modifier = Modifier,
    onButtonBuyNowClick: () -> Unit,
    onButtonAddToCartClick: () -> Unit,
    buyNowButtonText: String = stringResource(R.string.buy_now),
    addToCartButtonText: String = stringResource(R.string.add_to_cart)
) {
    Column(
        modifier = modifier
    ) {
        Divider()
        Row(
            modifier = modifier
                .padding(
                    vertical = 8.dp,
                    horizontal = 16.dp
                )
        ) {
            OutlinedButton(
                modifier = Modifier.weight(1f),
                onClick = onButtonBuyNowClick
            ) {
                Text(
                    text = buyNowButtonText,
                    fontFamily = Poppins,
                    fontWeight = FontWeight.Medium
                )
            }
            Spacer(modifier = Modifier.width(16.dp))
            Button(
                modifier = Modifier.weight(1f),
                onClick = onButtonAddToCartClick
            ) {
                Text(
                    text = addToCartButtonText,
                    fontFamily = Poppins,
                    fontWeight = FontWeight.Medium
                )
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ImageProductPager(
    modifier: Modifier = Modifier,
    pagerState: PagerState = rememberPagerState(),
    productImages: List<String>,
    pageCount: Int = productImages.size
) {
    Box(modifier = modifier) {
        HorizontalPager(
            pageCount = pageCount,
            state = pagerState,
            modifier = Modifier.height(400.dp)
        ) {
            AsyncImage(
                modifier = Modifier.fillMaxSize(),
                model = productImages[it],
                placeholder = painterResource(R.drawable.image_placeholder),
                contentDescription = "Product Image",
                contentScale = ContentScale.Crop,
            )
        }
        if (pageCount > 1) {
            Row(
                Modifier
                    .padding(bottom = 16.dp)
                    .fillMaxWidth()
                    .align(Alignment.BottomCenter),
                horizontalArrangement = Arrangement.Center
            ) {
                repeat(pageCount) { iteration ->
                    val color =
                        if (pagerState.currentPage == iteration) colorResource(R.color.phin_purple) else Color.LightGray
                    Box(
                        modifier = Modifier
                            .padding(4.dp)
                            .clip(CircleShape)
                            .background(color)
                            .size(8.dp)

                    )
                }
            }
        }
    }
}

@Composable
fun TitleProductSection(
    modifier: Modifier = Modifier,
    wishlistChecked: Boolean,
    onWishlistCheckedChange: () -> Unit,
    onShareClicked: (String, String, String) -> Unit,
    productName: String,
    productPrice: Int,
    productSold: Int,
    productRating: Double,
    productId: String,
    totalReview: Int
) {
    Column(
        modifier = modifier
            .padding(
                vertical = 12.dp,
                horizontal = 16.dp
            )
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = CurrencyHelper.convertToRupiah(productPrice),
                fontSize = 20.sp,
                fontFamily = Poppins,
                fontWeight = FontWeight.SemiBold,
                style = textStyleRemoveFontPadding
            )
            Spacer(modifier = Modifier.weight(1f))
            Icon(
                modifier = Modifier.clickable {
                    onShareClicked(
                        productName,
                        CurrencyHelper.convertToRupiah(productPrice),
                        productId
                    )
                },
                imageVector = Icons.Filled.Share,
                contentDescription = "Share"
            )
            Spacer(modifier = Modifier.width(8.dp))
            Box(
                modifier = Modifier.clickable {
                    onWishlistCheckedChange()
                }
            ) {
                if (wishlistChecked) {
                    Icon(Icons.Filled.Favorite, contentDescription = "Favorite")
                } else {
                    Icon(Icons.Filled.FavoriteBorder, contentDescription = "Favorite")
                }
            }
        }
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = productName,
            fontSize = 14.sp,
            fontFamily = Poppins,
            fontWeight = FontWeight.Normal,
            style = textStyleRemoveFontPadding
        )
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = stringResource(R.string.item_sold, productSold),
                fontSize = 12.sp,
                fontFamily = Poppins,
                fontWeight = FontWeight.Normal,
                style = textStyleRemoveFontPadding
            )
            Spacer(modifier = Modifier.width(8.dp))
            SuggestionChip(
                modifier = Modifier,
                onClick = {},
                label = {
                    Text(
                        text = "$productRating ($totalReview)",
                        fontSize = 12.sp,
                        fontFamily = Poppins,
                        fontWeight = FontWeight.Normal,
                        style = textStyleRemoveFontPadding
                    )
                },
                icon = {
                    Icon(
                        painter = painterResource(R.drawable.star),
                        contentDescription = null,
                        modifier = Modifier.size(15.dp),
                    )
                }
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun VariantProductSection(
    modifier: Modifier = Modifier,
    sectionTitleText: String,
    productVariantItems: List<ProductVariantItem>,
    selectedProductVariantItem: ProductVariantItem,
    onSelectedProductVariantItemChange: (ProductVariantItem) -> Unit
) {
    Column(
        modifier = modifier
            .padding(
                vertical = 12.dp,
                horizontal = 16.dp
            )
    ) {
        Text(
            text = sectionTitleText,
            fontSize = 16.sp,
            fontFamily = Poppins,
            fontWeight = FontWeight.Medium,
            style = textStyleRemoveFontPadding
        )
        LazyRow {
            items(productVariantItems) { productVariantItem ->
                InputChip(
                    selected = selectedProductVariantItem == productVariantItem,
                    onClick = {
                        onSelectedProductVariantItemChange(productVariantItem)
                    },
                    label = {
                        Text(
                            text = productVariantItem.variantName,
                            fontSize = 12.sp,
                            fontWeight = FontWeight.Normal,
                            fontFamily = Poppins,
                            style = textStyleRemoveFontPadding
                        )
                    }
                )
                Spacer(modifier = Modifier.width(8.dp))
            }
        }
    }
}

@Composable
fun DescriptionProductSection(
    modifier: Modifier = Modifier,
    productDescription: String
) {
    Column(
        modifier = modifier.padding(
            vertical = 12.dp,
            horizontal = 16.dp
        )
    ) {
        Text(
            text = stringResource(R.string.product_description),
            fontSize = 16.sp,
            fontFamily = Poppins,
            fontWeight = FontWeight.Medium,
            style = textStyleRemoveFontPadding
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = productDescription,
            fontSize = 14.sp,
            textAlign = TextAlign.Justify,
            fontFamily = Poppins,
            fontWeight = FontWeight.Normal,
            style = textStyleRemoveFontPadding
        )
    }
}

@Composable
fun ReviewProductSection(
    modifier: Modifier = Modifier,
    productRating: Double,
    totalSatisfaction: Int,
    totalRating: Int,
    totalReview: Int,
    onButtonReviewSeeAllClick: () -> Unit
) {
    Column(
        modifier = modifier
            .padding(
                vertical = 12.dp,
                horizontal = 16.dp
            )
    ) {
        // Sub-Title
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = stringResource(R.string.buyer_reviews),
                fontSize = 16.sp,
                fontFamily = Poppins,
                fontWeight = FontWeight.Medium,
                style = textStyleRemoveFontPadding
            )
            Spacer(modifier = Modifier.weight(1f))
            Text(
                text = stringResource(R.string.see_all),
                fontSize = 12.sp,
                fontFamily = Poppins,
                fontWeight = FontWeight.Medium,
                style = textStyleRemoveFontPadding,
                color = if (isSystemInDarkTheme()) {
                    MaterialTheme.colorScheme.primary
                } else {
                    colorResource(id = R.color.phin_purple)
                },
                modifier = Modifier
                    .clickable {
                        onButtonReviewSeeAllClick()
                    }
            )
        }
        Spacer(modifier = Modifier.height(8.dp))
        // Sub-Content
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                modifier = Modifier
                    .size(20.dp),
                painter = painterResource(R.drawable.star),
                contentDescription = null,
            )
            Spacer(modifier = Modifier.width(4.dp))
            Text(
                buildAnnotatedString {
                    withStyle(
                        style = SpanStyle(
                            fontSize = 20.sp,
                            fontFamily = Poppins,
                            fontWeight = FontWeight.SemiBold,
                        )
                    ) {
                        append(productRating.toString())
                    }
                    withStyle(
                        style = SpanStyle(
                            fontSize = 10.sp,
                            fontFamily = Poppins,
                            fontWeight = FontWeight.Normal
                        )
                    ) {
                        append("/5.0")
                    }
                },
                style = textStyleRemoveFontPadding
            )
            Spacer(modifier = Modifier.width(32.dp))
            Column {
                Text(
                    text = stringResource(R.string.customer_satisfaction_rate, totalSatisfaction),
                    fontSize = 12.sp,
                    fontFamily = Poppins,
                    fontWeight = FontWeight.SemiBold,
                    style = textStyleRemoveFontPadding
                )
                Text(
                    text = stringResource(
                        R.string.rating_review_total_count,
                        totalRating,
                        totalReview
                    ),
                    fontSize = 12.sp,
                    fontFamily = Poppins,
                    fontWeight = FontWeight.Normal,
                    style = textStyleRemoveFontPadding
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DetailScreenSuccessPreview() {
    var wishlistChecked by remember { mutableStateOf(false) }
    var selectedProductVariantItem by remember { mutableStateOf(DataDummy.productVariantItems[0]) }

    ECommerceTheme {
        DetailScreen(
            modifier = Modifier.fillMaxSize(),
            resourceProductResponse = Resource.Success(DataDummy.productDetailResponse),
            onNavigationIconClick = {},
            onNotificationIconClick = {},
            onCartIconClick = {},
            onMenuIconClick = {},
            onButtonAddToCartClick = {},
            onButtonBuyNowClick = {},
            wishlistChecked = wishlistChecked,
            onWishlistCheckedChange = { wishlistChecked = !wishlistChecked },
            selectedProductVariantItem = selectedProductVariantItem,
            onSelectedProductVariantItemChange = { selectedProductVariantItem = it },
            onButtonReviewSeeAllClick = {},
            cartProductCount = 1,
            unreadNotifCount = 4,
            onRefreshClick = {},
            onShareClicked = { _, _, _ -> }
        )
    }
}

// @Preview(showBackground = true)
// @Composable
// fun DetailScreenLoadingPreview() {
//    var wishlistChecked by remember { mutableStateOf(false) }
//    var selectedProductVariantItem by remember { mutableStateOf(DataDummy.productVariantItems[0]) }
//
//    DetailScreen(
//        modifier = Modifier.fillMaxSize(),
//        resourceProductResponse = Resource.Loading,
//        onNavigationIconClick = {},
//        onNotificationIconClick = {},
//        onCartIconClick = {},
//        onMenuIconClick = {},
//        onButtonAddToCartClick = {},
//        onButtonBuyNowClick = {},
//        wishlistChecked = wishlistChecked,
//        onWishlistCheckedChange = { wishlistChecked = !wishlistChecked },
//        selectedProductVariantItem = selectedProductVariantItem,
//        onSelectedProductVariantItemChange = { selectedProductVariantItem = it },
//        onButtonReviewSeeAllClick = {}
//    )
// }

object DataDummy {

    val productVariantItems: List<ProductVariantItem> = listOf(
        ProductVariantItem(
            variantName = "8gb",
            variantPrice = 2000,
        ),
        ProductVariantItem(
            variantName = "16gb",
            variantPrice = 3000,
        )
    )

    val productDetail = ProductDetail(
        productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292",
        productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
        productPrice = 23499000,
        image = listOf(
            "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
            "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
            "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
        ),
        brand = "ASUS",
        description = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\\n\\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\\nGraphics Memory : 6GB GDDR6\\nDiscrete/Optimus : MUX Switch + Optimus\\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
        store = "AsusStore",
        sale = 12,
        stock = 2,
        totalRating = 7,
        totalReview = 5,
        totalSatisfaction = 100,
        productRating = 4.5,
        productVariant = productVariantItems
    )

    val productDetailResponse = ProductDetailResponse(
        code = 200,
        message = "OK",
        data = productDetail
    )
}
