package com.verindrarizya.ecommerce.ui.main.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.databinding.OnBoardingItemBinding

class DetailPagerAdapter(
    private val detailProductImages: List<String>
) : RecyclerView.Adapter<DetailPagerAdapter.ViewHolder>() {

    class ViewHolder(private val binding: OnBoardingItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(imageUrl: String) {
            Glide.with(itemView.context)
                .load(imageUrl)
                .placeholder(R.drawable.image_placeholder)
                .into(binding.ivOnboard)

            binding.ivOnboard.scaleType = ImageView.ScaleType.FIT_XY
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            OnBoardingItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = detailProductImages.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(detailProductImages[position])
    }
}
