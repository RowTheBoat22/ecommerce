package com.verindrarizya.ecommerce.ui.main.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.databinding.FragmentDashboardBinding
import com.verindrarizya.ecommerce.utils.launchAndCollectIn
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardFragment : Fragment() {

    private var _binding: FragmentDashboardBinding? = null

    private val binding: FragmentDashboardBinding
        get() = _binding!!

    private val viewModel: DashboardViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navHostFragment =
            childFragmentManager.findFragmentById(R.id.dashboard_nav_host) as NavHostFragment
        val navController = navHostFragment.navController

        binding.bottomNav?.setupWithNavController(navController)
        binding.navRail?.setupWithNavController(navController)
        binding.navView?.setupWithNavController(navController)

        setToolbarMenuAndAction()

        viewModel.username.launchAndCollectIn(viewLifecycleOwner) {
            binding.toolbar.title = it
        }

        mutateBadge()
    }

    private fun setToolbarMenuAndAction() {
        binding.toolbar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.menu_notification -> {
                    findNavController().navigate(
                        DashboardFragmentDirections.actionGlobalNotificationFragment()
                    )
                    true
                }

                R.id.menu_cart -> {
                    findNavController().navigate(
                        DashboardFragmentDirections.actionGlobalCartFragment()
                    )
                    true
                }

                else -> false
            }
        }
    }

    @androidx.annotation.OptIn(com.google.android.material.badge.ExperimentalBadgeUtils::class)
    private fun mutateBadge() {
        val cartBadge: BadgeDrawable =
            BadgeDrawable.create(requireContext()).apply { isVisible = false }
        val notificationBadge: BadgeDrawable =
            BadgeDrawable.create(requireContext()).apply { isVisible = false }

        val wishlistBadge = if (binding.bottomNav != null) {
            binding.bottomNav?.getOrCreateBadge(R.id.wishlistFragment)
        } else if (binding.navRail != null) {
            binding.navRail?.getOrCreateBadge(R.id.wishlistFragment)
        } else {
            null
        }

        BadgeUtils.attachBadgeDrawable(notificationBadge, binding.toolbar, R.id.menu_notification)
        BadgeUtils.attachBadgeDrawable(cartBadge, binding.toolbar, R.id.menu_cart)

        viewModel.cartProductCount.launchAndCollectIn(viewLifecycleOwner) { count ->
            cartBadge.isVisible = count != 0
            cartBadge.number = count
        }

        viewModel.unreadNotif.launchAndCollectIn(viewLifecycleOwner) { count ->
            notificationBadge.isVisible = count != 0
            notificationBadge.number = count
        }

        viewModel.wishlistProductCount.launchAndCollectIn(viewLifecycleOwner) { count: Int ->
            wishlistBadge?.isVisible = count != 0
            wishlistBadge?.number = count
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
