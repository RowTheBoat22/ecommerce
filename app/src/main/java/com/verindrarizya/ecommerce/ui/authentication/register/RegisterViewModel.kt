package com.verindrarizya.ecommerce.ui.authentication.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.AuthRepository
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.data.PreferencesRepository
import com.verindrarizya.ecommerce.data.remote.response.AuthResponse
import com.verindrarizya.ecommerce.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val preferencesRepository: PreferencesRepository,
    private val firebaseAnalyticsManager: FirebaseAnalyticsManager
) : ViewModel() {

    private val _isEmailValid: MutableLiveData<Boolean> = MutableLiveData()
    val isEmailValid: LiveData<Boolean>
        get() = _isEmailValid

    private val _isPasswordValid: MutableLiveData<Boolean> = MutableLiveData()
    val isPasswordValid: LiveData<Boolean>
        get() = _isPasswordValid

    val isButtonEnabled: MediatorLiveData<Boolean> = MediatorLiveData()

    private val _registerState: MutableLiveData<Resource<AuthResponse>> = MutableLiveData()
    val registerState: LiveData<Resource<AuthResponse>>
        get() = _registerState

    private val emailPattern = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}\$"

    init {
        isButtonEnabled.addSource(isEmailValid) {
            isButtonEnabled.value = it && isPasswordValid.value == true
        }
        isButtonEnabled.addSource(isPasswordValid) {
            isButtonEnabled.value = it && isEmailValid.value == true
        }
    }

    fun checkEmailValid(email: String) {
        _isEmailValid.value = email.isNotBlank() && email.matches(emailPattern.toRegex())
    }

    fun checkPassword(password: String) {
        _isPasswordValid.value = password.length >= 8 && password.isNotBlank()
    }

    fun register(email: String, password: String) {
        firebaseAnalyticsManager.registerEvent()
        viewModelScope.launch {
            authRepository.register(email, password).collect { resource ->
                if (resource is Resource.Success) {
                    runBlocking {
                        preferencesRepository.apply {
                            setAccessToken(resource.data.data.accessToken)
                            setRefreshToken(resource.data.data.refreshToken)
                        }
                    }
                }
                _registerState.value = resource
            }
        }
    }
}
