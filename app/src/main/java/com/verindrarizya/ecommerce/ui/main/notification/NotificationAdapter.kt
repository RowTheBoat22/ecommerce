package com.verindrarizya.ecommerce.ui.main.notification

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.color.MaterialColors
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.local.entity.NotificationEntity
import com.verindrarizya.ecommerce.databinding.NotificationItemBinding

class NotificationAdapter(
    private val onActionItemClick: (NotificationEntity) -> Unit
) : ListAdapter<NotificationEntity, NotificationAdapter.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            NotificationItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val binding: NotificationItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(notificationEntity: NotificationEntity) {
            if (!notificationEntity.isRead) {
                itemView.setBackgroundColor(
                    MaterialColors.getColor(
                        itemView,
                        com.google.android.material.R.attr.colorPrimaryContainer
                    )
                )
            } else {
                itemView.background = null
            }

            Glide.with(itemView.context)
                .load(notificationEntity.image)
                .placeholder(R.drawable.image_placeholder)
                .centerCrop()
                .into(binding.ivNotif)

            binding.tvDateTime.text = "${notificationEntity.date}, ${notificationEntity.time}"
            binding.tvType.text = notificationEntity.type
            binding.tvTitle.text = notificationEntity.title
            binding.tvDesc.text = notificationEntity.desc

            itemView.setOnClickListener { onActionItemClick(notificationEntity) }
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<NotificationEntity>() {
            override fun areItemsTheSame(
                oldItem: NotificationEntity,
                newItem: NotificationEntity
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: NotificationEntity,
                newItem: NotificationEntity
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}
