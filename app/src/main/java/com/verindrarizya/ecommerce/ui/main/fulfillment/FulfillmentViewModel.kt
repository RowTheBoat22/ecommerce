package com.verindrarizya.ecommerce.ui.main.fulfillment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.ProductRepository
import com.verindrarizya.ecommerce.data.remote.response.RatingResponse
import com.verindrarizya.ecommerce.ui.main.model.Fulfillment
import com.verindrarizya.ecommerce.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FulfillmentViewModel @Inject constructor(
    private val productRepository: ProductRepository,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _ratingFlow: MutableLiveData<Resource<RatingResponse>> = MutableLiveData()
    val ratingFlow: LiveData<Resource<RatingResponse>>
        get() = _ratingFlow

    private val _fulfillmentData: MutableLiveData<Fulfillment> = MutableLiveData()
    val fulfillmentData: LiveData<Fulfillment>
        get() = _fulfillmentData

    init {
        populate()
    }

    private fun populate() {
        val fulfillment = savedStateHandle.get<Fulfillment>("fulfillment")
        fulfillment?.let { _fulfillmentData.value = it }
    }

    fun updateRating(newRating: Int) {
        val currentState = fulfillmentData.value
        currentState?.let {
            _fulfillmentData.value = currentState.copy(rating = newRating)
        }
    }

    fun postRating(review: String) {
        val fulfillmentData = fulfillmentData.value
        fulfillmentData?.let {
            viewModelScope.launch {
                productRepository.rating(
                    invoiceId = it.invoiceId,
                    rating = it.rating ?: 0,
                    review = review
                ).collect {
                    _ratingFlow.value = it
                }
            }
        }
    }
}
