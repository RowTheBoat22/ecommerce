package com.verindrarizya.ecommerce.ui.main.dashboard.wishlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.CartProductRepository
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.data.WishlistProductRepository
import com.verindrarizya.ecommerce.data.local.entity.WishlistProductEntity
import com.verindrarizya.ecommerce.data.local.entity.toCartProduct
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WishlistViewModel @Inject constructor(
    private val wishlistProductRepository: WishlistProductRepository,
    private val cartProductRepository: CartProductRepository,
    private val firebaseAnalyticsManager: FirebaseAnalyticsManager
) : ViewModel() {

    private val _isDisplayedList: MutableLiveData<Boolean> = MutableLiveData(true)
    val isDisplayedList: LiveData<Boolean>
        get() = _isDisplayedList

    val allWishlist = wishlistProductRepository.getAllWishlistProduct()

    val wishlistItemCount = allWishlist.map { it.size }

    fun deleteFromWishlist(product: WishlistProductEntity) {
        viewModelScope.launch {
            wishlistProductRepository.deleteFromWishlist(product)
        }
    }

    fun addToCart(product: WishlistProductEntity) {
        viewModelScope.launch {
            val cartProductEntity = product.toCartProduct()
            firebaseAnalyticsManager.addToCartEvent(cartProductEntity)
            cartProductRepository.insertToCart(cartProductEntity)
        }
    }

    fun toggleDisplayedList() {
        _isDisplayedList.value = !_isDisplayedList.value!!
    }
}
