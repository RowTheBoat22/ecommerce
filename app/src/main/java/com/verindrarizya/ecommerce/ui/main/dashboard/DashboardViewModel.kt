package com.verindrarizya.ecommerce.ui.main.dashboard

import androidx.lifecycle.ViewModel
import com.verindrarizya.ecommerce.data.CartProductRepository
import com.verindrarizya.ecommerce.data.NotifRepository
import com.verindrarizya.ecommerce.data.PreferencesRepository
import com.verindrarizya.ecommerce.data.WishlistProductRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(
    preferencesRepository: PreferencesRepository,
    wishlistProductRepository: WishlistProductRepository,
    cartProductRepository: CartProductRepository,
    notifRepository: NotifRepository
) : ViewModel() {

    val wishlistProductCount = wishlistProductRepository.getAllWishlistProduct().map {
        it.size
    }

    val cartProductCount = cartProductRepository.getAllCartProduct().map {
        it.size
    }

    val unreadNotif = notifRepository.allNotification
        .map {
            it.filter { notif ->
                !notif.isRead
            }
        }
        .map { it.size }

    val username = preferencesRepository.username
}
