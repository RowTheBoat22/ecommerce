package com.verindrarizya.ecommerce.ui.authentication.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.AuthRepository
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.data.PreferencesRepository
import com.verindrarizya.ecommerce.data.remote.response.LoginResponse
import com.verindrarizya.ecommerce.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val preferencesRepository: PreferencesRepository,
    private val firebaseAnalyticsManager: FirebaseAnalyticsManager
) : ViewModel() {

    val isAlreadyOnBoarded = preferencesRepository.isUserAlreadyOnBoard

    private val _isEmailValid: MutableLiveData<Boolean> = MutableLiveData()
    val isEmailValid: LiveData<Boolean>
        get() = _isEmailValid

    private val _isPasswordValid: MutableLiveData<Boolean> = MutableLiveData()
    val isPasswordValid: LiveData<Boolean>
        get() = _isPasswordValid

    val isButtonEnabled: MediatorLiveData<Boolean> = MediatorLiveData()

    private val _loginState: MutableLiveData<Resource<LoginResponse>> = MutableLiveData()
    val loginState: LiveData<Resource<LoginResponse>>
        get() = _loginState

    private val emailPattern = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}\$"

    init {
        isButtonEnabled.addSource(isEmailValid) {
            isButtonEnabled.value = it && isPasswordValid.value == true
        }
        isButtonEnabled.addSource(isPasswordValid) {
            isButtonEnabled.value = it && isEmailValid.value == true
        }
    }

    fun checkEmailValid(email: String) {
        _isEmailValid.value = email.isNotBlank() && email.matches(emailPattern.toRegex())
    }

    fun checkPassword(password: String) {
        _isPasswordValid.value = password.length >= 8 && password.isNotBlank()
    }

    fun login(email: String, password: String) {
        firebaseAnalyticsManager.loginEvent()
        viewModelScope.launch {
            authRepository.login(email, password).collect { resource ->
                if (resource is Resource.Success) {
                    runBlocking {
                        preferencesRepository.apply {
                            setAccessToken(resource.data.data.accessToken)
                            setRefreshToken(resource.data.data.refreshToken)
                            val username = resource.data.data.userName
                            if (username.isNotBlank()) {
                                setUserName(username)
                            }
                        }
                    }
                }
                _loginState.value = resource
            }
        }
    }
}
