package com.verindrarizya.ecommerce.ui.main.detail

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.data.remote.response.ProductDetailResponse
import com.verindrarizya.ecommerce.ui.main.model.ListProduct
import com.verindrarizya.ecommerce.ui.main.model.Product
import com.verindrarizya.ecommerce.ui.theme.ECommerceTheme
import com.verindrarizya.ecommerce.utils.Resource
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DetailFragment : Fragment() {

    private val viewModel: DetailViewModel by viewModels()

    @Inject
    lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeView(requireContext()).apply {
        setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)

        setContent {
            val resourceProductResponse by viewModel.productDetail.observeAsState(initial = Resource.Loading)
            val isInWishlist by viewModel.isInWishlist.collectAsState(initial = false)
            val selectedProductVariantItem by viewModel.selectedProductVariantItem.observeAsState(
                DataDummy.productVariantItems[0]
            )

            val cartProductCount by viewModel.cartProductCount.observeAsState()
            val unreadNotifCount by viewModel.unreadNotif.observeAsState()

            ECommerceTheme {
                DetailScreen(
                    resourceProductResponse = resourceProductResponse,
                    onNavigationIconClick = { findNavController().navigateUp() },
                    onNotificationIconClick = {
                        val direction = DetailFragmentDirections.actionGlobalNotificationFragment()
                        findNavController().navigate(direction)
                    },
                    onCartIconClick = {
                        val direction = DetailFragmentDirections.actionGlobalCartFragment()
                        findNavController().navigate(direction)
                    },
                    onMenuIconClick = { /* Not Implemented */ },
                    onButtonBuyNowClick = {
                        if (resourceProductResponse is Resource.Success) {
                            val productDetail =
                                (resourceProductResponse as Resource.Success<ProductDetailResponse>).data.data
                            val product = Product(
                                productId = productDetail.productId,
                                productName = productDetail.productName,
                                image = productDetail.image[0],
                                productVariantName = selectedProductVariantItem.variantName,
                                stock = productDetail.stock,
                                price = productDetail.productPrice + selectedProductVariantItem.variantPrice,
                                quantity = 1,
                            )
                            val listProduct = ListProduct(listOf(product))

                            val direction =
                                DetailFragmentDirections.actionDetailFragmentToCheckoutFragment(
                                    listProduct
                                )
                            findNavController().navigate(direction)
                        }
                    },
                    onButtonAddToCartClick = {
                        viewModel.insertToCart()
                    },
                    onButtonReviewSeeAllClick = {
                        if (resourceProductResponse is Resource.Success) {
                            val productDetail =
                                (resourceProductResponse as Resource.Success<ProductDetailResponse>).data.data
                            val direction =
                                DetailFragmentDirections.actionDetailFragmentToReviewFragment(
                                    productDetail.productId
                                )
                            findNavController().navigate(direction)
                        }
                    },
                    wishlistChecked = isInWishlist,
                    onWishlistCheckedChange = {
                        if (isInWishlist) {
                            viewModel.deleteFromWishlist()
                        } else {
                            viewModel.saveToWishlist()
                        }
                    },
                    selectedProductVariantItem = selectedProductVariantItem,
                    onSelectedProductVariantItemChange = {
                        viewModel.selectProductVariantItem(it)
                    },
                    cartProductCount = cartProductCount,
                    unreadNotifCount = unreadNotifCount,
                    onRefreshClick = { viewModel.getProductDetail() },
                    onShareClicked = { productName, productPrice, productId ->
                        val sendIntent = Intent().apply {
                            action = Intent.ACTION_SEND
                            putExtra(
                                Intent.EXTRA_TEXT,
                                """
                             product: $productName
                             price: $productPrice
                             link: https://ecommerce.verindrarizya.com/product/$productId
                                """.trimIndent()
                            )
                            type = "text/plain"
                        }

                        val shareIntent = Intent.createChooser(sendIntent, null)
                        startActivity(shareIntent)
                    }
                )
            }
        }
    }
}
