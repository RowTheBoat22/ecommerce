package com.verindrarizya.ecommerce.ui.main.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class ListProduct(
    val data: List<Product>
) : Parcelable
