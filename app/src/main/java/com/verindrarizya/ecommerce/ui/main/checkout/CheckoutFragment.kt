package com.verindrarizya.ecommerce.ui.main.checkout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.databinding.FragmentCheckoutBinding
import com.verindrarizya.ecommerce.ui.main.choosepayment.ChoosePaymentFragment
import com.verindrarizya.ecommerce.ui.main.model.Payment
import com.verindrarizya.ecommerce.utils.CurrencyHelper
import com.verindrarizya.ecommerce.utils.Resource
import com.verindrarizya.ecommerce.utils.getErrorResponse
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class CheckoutFragment : Fragment() {

    private var _binding: FragmentCheckoutBinding? = null
    private val binding: FragmentCheckoutBinding
        get() = _binding!!

    private val viewModel: CheckoutViewModel by viewModels()

    private lateinit var rvAdapter: CheckoutAdapter

    @Inject
    lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCheckoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToolbarAction()
        setInitRvAdapterAndLayoutManager()
        setPaymentResultListener()

        viewModel.checkoutProducts.observe(viewLifecycleOwner) {
            rvAdapter.submitList(it)
        }

        viewModel.sumOfPrice.observe(viewLifecycleOwner) {
            binding.tvTotalPrice.text = CurrencyHelper.convertToRupiah(it)
        }

        viewModel.fulfillmentFlow.observe(viewLifecycleOwner) {
        }

        viewModel.selectedPayment.observe(viewLifecycleOwner) { payment ->
            if (payment != null) {
                context?.let {
                    Glide.with(it)
                        .load(payment.image)
                        .into(binding.ivPayment)
                }

                binding.tvPaymentTitle.text = payment.label
            }

            binding.btnBuy.isEnabled = payment != null
        }

        viewModel.fulfillmentFlow.observe(viewLifecycleOwner) {
            binding.progressBar.isVisible = it is Resource.Loading

            binding.btnBuy.isEnabled = it !is Resource.Loading

            binding.nestedScrollView.isVisible = it !is Resource.Loading

            if (it is Resource.Success) {
                findNavController().navigate(
                    CheckoutFragmentDirections.actionCheckoutFragmentToFulfillmentFragment(
                        fulfillment = it.data,
                        isFromCheckout = true
                    )
                )
            } else if (it is Resource.Failure) {
                when (val error = it.throwable) {
                    is HttpException -> {
                        val errorResponse = error.getErrorResponse()
                        showSnackbar(errorResponse.message)
                    }

                    is IOException -> {
                        showSnackbar(getString(R.string.your_connection_is_unavailable))
                    }

                    else -> {
                        showSnackbar(getString(R.string.general_error_statement))
                    }
                }
            }
        }

        binding.cvPayment.setOnClickListener {
            findNavController().navigate(
                CheckoutFragmentDirections.actionCheckoutFragmentToChoosePaymentFragment()
            )
        }

        binding.btnBuy.setOnClickListener {
            firebaseAnalyticsManager.buttonClickEvent("button buy from checkout")
            viewModel.checkout()
        }
    }

    private fun setPaymentResultListener() {
        setFragmentResultListener(ChoosePaymentFragment.REQUEST_PAYMENT_KEY) { _: String, bundle: Bundle ->
            /**
             *
             * this is deprecated, but the one who is not must require API level 33
             *
             */
            val paymentResult =
                bundle.getParcelable<Payment>(ChoosePaymentFragment.PAYMENT_RESULT_KEY)
            viewModel.setSelectedPayment(paymentResult)
        }
    }

    private fun showSnackbar(snackbarMessage: String) {
        Snackbar.make(binding.btnBuy, snackbarMessage, Snackbar.LENGTH_SHORT)
            .setAnchorView(binding.buySectionDivider)
            .show()
    }

    private fun setInitRvAdapterAndLayoutManager() {
        rvAdapter = CheckoutAdapter(
            onActionQuantityIncrease = { product ->
                firebaseAnalyticsManager.buttonClickEvent("button quantity increase")
                viewModel.updateQuantity(product = product.copy(quantity = product.quantity + 1))
            },
            onActionQuantityDecrease = { product ->
                firebaseAnalyticsManager.buttonClickEvent("button quantity decrease")
                viewModel.updateQuantity(product = product.copy(quantity = product.quantity - 1))
            }
        )

        binding.rvPurchasedItems.adapter = rvAdapter
        binding.rvPurchasedItems.layoutManager = LinearLayoutManager(context)
        binding.rvPurchasedItems.itemAnimator = null
    }

    private fun setToolbarAction() {
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
