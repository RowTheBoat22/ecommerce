package com.verindrarizya.ecommerce.ui.main.profile

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.AuthRepository
import com.verindrarizya.ecommerce.data.PreferencesRepository
import com.verindrarizya.ecommerce.data.remote.response.ProfileResponse
import com.verindrarizya.ecommerce.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val preferencesRepository: PreferencesRepository,
    private val authRepository: AuthRepository
) : ViewModel() {

    private val _username: MutableLiveData<String> = MutableLiveData()

    val isUsernameSet: Flow<Boolean> = preferencesRepository.isUserNameAlreadySet

    val isNameValid: LiveData<Boolean> = _username.map {
        it.isNotBlank()
    }

    private val _imageProfileStringUri: MutableLiveData<String> = MutableLiveData()
    val imageProfileStringUri: LiveData<String>
        get() = _imageProfileStringUri

    private val _profileState: MutableLiveData<Resource<ProfileResponse>> = MutableLiveData()
    val profileState: LiveData<Resource<ProfileResponse>>
        get() = _profileState

    fun checkName(name: String) {
        _username.value = name
    }

    fun setImageUri(uri: Uri) {
        _imageProfileStringUri.value = uri.toString()
    }

    fun setProfile(imageFile: File?) {
        viewModelScope.launch {
            authRepository.profile(
                _username.value ?: "",
                imageFile
            ).collect { resource ->
                if (resource is Resource.Success) {
                    runBlocking {
                        preferencesRepository.setUserName(resource.data.data.userName)
                    }
                }
                _profileState.value = resource
            }
        }
    }
}
