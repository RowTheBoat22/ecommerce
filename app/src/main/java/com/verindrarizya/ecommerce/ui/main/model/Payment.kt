package com.verindrarizya.ecommerce.ui.main.model

import android.os.Parcelable
import androidx.annotation.Keep
import com.verindrarizya.ecommerce.data.remote.response.PaymentItem
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class Payment(
    val image: String,
    val label: String
) : Parcelable

fun PaymentItem.toPayment() =
    Payment(
        image = image,
        label = label
    )
