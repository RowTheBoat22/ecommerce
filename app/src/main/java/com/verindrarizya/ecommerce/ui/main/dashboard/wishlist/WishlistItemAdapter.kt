package com.verindrarizya.ecommerce.ui.main.dashboard.wishlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.local.entity.WishlistProductEntity
import com.verindrarizya.ecommerce.databinding.WishlistProductItemGridBinding
import com.verindrarizya.ecommerce.databinding.WishlistProductItemListBinding
import com.verindrarizya.ecommerce.utils.CurrencyHelper
import java.util.Locale

class WishlistItemAdapter(
    val onDeleteButtonClick: (WishlistProductEntity) -> Unit,
    val onActionAddToCart: (WishlistProductEntity) -> Unit,
    val onItemClick: (String) -> Unit
) : ListAdapter<WishlistProductEntity, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    // 1 -> list, 2 -> grid
    private var displayedViewType: Int = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (displayedViewType) {
            1 -> {
                WishlistItemListViewHolder(
                    WishlistProductItemListBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }

            else -> {
                WishlistItemGridViewHolder(
                    WishlistProductItemGridBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val wishlistProduct = getItem(position)
        wishlistProduct?.let { p ->
            when (holder) {
                is WishlistItemAdapter.WishlistItemListViewHolder -> holder.bind(p)
                is WishlistItemAdapter.WishlistItemGridViewHolder -> holder.bind(p)
            }
        }
    }

    fun setViewType(newViewType: Int) {
        displayedViewType = newViewType
    }

    override fun getItemViewType(position: Int): Int {
        return displayedViewType
    }

    inner class WishlistItemListViewHolder(private val binding: WishlistProductItemListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(wishlistProductEntity: WishlistProductEntity) {
            itemView.setOnClickListener { onItemClick(wishlistProductEntity.productId) }
            Glide.with(itemView.context)
                .load(wishlistProductEntity.image)
                .placeholder(R.drawable.image_placeholder)
                .into(binding.ivItem)

            binding.tvTitle.text = wishlistProductEntity.productName
            binding.tvPrice.text =
                CurrencyHelper.convertToRupiah(wishlistProductEntity.productPrice)
            binding.tvStoreName.text = wishlistProductEntity.store
            binding.tvRatingAndSoldItem.text = itemView.resources.getString(
                R.string.rating_and_sold_item,
                wishlistProductEntity.productRating.toString().toFloat(),
                wishlistProductEntity.sale
            )

            binding.btnDelete.setOnClickListener { onDeleteButtonClick(wishlistProductEntity) }
            binding.btnCart.setOnClickListener { onActionAddToCart(wishlistProductEntity) }
        }
    }

    inner class WishlistItemGridViewHolder(private val binding: WishlistProductItemGridBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(wishlistProductEntity: WishlistProductEntity) {
            itemView.setOnClickListener { onItemClick(wishlistProductEntity.productId) }
            Glide.with(itemView.context)
                .load(wishlistProductEntity.image)
                .into(binding.ivItem)

            binding.tvTitle.text = wishlistProductEntity.productName
            binding.tvPrice.text =
                CurrencyHelper.convertToRupiah(wishlistProductEntity.productPrice)
            binding.tvStoreName.text = wishlistProductEntity.store
            binding.tvRatingAndSoldItem.text = itemView.resources.getString(
                R.string.rating_and_sold_item,
                wishlistProductEntity.productRating.toString().toFloat(),
                wishlistProductEntity.sale
            )

            if (Locale.getDefault().language == "in") {
                binding.btnCart.textSize = 12f
            }

            binding.btnDelete.setOnClickListener { onDeleteButtonClick(wishlistProductEntity) }
            binding.btnCart.setOnClickListener { onActionAddToCart(wishlistProductEntity) }
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<WishlistProductEntity>() {
            override fun areItemsTheSame(
                oldItem: WishlistProductEntity,
                newItem: WishlistProductEntity
            ): Boolean {
                return oldItem.productId == newItem.productId
            }

            override fun areContentsTheSame(
                oldItem: WishlistProductEntity,
                newItem: WishlistProductEntity
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}
