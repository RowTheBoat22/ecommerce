package com.verindrarizya.ecommerce.ui.main.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.data.local.entity.CartProductEntity
import com.verindrarizya.ecommerce.databinding.FragmentCartBinding
import com.verindrarizya.ecommerce.ui.main.model.ListProduct
import com.verindrarizya.ecommerce.ui.main.model.Product
import com.verindrarizya.ecommerce.ui.main.model.toProduct
import com.verindrarizya.ecommerce.utils.CurrencyHelper
import com.verindrarizya.ecommerce.utils.launchAndCollectIn
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class CartFragment : Fragment() {

    private var _binding: FragmentCartBinding? = null
    private val binding: FragmentCartBinding
        get() = _binding!!

    private val viewModel: CartViewModel by viewModels()

    private lateinit var rvAdapter: CartAdapter

    @Inject
    lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToolbarMenuAndAction()
        populateErrorLayout()
        initRvAdapterAndLayoutManager()

        viewModel.isEveryProductSelected.launchAndCollectIn(viewLifecycleOwner) {
            binding.cbSelectAll.isChecked = it
        }

        viewModel.isAnyProductSelected.launchAndCollectIn(viewLifecycleOwner) {
            binding.btnBuy.isVisible = it
            binding.btnDelete.isVisible = it
        }

        viewModel.allCartProduct.launchAndCollectIn(viewLifecycleOwner) {
            if (it.isNotEmpty()) {
                firebaseAnalyticsManager.viewCartEvent(it)
            }
            binding.errorLayout.root.isVisible = it.isEmpty()
            layoutVisibility(it.isNotEmpty())
            rvAdapter.submitList(it)
        }

        viewModel.sumPrice.launchAndCollectIn(viewLifecycleOwner) {
            binding.tvTotalPrice.text = CurrencyHelper.convertToRupiah(it)
        }

        binding.cbSelectAll.setOnClickListener {
            val isCheckedAll = binding.cbSelectAll.isChecked
            viewModel.updateSelectedAllProduct(isCheckedAll)
        }

        binding.btnDelete.setOnClickListener {
            firebaseAnalyticsManager.buttonClickEvent("button delete cart")
            viewModel.deleteSelectedProduct()
        }

        binding.btnBuy.setOnClickListener {
            lifecycleScope.launch {
                firebaseAnalyticsManager.buttonClickEvent("button buy from cart")
                val cartProducts = viewModel.allCartProduct.first()

                val selectedCartProducts = cartProducts.filter { it.isSelected }

                val checkoutProducts: List<Product> = selectedCartProducts.map {
                    it.toProduct()
                }

                findNavController().navigate(
                    CartFragmentDirections.actionCartFragmentToCheckoutFragment(
                        ListProduct(data = checkoutProducts)
                    )
                )
            }
        }
    }

    private fun layoutVisibility(isVisible: Boolean) {
        binding.cbSelectAll.isVisible = isVisible
        binding.tvSelectAll.isVisible = isVisible
        binding.selectAllSectionDivider.isVisible = isVisible
        binding.buySectionDivider.isVisible = isVisible
        binding.tvTotalPrice.isVisible = isVisible
        binding.tvTotalPay.isVisible = isVisible
    }

    private fun initRvAdapterAndLayoutManager() {
        rvAdapter = CartAdapter(
            onActionCheckbox = { cartProductEntity: CartProductEntity ->
                val newCartProductState =
                    cartProductEntity.copy(isSelected = !cartProductEntity.isSelected)
                viewModel.updateCartProduct(newCartProductState)
            },
            onActionDelete = {
                firebaseAnalyticsManager.buttonClickEvent("button delete from cart")
                viewModel.deleteFromCart(it)
            },
            onActionQuantityDecrease = { cartProductEntity: CartProductEntity ->
                firebaseAnalyticsManager.buttonClickEvent("button quantity decrease")
                viewModel.updateCartProduct(cartProductEntity.copy(quantity = cartProductEntity.quantity - 1))
            },
            onActionQuantityIncrease = { cartProductEntity: CartProductEntity ->
                firebaseAnalyticsManager.buttonClickEvent("button quantity increase")
                viewModel.updateCartProduct(cartProductEntity.copy(quantity = cartProductEntity.quantity + 1))
            }
        )
        binding.rvCart.adapter = rvAdapter
        binding.rvCart.layoutManager = LinearLayoutManager(context)
        binding.rvCart.itemAnimator = null
    }

    private fun setToolbarMenuAndAction() {
        binding.toolbar.setNavigationOnClickListener { findNavController().navigateUp() }
    }

    private fun populateErrorLayout() {
        binding.errorLayout.tvErrorTitle.text = getString(R.string.empty)
        binding.errorLayout.tvErrorDesc.text = getString(R.string.empty_description)
        binding.errorLayout.btnAction.visibility = View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
