package com.verindrarizya.ecommerce.ui.main.choosepayment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.verindrarizya.ecommerce.data.remote.response.PaymentItem
import com.verindrarizya.ecommerce.data.remote.response.PaymentSectionItem
import com.verindrarizya.ecommerce.databinding.PaymentSectionItemBinding

class PaymentSectionItemAdapter(
    private val onPaymentItemClick: (PaymentItem) -> Unit
) : ListAdapter<PaymentSectionItem, PaymentSectionItemAdapter.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            PaymentSectionItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val binding: PaymentSectionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(paymentSectionItem: PaymentSectionItem) {
            binding.paymentSectionTitle.text = paymentSectionItem.title

            val rvAdapter = PaymentItemAdapter(
                onPaymentItemClick = onPaymentItemClick
            )

            binding.rvPaymentSection.layoutManager = LinearLayoutManager(itemView.context)
            binding.rvPaymentSection.adapter = rvAdapter

            rvAdapter.submitList(paymentSectionItem.item)

            val lastPosition = itemCount - 1
            val isLastPosition = bindingAdapterPosition == lastPosition

            if (isLastPosition) binding.sectionDivider.visibility = View.GONE
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PaymentSectionItem>() {
            override fun areItemsTheSame(
                oldItem: PaymentSectionItem,
                newItem: PaymentSectionItem
            ): Boolean {
                return oldItem.title == newItem.title
            }

            override fun areContentsTheSame(
                oldItem: PaymentSectionItem,
                newItem: PaymentSectionItem
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}
