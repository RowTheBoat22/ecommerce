package com.verindrarizya.ecommerce.ui.main.dashboard.transaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.PaymentRepository
import com.verindrarizya.ecommerce.data.remote.response.TransactionResponse
import com.verindrarizya.ecommerce.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(
    private val paymentRepository: PaymentRepository,
) : ViewModel() {

    private val _transactionHistoryFlow: MutableLiveData<Resource<TransactionResponse>> =
        MutableLiveData()
    val transactionHistoryFlow: LiveData<Resource<TransactionResponse>>
        get() = _transactionHistoryFlow

    init {
        getTransactionHistory()
    }

    fun getTransactionHistory() {
        viewModelScope.launch {
            paymentRepository.transactionHistory().collect {
                _transactionHistoryFlow.value = it
            }
        }
    }
}
