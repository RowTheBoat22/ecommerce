package com.verindrarizya.ecommerce.ui.authentication.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.databinding.FragmentLoginBinding
import com.verindrarizya.ecommerce.utils.Resource
import com.verindrarizya.ecommerce.utils.getErrorResponse
import com.verindrarizya.ecommerce.utils.launchAndCollectIn
import com.verindrarizya.ecommerce.utils.showShortToast
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : Fragment() {

    private val viewModel: LoginViewModel by viewModels()

    private var _binding: FragmentLoginBinding? = null
    private val binding
        get() = _binding!!

    @Inject
    lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.hide()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkUserOnboardedStatus()

        setValidationForTextField()
        setListenerForTextField()
        setButtonClickListener()

        // button register enabled state
        viewModel.isButtonEnabled.observe(viewLifecycleOwner) {
            binding.btnLogin.isEnabled = it
        }

        viewModel.loginState.observe(viewLifecycleOwner) { resource ->
            binding.progressBar.isVisible = resource is Resource.Loading
            binding.btnRegister.isEnabled = resource !is Resource.Loading
            binding.btnLogin.isEnabled = resource !is Resource.Loading

            if (resource is Resource.Success) {
                Toast.makeText(context, "Login Success", Toast.LENGTH_SHORT).show()
            }

            if (resource is Resource.Failure) {
                when (val error = resource.throwable) {
                    is HttpException -> {
                        if (error.code() == 400) {
                            val errorResponse = error.getErrorResponse()
                            context?.let { showShortToast(it, errorResponse.message) }
                        }
                    }

                    is IOException -> {
                        context?.let {
                            showShortToast(it, getString(R.string.your_connection_is_unavailable))
                        }
                    }

                    else -> {
                        context?.let {
                            showShortToast(it, getString(R.string.general_error_statement))
                        }
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setValidationForTextField() {
        viewModel.isPasswordValid.observe(viewLifecycleOwner) {
            if (it || binding.edPassword.text.toString().isBlank()) {
                binding.tfPassword.isErrorEnabled = false
                binding.tfPassword.error = null
            } else {
                binding.tfPassword.error = getString(R.string.error_password_text_field_not_valid)
            }
        }

        viewModel.isEmailValid.observe(viewLifecycleOwner) {
            if (it || binding.edEmail.text.toString().isBlank()) {
                binding.tfEmail.isErrorEnabled = false
                binding.tfEmail.error = null
            } else {
                binding.tfEmail.error = getString(R.string.error_email_text_field_not_valid)
            }
        }
    }

    private fun setListenerForTextField() {
        binding.edEmail.doOnTextChanged { text, _, _, _ ->
            viewModel.checkEmailValid(text.toString())
        }

        binding.edPassword.doOnTextChanged { text, _, _, _ ->
            viewModel.checkPassword(text.toString())
        }
    }

    private fun setButtonClickListener() {
        binding.btnLogin.setOnClickListener {
            firebaseAnalyticsManager.buttonClickEvent("button login")
            viewModel.login(
                email = binding.edEmail.text.toString(),
                password = binding.edPassword.text.toString()
            )
        }

        binding.btnRegister.setOnClickListener {
            firebaseAnalyticsManager.buttonClickEvent("button register")
            findNavController().navigate(
                LoginFragmentDirections.actionLoginFragmentToRegisterFragment()
            )
        }
    }

    private fun checkUserOnboardedStatus() {
        viewModel.isAlreadyOnBoarded.launchAndCollectIn(viewLifecycleOwner) {
            if (it == false) {
                findNavController().navigate(
                    LoginFragmentDirections.actionLoginFragmentToOnBoardingFragment()
                )
            }
        }
    }
}
