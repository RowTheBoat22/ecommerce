package com.verindrarizya.ecommerce.ui.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.google.android.material.tabs.TabLayoutMediator
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.databinding.FragmentOnBoardingBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class OnBoardingFragment : Fragment() {

    private val viewModel: OnBoardingViewModel by viewModels()

    private var _binding: FragmentOnBoardingBinding? = null
    private val binding
        get() = _binding!!

    private val onBoardItemDrawables = listOf(
        R.drawable.onboard_1,
        R.drawable.onboard_2,
        R.drawable.onboard_3
    )

    @Inject
    lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    private val viewPagerObserver = object : OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            binding.btnSelanjutnya.setOnClickListener {
                firebaseAnalyticsManager.buttonClickEvent("button on boarding next")
                binding.vpImage.setCurrentItem(position + 1, true)
            }
            if (position == 2) {
                binding.btnSelanjutnya.visibility = View.GONE
            } else {
                binding.btnSelanjutnya.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentOnBoardingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewPager2Adapter = BoardingPagerAdapter(onBoardItemDrawables)

        binding.vpImage.adapter = viewPager2Adapter
        TabLayoutMediator(binding.tlIndicator, binding.vpImage) { _, _ -> }.attach()
        setListenToViewPager()

        binding.btnGabungSekarang.setOnClickListener {
            firebaseAnalyticsManager.buttonClickEvent("button join now")
            viewModel.setOnBoarded()
            findNavController().navigate(
                OnBoardingFragmentDirections.actionOnBoardingFragmentToRegisterFragment()
            )
        }

        binding.btnLewati.setOnClickListener {
            firebaseAnalyticsManager.buttonClickEvent("button skip")
            viewModel.setOnBoarded()
            findNavController().navigate(
                OnBoardingFragmentDirections.actionOnBoardingFragmentToLoginFragment()
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.vpImage.unregisterOnPageChangeCallback(viewPagerObserver)
        _binding = null
    }

    private fun setListenToViewPager() {
        binding.vpImage.registerOnPageChangeCallback(viewPagerObserver)
    }
}
