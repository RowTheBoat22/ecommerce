package com.verindrarizya.ecommerce.ui.main.dashboard.transaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.remote.response.TransactionData
import com.verindrarizya.ecommerce.databinding.TransactionItemBinding
import com.verindrarizya.ecommerce.utils.CurrencyHelper

class TransactionAdapter(
    private val onActionReview: (TransactionData) -> Unit
) : ListAdapter<TransactionData, TransactionAdapter.TransactionViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val binding =
            TransactionItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TransactionViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class TransactionViewHolder(private val binding: TransactionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(transactionData: TransactionData) {
            binding.tvDate.text = transactionData.date

            Glide.with(itemView.context)
                .load(transactionData.image)
                .placeholder(R.drawable.image_placeholder)
                .into(binding.ivProduct)

            binding.tvProductName.text = transactionData.name
            binding.tvProductQuantity.text =
                itemView.context.getString(R.string.item_count, transactionData.items.size)
            binding.tvPrice.text = CurrencyHelper.convertToRupiah(transactionData.total)

            binding.tvStatus.text = if (transactionData.status) {
                "Selesai"
            } else {
                "Dalam Proses"
            }

            if (transactionData.rating == null || transactionData.review == null) {
                binding.btnReview.visibility = View.VISIBLE
            } else {
                binding.btnReview.visibility = View.GONE
            }

            binding.btnReview.setOnClickListener { onActionReview(transactionData) }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TransactionData>() {
            override fun areItemsTheSame(
                oldItem: TransactionData,
                newItem: TransactionData
            ): Boolean {
                return oldItem.invoiceId == newItem.invoiceId
            }

            override fun areContentsTheSame(
                oldItem: TransactionData,
                newItem: TransactionData
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}
