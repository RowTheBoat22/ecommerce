package com.verindrarizya.ecommerce.ui.main.checkout

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.databinding.CheckoutItemBinding
import com.verindrarizya.ecommerce.ui.main.model.Product
import com.verindrarizya.ecommerce.utils.CurrencyHelper

class CheckoutAdapter(
    private val onActionQuantityDecrease: (Product) -> Unit,
    private val onActionQuantityIncrease: (Product) -> Unit
) : ListAdapter<Product, CheckoutAdapter.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            CheckoutItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val binding: CheckoutItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(product: Product) {
            binding.tgQuantity.addOnButtonCheckedListener { group, _, _ ->
                group.clearChecked()
            }
            Glide.with(itemView.context)
                .load(product.image)
                .placeholder(R.drawable.image_placeholder)
                .into(binding.ivItem)

            binding.tvProductName.text = product.productName
            binding.tvProductVarian.text = product.productVariantName

            binding.tvProductStock.text = if (product.stock >= 10) {
                itemView.context.getString(R.string.stock_quantity, product.stock)
            } else {
                binding.tvProductStock.setTextColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.text_warning
                    )
                )
                itemView.context.getString(
                    R.string.stock_quantity_nearly_run_out,
                    product.stock
                )
            }
            binding.tvPrice.text = CurrencyHelper.convertToRupiah(product.price * product.quantity)

            binding.btnTextQuantity.text = product.quantity.toString()

            binding.btnQuantityDecrease.setOnClickListener {
                if (product.quantity > 1) {
                    onActionQuantityDecrease(product)
                }
            }
            binding.btnQuantityIncrease.setOnClickListener {
                if (product.quantity < product.stock) {
                    onActionQuantityIncrease(product)
                }
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Product>() {
            override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
                return oldItem.productId == newItem.productId
            }

            override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
                return oldItem == newItem
            }
        }
    }
}
