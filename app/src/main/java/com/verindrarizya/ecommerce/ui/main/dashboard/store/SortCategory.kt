package com.verindrarizya.ecommerce.ui.main.dashboard.store

import androidx.annotation.StringRes
import com.verindrarizya.ecommerce.R

enum class SortCategory(
    @StringRes val displayedName: Int,
    val value: String
) {
    Review(R.string.review, "rating"),
    Sale(R.string.sold, "sale"),
    LowestPrice(R.string.lowest_price, "lowest"),
    HighestPrice(R.string.highest_price, "highest")
}
