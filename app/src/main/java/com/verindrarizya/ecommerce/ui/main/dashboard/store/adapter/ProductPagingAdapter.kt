package com.verindrarizya.ecommerce.ui.main.dashboard.store.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.remote.response.Product
import com.verindrarizya.ecommerce.databinding.ProductItemGridBinding
import com.verindrarizya.ecommerce.databinding.ProductItemListBinding
import com.verindrarizya.ecommerce.utils.CurrencyHelper

class ProductPagingAdapter(
    val onItemClick: (Product) -> Unit
) : PagingDataAdapter<Product, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    // 1 -> list, 2 -> grid
    private var displayedViewType: Int = 1

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return when (displayedViewType) {
            1 -> {
                ListItemViewHolder(
                    ProductItemListBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }

            else -> {
                GridItemViewHolder(
                    ProductItemGridBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val product = getItem(position)
        product?.let { p: Product ->
            when (holder) {
                is ListItemViewHolder -> holder.bind(p)
                is GridItemViewHolder -> holder.bind(p)
            }
        }
    }

    fun setViewType(newViewType: Int) {
        displayedViewType = newViewType
    }

    inner class ListItemViewHolder(private val binding: ProductItemListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(product: Product) {
            itemView.setOnClickListener { onItemClick(product) }
            Glide.with(itemView.context)
                .load(product.image)
                .placeholder(R.drawable.image_placeholder)
                .centerCrop()
                .into(binding.ivItem)

            binding.tvTitle.text = product.productName
            binding.tvPrice.text = CurrencyHelper.convertToRupiah(product.productPrice)
            binding.tvStoreName.text = product.store
            binding.tvRatingAndSoldItem.text = itemView.resources.getString(
                R.string.rating_and_sold_item,
                product.productRating.toString().toFloat(),
                product.sale
            )
        }
    }

    inner class GridItemViewHolder(private val binding: ProductItemGridBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(product: Product) {
            itemView.setOnClickListener { onItemClick(product) }
            Glide.with(itemView.context)
                .load(product.image)
                .placeholder(R.drawable.image_placeholder)
                .centerCrop()
                .into(binding.ivItem)

            binding.tvTitle.text = product.productName
            binding.tvPrice.text = CurrencyHelper.convertToRupiah(product.productPrice)
            binding.tvStoreName.text = product.store
            binding.tvRatingAndSoldItem.text = itemView.resources.getString(
                R.string.rating_and_sold_item,
                product.productRating.toString().toFloat(),
                product.sale
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        return displayedViewType
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Product>() {
            override fun areItemsTheSame(
                oldItem: Product,
                newItem: Product
            ): Boolean {
                return oldItem.productId == newItem.productId
            }

            override fun areContentsTheSame(
                oldItem: Product,
                newItem: Product
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}
