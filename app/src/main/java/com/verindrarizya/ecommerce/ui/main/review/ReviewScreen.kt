package com.verindrarizya.ecommerce.ui.main.review

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Star
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.remote.response.ProductReview
import com.verindrarizya.ecommerce.data.remote.response.ProductReviewResponse
import com.verindrarizya.ecommerce.ui.theme.ECommerceTheme
import com.verindrarizya.ecommerce.ui.theme.ErrorLayout
import com.verindrarizya.ecommerce.ui.theme.Poppins
import com.verindrarizya.ecommerce.ui.theme.textStyleRemoveFontPadding
import com.verindrarizya.ecommerce.utils.Resource

@Composable
fun ReviewScreen(
    modifier: Modifier = Modifier,
    appBarTitle: String = stringResource(R.string.buyer_reviews),
    onNavigationIconClick: () -> Unit,
    resourceProductReviewResponse: Resource<ProductReviewResponse>,
    onRefreshClick: () -> Unit
) {
    Scaffold(
        modifier = modifier,
        topBar = {
            ReviewAppBar(
                title = appBarTitle,
                onNavigationIconClick = onNavigationIconClick
            )
        },
        content = {
            when (resourceProductReviewResponse) {
                is Resource.Success -> {
                    val productReviews = resourceProductReviewResponse.data.data

                    LazyColumn(
                        modifier = Modifier.padding(it)
                    ) {
                        items(productReviews) { productReview ->
                            Column {
                                ReviewItem(productReview = productReview)
                                Divider()
                            }
                        }
                    }
                }

                is Resource.Failure -> {
                    val error = resourceProductReviewResponse.throwable

                    Box(
                        modifier = Modifier
                            .padding(it)
                            .fillMaxSize(),
                        contentAlignment = Alignment.Center
                    ) {
                        ErrorLayout(
                            error = error,
                            buttonText = stringResource(R.string.refresh),
                            onRetryClick = onRefreshClick
                        )
                    }
                }

                is Resource.Loading -> {
                    Box(
                        modifier = Modifier
                            .padding(it)
                            .fillMaxSize(),
                        contentAlignment = Alignment.Center
                    ) {
                        CircularProgressIndicator()
                    }
                }
            }
        }
    )
}

@Composable
@OptIn(ExperimentalMaterial3Api::class)
fun ReviewAppBar(
    modifier: Modifier = Modifier,
    title: String,
    onNavigationIconClick: () -> Unit,
) {
    Column(modifier = modifier) {
        TopAppBar(
            modifier = modifier,
            title = {
                Text(
                    text = title,
                    fontFamily = Poppins,
                    fontSize = 22.sp,
                    fontWeight = FontWeight.Normal,
                    style = textStyleRemoveFontPadding
                )
            },
            navigationIcon = {
                IconButton(onClick = onNavigationIconClick) {
                    Icon(
                        painter = painterResource(R.drawable.arrow_back),
                        contentDescription = "Back"
                    )
                }
            },
        )
        Divider()
    }
}

@Composable
fun ReviewItem(
    modifier: Modifier = Modifier,
    productReview: ProductReview
) {
    Column(
        modifier = modifier
            .padding(16.dp)
    ) {
        Row(
            modifier = Modifier.height(IntrinsicSize.Min)
        ) {
            AsyncImage(
                modifier = Modifier
                    .size(40.dp)
                    .clip(CircleShape),
                model = productReview.userImage,
                error = painterResource(R.drawable.rounded_image_placeholder),
                contentDescription = "${productReview.userName}  User Image",
                placeholder = painterResource(R.drawable.rounded_image_placeholder)
            )
            Spacer(modifier = Modifier.width(8.dp))
            Column(
                modifier = Modifier.fillMaxHeight(),
                verticalArrangement = Arrangement.SpaceEvenly
            ) {
                Text(
                    text = productReview.userName,
                    fontSize = 12.sp,
                    fontFamily = Poppins,
                    fontWeight = FontWeight.SemiBold,
                    style = textStyleRemoveFontPadding
                )
                RatingBar(
                    modifier = Modifier.height(12.dp),
                    rating = productReview.userRating ?: 0
                )
            }
        }
        Spacer(modifier = Modifier.height(8.dp))
        if (!productReview.userReview.isNullOrEmpty()) {
            Text(
                text = productReview.userReview,
                fontSize = 12.sp,
                fontFamily = Poppins,
                fontWeight = FontWeight.Normal,
                style = textStyleRemoveFontPadding
            )
        }
    }
}

@Composable
fun RatingBar(
    modifier: Modifier = Modifier,
    rating: Int = 1,
    stars: Int = 5,
    starsColor: Color = if (isSystemInDarkTheme()) Color(137, 71, 255, 255) else Color.Black
) {
    val unfilledStars = stars - rating

    Row(modifier = modifier) {
        repeat(rating) {
            Icon(
                imageVector = Icons.Outlined.Star,
                contentDescription = null,
                tint = starsColor
            )
        }

        repeat(unfilledStars) {
            Icon(
                imageVector = Icons.Outlined.Star,
                contentDescription = null,
                tint = starsColor.copy(alpha = 0.3f)
            )
        }
    }
}

// @Preview(showBackground = true)
// @Composable
// fun ReviewScreenSuccessLightPreview() {
//    ECommerceTheme {
//        ReviewScreen(
//            onNavigationIconClick = {},
//            resourceProductReviewResponse = Resource.Success(DataDummy.productReviewResponse),
//            onRefreshClick = {}
//        )
//    }
// }
//
// @Preview(showBackground = true)
// @Composable
// fun ReviewScreenSuccessDarkPreview() {
//    ECommerceTheme(darkTheme = true) {
//        ReviewScreen(
//            onNavigationIconClick = {},
//            resourceProductReviewResponse = Resource.Success(DataDummy.productReviewResponse),
//            onRefreshClick = {}
//        )
//    }
// }

// @Preview(showBackground = true)
// @Composable
// fun ReviewScreenFailurePreview() {
//    ReviewScreen(
//        onNavigationIconClick = {},
//        resourceProductReviewResponse = Resource.Failure(IOException()),
//        onRefreshClick = {}
//    )
// }
//
// @Preview(showBackground = true)
// @Composable
// fun ReviewScreenLoadingPreview() {
//    ReviewScreen(
//        onNavigationIconClick = {},
//        resourceProductReviewResponse = Resource.Loading,
//        onRefreshClick = {}
//    )
// }

@Preview(showBackground = true)
@Composable
fun ReviewItemLightPreview() {
    ECommerceTheme {
        ReviewItem(
            modifier = Modifier.fillMaxWidth(),
            productReview = DataDummy.productReviewResponse.data[0]
        )
    }
}

@Preview(showBackground = true)
@Composable
fun ReviewItemDarkPreview() {
    ECommerceTheme(darkTheme = true) {
        ReviewItem(
            modifier = Modifier.fillMaxWidth(),
            productReview = DataDummy.productReviewResponse.data[0]
        )
    }
}

// @Preview(showBackground = true)
// @Composable
// fun RatingPreview() {
//    RatingBar(rating = 2)
// }

object DataDummy {
    val productReviewResponse = ProductReviewResponse(
        code = 200,
        message = "OK",
        data = listOf(
            ProductReview(
                userImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQM4VpzpVw8mR2j9_gDajEthwY3KCOWJ1tOhcv47-H9o1a-s9GRPxdb_6G9YZdGfv0HIg&usqp=CAU",
                userName = "John Doe",
                userReview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec euismod, nisl eget tempor dapibus, nunc libero ultricies nunc, quis aliquam nisl nunc eu nisi. Sed vitae nisl eget nisl aliquet ultrices. Sed vitae nisl eget nisl aliquet ultrices.",
                userRating = 3
            ),
            ProductReview(
                userImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTR3Z6PN8QNVhH0e7rEINu_XJS0qHIFpDT3nwF5WSkcYmr3znhY7LOTkc8puJ68Bts-TMc&usqp=CAU",
                userName = "Jane Doe",
                userReview = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec euismod, nisl eget tempor dapibus, nunc libero ultricies nunc, quis aliquam nisl nunc eu nisi. Sed vitae nisl eget nisl aliquet ultrices. Sed vitae nisl eget nisl aliquet ultrices.",
                userRating = 4
            ),
        )
    )
}
