package com.verindrarizya.ecommerce.ui.main.choosepayment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.remote.response.PaymentItem
import com.verindrarizya.ecommerce.databinding.PaymentItemBinding

class PaymentItemAdapter(
    private val onPaymentItemClick: (PaymentItem) -> Unit
) : ListAdapter<PaymentItem, PaymentItemAdapter.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = PaymentItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val binding: PaymentItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(paymentItem: PaymentItem) {
            Glide.with(itemView.context)
                .load(paymentItem.image)
                .placeholder(R.drawable.rounded_image_placeholder)
                .into(binding.ivPaymentLogo)

            binding.tvPaymentTitle.text = paymentItem.label

            binding.root.alpha = if (paymentItem.status) {
                1f
            } else {
                binding.root.background = null
                0.5f
            }

            itemView.setOnClickListener {
                if (paymentItem.status) {
                    onPaymentItemClick(paymentItem)
                }
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PaymentItem>() {
            override fun areItemsTheSame(oldItem: PaymentItem, newItem: PaymentItem): Boolean {
                return oldItem.label == newItem.label
            }

            override fun areContentsTheSame(oldItem: PaymentItem, newItem: PaymentItem): Boolean {
                return oldItem == newItem
            }
        }
    }
}
