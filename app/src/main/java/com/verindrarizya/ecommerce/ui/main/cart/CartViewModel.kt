package com.verindrarizya.ecommerce.ui.main.cart

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.CartProductRepository
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.data.local.entity.CartProductEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    private val cartProductRepository: CartProductRepository,
    private val firebaseAnalyticsManager: FirebaseAnalyticsManager
) : ViewModel() {

    private val _allCartProduct = cartProductRepository.getAllCartProduct()
    val allCartProduct = _allCartProduct

    val isEveryProductSelected = _allCartProduct.map {
        it.all { product -> product.isSelected }
    }

    val isAnyProductSelected = _allCartProduct.map {
        it.any { product -> product.isSelected }
    }

    val sumPrice = _allCartProduct.map {
        it.filter { product -> product.isSelected }
            .sumOf { product -> product.price * product.quantity }
    }

    fun deleteFromCart(cartProductEntity: CartProductEntity) {
        viewModelScope.launch {
            cartProductRepository.deleteCartProducts(arrayOf(cartProductEntity))
        }
    }

    fun updateCartProduct(cartProductEntity: CartProductEntity) {
        viewModelScope.launch {
            cartProductRepository.updateProduct(cartProductEntity)
        }
    }

    fun updateSelectedAllProduct(flag: Boolean) {
        viewModelScope.launch {
            cartProductRepository.updateAllSelectedProduct(flag)
        }
    }

    fun deleteSelectedProduct() {
        viewModelScope.launch {
            val carts = _allCartProduct.first()
            val checkedCarts = carts.filter { it.isSelected }.toTypedArray()
            firebaseAnalyticsManager.removeFromCart(checkedCarts)
            cartProductRepository.deleteCartProducts(checkedCarts)
        }
    }
}
