package com.verindrarizya.ecommerce.ui.authentication.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.databinding.FragmentRegisterBinding
import com.verindrarizya.ecommerce.utils.Resource
import com.verindrarizya.ecommerce.utils.getErrorResponse
import com.verindrarizya.ecommerce.utils.showShortToast
import dagger.hilt.android.AndroidEntryPoint
import okio.IOException
import retrofit2.HttpException
import javax.inject.Inject

@AndroidEntryPoint
class RegisterFragment : Fragment() {

    private val viewModel: RegisterViewModel by viewModels()

    private var _binding: FragmentRegisterBinding? = null
    private val binding
        get() = _binding!!

    @Inject
    lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setValidationForTextField()
        setListenerForTextField()
        setButtonClickListener()

        // button register enabled state
        viewModel.isButtonEnabled.observe(viewLifecycleOwner) {
            binding.btnRegister.isEnabled = it
        }

        viewModel.registerState.observe(viewLifecycleOwner) { resource ->
            binding.progressBar.isVisible = resource is Resource.Loading
            binding.btnRegister.isEnabled = resource !is Resource.Loading
            binding.btnLogin.isEnabled = resource !is Resource.Loading

            if (resource is Resource.Success) {
                Toast.makeText(context, "Register Success", Toast.LENGTH_SHORT).show()
            }

            if (resource is Resource.Failure) {
                when (val error = resource.throwable) {
                    is HttpException -> {
                        if (error.code() == 400) {
                            val errorResponse = error.getErrorResponse()
                            context?.let { showShortToast(it, errorResponse.message) }
                            binding.tfEmail.error = errorResponse.message
                        }
                    }

                    is IOException -> {
                        context?.let {
                            showShortToast(it, getString(R.string.your_connection_is_unavailable))
                        }
                    }

                    else -> {
                        context?.let {
                            showShortToast(
                                it,
                                getString(R.string.general_error_statement)
                            )
                        }
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setValidationForTextField() {
        viewModel.isPasswordValid.observe(viewLifecycleOwner) {
            if (it || binding.edPassword.text.toString().isBlank()) {
                binding.tfPassword.isErrorEnabled = false
                binding.tfPassword.error = null
            } else {
                binding.tfPassword.error = getString(R.string.error_password_text_field_not_valid)
            }
        }

        viewModel.isEmailValid.observe(viewLifecycleOwner) {
            if (it || binding.edEmail.text.toString().isBlank()) {
                binding.tfEmail.isErrorEnabled = false
                binding.tfEmail.error = null
            } else {
                binding.tfEmail.error = getString(R.string.error_email_text_field_not_valid)
            }
        }
    }

    private fun setListenerForTextField() {
        binding.edEmail.doOnTextChanged { text, _, _, _ ->
            viewModel.checkEmailValid(text.toString())
        }

        binding.edPassword.doOnTextChanged { text, _, _, _ ->
            viewModel.checkPassword(text.toString())
        }
    }

    private fun setButtonClickListener() {
        binding.btnLogin.setOnClickListener {
            firebaseAnalyticsManager.buttonClickEvent("button login")
            findNavController().navigate(
                RegisterFragmentDirections.actionRegisterFragmentToLoginFragment()
            )
        }

        binding.btnRegister.setOnClickListener {
            firebaseAnalyticsManager.buttonClickEvent("button register")
            viewModel.register(
                email = binding.edEmail.text.toString(),
                password = binding.edPassword.text.toString()
            )
        }
    }
}
