package com.verindrarizya.ecommerce.ui.main.dashboard.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.databinding.FragmentTransactionBinding
import com.verindrarizya.ecommerce.ui.main.dashboard.DashboardFragmentDirections
import com.verindrarizya.ecommerce.ui.main.fulfillment.FulfillmentFragment
import com.verindrarizya.ecommerce.ui.main.model.toFulfillment
import com.verindrarizya.ecommerce.utils.Resource
import com.verindrarizya.ecommerce.utils.errorLayoutHandler
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class TransactionFragment : Fragment() {

    private var _binding: FragmentTransactionBinding? = null
    private val binding: FragmentTransactionBinding
        get() = _binding!!

    private val viewModel: TransactionViewModel by viewModels()

    private lateinit var rvAdapter: TransactionAdapter

    @Inject
    lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTransactionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setInitRvAdapterAndLayoutManager()

        setFulfillmentResultListener()

        viewModel.transactionHistoryFlow.observe(viewLifecycleOwner) {
            binding.progressBar.isVisible = it is Resource.Loading
            binding.rvTransaction.isVisible = it is Resource.Success
            binding.errorLayout.root.isVisible = it is Resource.Failure

            if (it is Resource.Success) {
                val listTransaction = it.data.data
                rvAdapter.submitList(listTransaction)
            } else if (it is Resource.Failure) {
                context?.let { c ->
                    errorLayoutHandler(
                        context = c,
                        binding = binding.errorLayout,
                        error = it.throwable,
                        buttonText = getString(R.string.refresh),
                        onButtonClick = { viewModel.getTransactionHistory() }
                    )
                }
            }
        }
    }

    private fun setFulfillmentResultListener() {
        requireActivity().supportFragmentManager.setFragmentResultListener(
            FulfillmentFragment.FULFILLMENT_REQUEST_KEY,
            viewLifecycleOwner
        ) { _, _ ->
            viewModel.getTransactionHistory()
        }
    }

    private fun setInitRvAdapterAndLayoutManager() {
        rvAdapter = TransactionAdapter(
            onActionReview = { transactionData ->
                firebaseAnalyticsManager.buttonClickEvent("button transaction review")
                val parentNavController =
                    requireActivity().findNavController(R.id.nav_host_fragment)
                parentNavController.navigate(
                    DashboardFragmentDirections.actionDashboardFragmentToFulfillmentFragment(
                        fulfillment = transactionData.toFulfillment(),
                        isFromCheckout = false
                    )
                )
            }
        )
        binding.rvTransaction.layoutManager = LinearLayoutManager(context)
        binding.rvTransaction.adapter = rvAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
