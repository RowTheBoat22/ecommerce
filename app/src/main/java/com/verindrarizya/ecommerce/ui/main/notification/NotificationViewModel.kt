package com.verindrarizya.ecommerce.ui.main.notification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.NotifRepository
import com.verindrarizya.ecommerce.data.local.entity.NotificationEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(
    private val notifRepository: NotifRepository
) : ViewModel() {

    val allNotification = notifRepository.allNotification

    fun setReadNotification(notificationEntity: NotificationEntity) {
        viewModelScope.launch {
            val newState = notificationEntity.copy(isRead = true)
            notifRepository.updateNotif(newState)
        }
    }
}
