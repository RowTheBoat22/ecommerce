package com.verindrarizya.ecommerce.ui.main.cart

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.data.local.entity.CartProductEntity
import com.verindrarizya.ecommerce.databinding.CartItemBinding
import com.verindrarizya.ecommerce.utils.CurrencyHelper

class CartAdapter(
    private val onActionCheckbox: (CartProductEntity) -> Unit,
    private val onActionDelete: (CartProductEntity) -> Unit,
    private val onActionQuantityDecrease: (CartProductEntity) -> Unit,
    private val onActionQuantityIncrease: (CartProductEntity) -> Unit,
) : ListAdapter<CartProductEntity, CartAdapter.CartItemViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartItemViewHolder {
        val binding = CartItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CartItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CartItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class CartItemViewHolder(private val binding: CartItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(cartProductEntity: CartProductEntity) {
            binding.tgQuantity.addOnButtonCheckedListener { group, _, _ ->
                group.clearChecked()
            }
            binding.cbSelect.isChecked = cartProductEntity.isSelected

            Glide.with(itemView.context)
                .load(cartProductEntity.image)
                .placeholder(R.drawable.image_placeholder)
                .into(binding.ivItem)

            binding.tvProductName.text = cartProductEntity.productName
            binding.tvProductVarian.text = cartProductEntity.productVariantName

            binding.tvProductStock.text = if (cartProductEntity.stock >= 10) {
                itemView.context.getString(R.string.stock_quantity, cartProductEntity.stock)
            } else {
                binding.tvProductStock.setTextColor(
                    ContextCompat.getColor(
                        itemView.context,
                        R.color.text_warning
                    )
                )
                itemView.context.getString(
                    R.string.stock_quantity_nearly_run_out,
                    cartProductEntity.stock
                )
            }

            binding.tvPrice.text = CurrencyHelper.convertToRupiah(
                cartProductEntity.price * cartProductEntity.quantity
            )
            binding.btnTextQuantity.text = cartProductEntity.quantity.toString()

            binding.cbSelect.setOnClickListener { onActionCheckbox(cartProductEntity) }
            binding.ivDelete.setOnClickListener {
                onActionDelete(cartProductEntity)
            }
            binding.btnQuantityDecrease.setOnClickListener {
                if (cartProductEntity.quantity > 1) {
                    onActionQuantityDecrease(cartProductEntity)
                }
            }
            binding.btnQuantityIncrease.setOnClickListener {
                if (cartProductEntity.quantity < cartProductEntity.stock) {
                    onActionQuantityIncrease(cartProductEntity)
                }
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<CartProductEntity>() {
            override fun areItemsTheSame(
                oldItem: CartProductEntity,
                newItem: CartProductEntity
            ): Boolean {
                return oldItem.productId == newItem.productId
            }

            override fun areContentsTheSame(
                oldItem: CartProductEntity,
                newItem: CartProductEntity
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}
