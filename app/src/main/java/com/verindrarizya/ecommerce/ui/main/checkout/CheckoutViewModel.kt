package com.verindrarizya.ecommerce.ui.main.checkout

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.verindrarizya.ecommerce.data.CartProductRepository
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.data.PaymentRepository
import com.verindrarizya.ecommerce.ui.main.model.Fulfillment
import com.verindrarizya.ecommerce.ui.main.model.ListProduct
import com.verindrarizya.ecommerce.ui.main.model.Payment
import com.verindrarizya.ecommerce.ui.main.model.Product
import com.verindrarizya.ecommerce.ui.main.model.toCartProductEntity
import com.verindrarizya.ecommerce.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CheckoutViewModel @Inject constructor(
    private val paymentRepository: PaymentRepository,
    private val cartProductRepository: CartProductRepository,
    private val savedStateHandle: SavedStateHandle,
    private val firebaseAnalyticsManager: FirebaseAnalyticsManager
) : ViewModel() {

    private val _checkoutProducts: MutableLiveData<List<Product>> = MutableLiveData()
    val checkoutProducts: LiveData<List<Product>>
        get() = _checkoutProducts

    val sumOfPrice: LiveData<Int> = checkoutProducts.map { products ->
        products.sumOf { it.price * it.quantity }
    }

    private val _fulfillmentFlow: MutableLiveData<Resource<Fulfillment>> = MutableLiveData()
    val fulfillmentFlow: LiveData<Resource<Fulfillment>>
        get() = _fulfillmentFlow

    private val _selectedPayment: MutableLiveData<Payment?> = MutableLiveData(null)
    val selectedPayment: LiveData<Payment?>
        get() = _selectedPayment

    init {
        getCheckoutProducts()
    }

    private fun getCheckoutProducts() {
        // same nav argument name defined in navgraph
        val listProductCheckout = savedStateHandle.get<ListProduct>("listProductCheckout")
        listProductCheckout?.let { firebaseAnalyticsManager.beginCheckoutEvent(it) }
        _checkoutProducts.value = listProductCheckout?.data
    }

    fun setSelectedPayment(payment: Payment?) {
        payment?.let { firebaseAnalyticsManager.addPaymentInfoEvent(it.label) }
        _selectedPayment.value = payment
    }

    fun updateQuantity(product: Product) {
        // just to make this non nullable
        val currentCheckoutProducts = checkoutProducts.value ?: emptyList()

        val index = currentCheckoutProducts.indexOfFirst { it.productId == product.productId }

        val updatedCheckoutProducts = currentCheckoutProducts.toMutableList()
        updatedCheckoutProducts[index] = product

        _checkoutProducts.value = updatedCheckoutProducts
    }

    fun checkout() {
        viewModelScope.launch {
            paymentRepository.fulfillment(
                paymentMethod = selectedPayment.value!!.label,
                checkoutProducts = checkoutProducts.value!!
            ).collect {
                _fulfillmentFlow.value = it
                if (it is Resource.Success) {
                    val fulfillment = it.data
                    firebaseAnalyticsManager.purchaseEvent(fulfillment)

                    val cartProducts = checkoutProducts.value!!
                    val cartProductEntities =
                        cartProducts.map { cp -> cp.toCartProductEntity() }.toTypedArray()
                    firebaseAnalyticsManager.removeFromCart(cartProductEntities)
                    cartProductRepository.deleteCartProducts(cartProductEntities)
                }
            }
        }
    }
}
