package com.verindrarizya.ecommerce.ui.theme

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.verindrarizya.ecommerce.R
import com.verindrarizya.ecommerce.utils.getErrorResponse
import retrofit2.HttpException
import java.io.IOException

@Composable
fun ErrorLayout(
    modifier: Modifier = Modifier,
    error: Throwable,
    buttonText: String? = null,
    onRetryClick: () -> Unit
) {
    when (error) {
        is HttpException -> {
            val errorResponse = error.getErrorResponse()

            val title: String
            val description: String

            if (errorResponse.code == 404) {
                title = stringResource(R.string.empty)
                description = stringResource(R.string.empty_description)
            } else {
                title = errorResponse.code.toString()
                description = errorResponse.message
            }

            ErrorLayout(
                modifier = modifier,
                errorTitle = title,
                errorDesc = description,
                buttonText = buttonText,
                onRetryClick = onRetryClick
            )
        }

        is IOException -> {
            ErrorLayout(
                modifier = modifier,
                errorTitle = stringResource(R.string.connection),
                errorDesc = stringResource(R.string.your_connection_is_unavailable),
                buttonText = buttonText,
                onRetryClick = onRetryClick
            )
        }
    }
}

@Composable
fun ErrorLayout(
    modifier: Modifier = Modifier,
    errorTitle: String,
    errorDesc: String,
    buttonText: String?,
    onRetryClick: () -> Unit
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            modifier = Modifier
                .height(128.dp),
            painter = painterResource(id = R.drawable.error_smartphone),
            contentDescription = null,
            contentScale = ContentScale.FillHeight
        )
        Spacer(Modifier.height(8.dp))
        Text(
            text = errorTitle,
            style = textStyleRemoveFontPadding,
            fontFamily = Poppins,
            fontSize = 32.sp,
            fontWeight = FontWeight.Medium
        )
        Spacer(Modifier.height(4.dp))
        Text(
            text = errorDesc,
            style = textStyleRemoveFontPadding,
            fontFamily = Poppins,
            fontSize = 16.sp,
            fontWeight = FontWeight.Normal
        )
        if (buttonText != null) {
            Spacer(Modifier.height(8.dp))
            Button(onClick = onRetryClick) {
                Text(
                    text = buttonText,
                    fontSize = 14.sp,
                    fontFamily = Poppins,
                    fontWeight = FontWeight.Medium,
                    style = textStyleRemoveFontPadding
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ErrorLayoutPreview() {
    ErrorLayout(
        errorTitle = "Empty",
        errorDesc = "Your requested data is unavailable",
        buttonText = "Refresh",
        onRetryClick = {}
    )
}
