package com.verindrarizya.ecommerce.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.verindrarizya.ecommerce.data.ProductRepository
import com.verindrarizya.ecommerce.expected.DummyStore
import com.verindrarizya.ecommerce.helper.MainDispatcherRule
import com.verindrarizya.ecommerce.helper.getOrAwaitValue
import com.verindrarizya.ecommerce.ui.main.search.StoreSearchViewModel
import com.verindrarizya.ecommerce.utils.Resource
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class StoreSearchViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Mock
    private lateinit var productRepository: ProductRepository

    private lateinit var storeSearchViewModel: StoreSearchViewModel

    @Before
    fun setUp() {
        storeSearchViewModel = StoreSearchViewModel(productRepository)
    }

    @Test
    fun searchStoreResourceLoading() = runTest {
        val productName = "product"
        whenever(productRepository.search(productName)).thenReturn(flowOf(Resource.Loading))

        storeSearchViewModel.searchStore(productName)
        advanceUntilIdle()

        val actualValue = storeSearchViewModel.storeSearchResponse.getOrAwaitValue()

        assertEquals(Resource.Loading, actualValue)
    }

    @Test
    fun searchStoreResourceSuccess() = runTest {
        val productName = "product"
        whenever(productRepository.search(productName)).thenReturn(
            flowOf(
                Resource.Success(
                    DummyStore.searchProductResponse
                )
            )
        )

        storeSearchViewModel.searchStore(productName)
        advanceUntilIdle()

        val actualValue = storeSearchViewModel.storeSearchResponse.getOrAwaitValue()

        assertEquals(Resource.Success(DummyStore.searchProductResponse), actualValue)
    }

    @Test
    fun searchStoreResourceError() = runTest {
        val productName = "product"
        val exception = Exception("Error")
        whenever(productRepository.search(productName)).thenReturn(flowOf(Resource.Failure(exception)))

        storeSearchViewModel.searchStore(productName)
        advanceUntilIdle()

        val actualValue = storeSearchViewModel.storeSearchResponse.getOrAwaitValue()

        assertEquals(Resource.Failure(exception), actualValue)
    }
}
