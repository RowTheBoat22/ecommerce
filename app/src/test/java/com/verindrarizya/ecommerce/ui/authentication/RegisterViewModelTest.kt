package com.verindrarizya.ecommerce.ui.authentication

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.verindrarizya.ecommerce.data.AuthRepository
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.data.PreferencesRepository
import com.verindrarizya.ecommerce.expected.DummyAuth
import com.verindrarizya.ecommerce.helper.MainDispatcherRule
import com.verindrarizya.ecommerce.helper.getOrAwaitValue
import com.verindrarizya.ecommerce.ui.authentication.register.RegisterViewModel
import com.verindrarizya.ecommerce.utils.Resource
import junit.framework.TestCase
import kotlinx.coroutines.flow.flowOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class RegisterViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Mock
    private lateinit var authRepository: AuthRepository

    @Mock
    private lateinit var preferencesRepository: PreferencesRepository

    @Mock
    private lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    private lateinit var registerViewModel: RegisterViewModel

    private val email = "bahar@gmail.com"
    private val password = "123456789"

    @Before
    fun setup() {
        registerViewModel =
            RegisterViewModel(authRepository, preferencesRepository, firebaseAnalyticsManager)
    }

    @Test
    fun checkIsEmailValidCaseResultTrue() {
        registerViewModel.checkEmailValid("indra@gmail.com")

        val actualValue = registerViewModel.isEmailValid.getOrAwaitValue()

        TestCase.assertEquals(true, actualValue)
    }

    @Test
    fun checkIsEmailValidBlankCaseResultFalse() {
        registerViewModel.checkEmailValid("")

        val actualValue = registerViewModel.isEmailValid.getOrAwaitValue()

        TestCase.assertEquals(false, actualValue)
    }

    @Test
    fun checkIsEmailNotValidEmailCaseResultFalse() {
        registerViewModel.checkEmailValid("notvalid.")

        val actualValue = registerViewModel.isEmailValid.getOrAwaitValue()

        TestCase.assertEquals(false, actualValue)
    }

    @Test
    fun checkIsPasswordValidCaseResultTrue() {
        registerViewModel.checkPassword("123456789")

        val actualValue = registerViewModel.isPasswordValid.getOrAwaitValue()

        TestCase.assertEquals(true, actualValue)
    }

    @Test
    fun checkIsPasswordBlankCaseResultFalse() {
        registerViewModel.checkPassword("")

        val actualValue = registerViewModel.isPasswordValid.getOrAwaitValue()

        TestCase.assertEquals(false, actualValue)
    }

    @Test
    fun checkIsPasswordLengthNotSufficeCaseResultFalse() {
        registerViewModel.checkPassword("123a")

        val actualValue = registerViewModel.isPasswordValid.getOrAwaitValue()

        TestCase.assertEquals(false, actualValue)
    }

    @Test
    fun registerResultLoading() {
        whenever(
            authRepository.register(
                email,
                password
            )
        ).thenReturn(flowOf(Resource.Loading))

        registerViewModel.register(email, password)
        val actualValue = registerViewModel.registerState.getOrAwaitValue()

        TestCase.assertEquals(Resource.Loading, actualValue)
    }

    @Test
    fun registerResultSuccess() {
        whenever(
            authRepository.register(
                email,
                password
            )
        ).thenReturn(flowOf(Resource.Success(DummyAuth.registerResponse)))

        registerViewModel.register(email, password)
        val actualValue = registerViewModel.registerState.getOrAwaitValue()

        TestCase.assertEquals(Resource.Success(DummyAuth.registerResponse), actualValue)
    }

    @Test
    fun registerResultError() {
        val exception = Exception()

        whenever(
            authRepository.register(
                email,
                password
            )
        ).thenReturn(flowOf(Resource.Failure(exception)))

        registerViewModel.register(email, password)
        val actualValue = registerViewModel.registerState.getOrAwaitValue()

        TestCase.assertEquals(Resource.Failure(exception), actualValue)
    }
}
