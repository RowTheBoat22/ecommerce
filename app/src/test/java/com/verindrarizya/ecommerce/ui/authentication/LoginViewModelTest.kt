@file:OptIn(ExperimentalCoroutinesApi::class)

package com.verindrarizya.ecommerce.ui.authentication

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.verindrarizya.ecommerce.data.AuthRepository
import com.verindrarizya.ecommerce.data.FirebaseAnalyticsManager
import com.verindrarizya.ecommerce.data.PreferencesRepository
import com.verindrarizya.ecommerce.expected.DummyAuth
import com.verindrarizya.ecommerce.helper.MainDispatcherRule
import com.verindrarizya.ecommerce.helper.getOrAwaitValue
import com.verindrarizya.ecommerce.ui.authentication.login.LoginViewModel
import com.verindrarizya.ecommerce.utils.Resource
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Mock
    private lateinit var authRepository: AuthRepository

    @Mock
    private lateinit var preferencesRepository: PreferencesRepository

    @Mock
    private lateinit var firebaseAnalyticsManager: FirebaseAnalyticsManager

    private lateinit var loginViewModel: LoginViewModel

    private val email = "bahar@gmail.com"
    private val password = "123456789"

    // error somehow the dependency is not mocked
    //    @Before
    //    fun setup() {
    //        loginViewModel =
    //            LoginViewModel(authRepository, preferencesRepository, firebaseAnalyticsManager)
    //    }

    @Test
    fun isAlreadyOnBoardedTrue() = runTest {
        setViewModelDependency {
            whenever(preferencesRepository.isUserAlreadyOnBoard).thenReturn(
                flowOf(true)
            )
        }

        val actualValue = loginViewModel.isAlreadyOnBoarded.getOrAwaitValue()

        assertEquals(true, actualValue)
    }

    @Test
    fun isAlreadyOnBoardedFalse() = runTest {
        setViewModelDependency {
            whenever(preferencesRepository.isUserAlreadyOnBoard).thenReturn(flowOf(false))
        }

        val actualValue = loginViewModel.isAlreadyOnBoarded.getOrAwaitValue()

        assertEquals(false, actualValue)
    }

    @Test
    fun checkIsEmailValidCaseResultTrue() {
        setViewModelDependency()

        loginViewModel.checkEmailValid("indra@gmail.com")

        val actualValue = loginViewModel.isEmailValid.getOrAwaitValue()

        assertEquals(true, actualValue)
    }

    @Test
    fun checkIsEmailValidBlankCaseResultFalse() {
        setViewModelDependency()

        loginViewModel.checkEmailValid("")

        val actualValue = loginViewModel.isEmailValid.getOrAwaitValue()

        assertEquals(false, actualValue)
    }

    @Test
    fun checkIsEmailNotValidEmailCaseResultFalse() {
        setViewModelDependency()

        loginViewModel.checkEmailValid("notvalid.")

        val actualValue = loginViewModel.isEmailValid.getOrAwaitValue()

        assertEquals(false, actualValue)
    }

    @Test
    fun checkIsPasswordValidCaseResultTrue() {
        setViewModelDependency()

        loginViewModel.checkPassword("123456789")

        val actualValue = loginViewModel.isPasswordValid.getOrAwaitValue()

        assertEquals(true, actualValue)
    }

    @Test
    fun checkIsPasswordBlankCaseResultFalse() {
        setViewModelDependency()

        loginViewModel.checkPassword("")

        val actualValue = loginViewModel.isPasswordValid.getOrAwaitValue()

        assertEquals(false, actualValue)
    }

    @Test
    fun checkIsPasswordLengthNotSufficeCaseResultFalse() {
        setViewModelDependency()

        loginViewModel.checkPassword("123a")

        val actualValue = loginViewModel.isPasswordValid.getOrAwaitValue()

        assertEquals(false, actualValue)
    }

    @Test
    fun loginResultLoading() {
        setViewModelDependency()

        whenever(
            authRepository.login(
                email,
                password
            )
        ).thenReturn(flowOf(Resource.Loading))

        loginViewModel.login(email, password)
        val actualValue = loginViewModel.loginState.getOrAwaitValue()

        assertEquals(Resource.Loading, actualValue)
    }

    @Test
    fun loginResultSuccess() {
        setViewModelDependency()

        whenever(
            authRepository.login(
                email,
                password
            )
        ).thenReturn(flowOf(Resource.Success(DummyAuth.loginResponse)))

        loginViewModel.login(email, password)
        val actualValue = loginViewModel.loginState.getOrAwaitValue()

        assertEquals(Resource.Success(DummyAuth.loginResponse), actualValue)
    }

    @Test
    fun loginResultError() {
        setViewModelDependency()
        val exception = Exception()

        whenever(
            authRepository.login(
                email,
                password
            )
        ).thenReturn(flowOf(Resource.Failure(exception)))

        loginViewModel.login(email, password)
        val actualValue = loginViewModel.loginState.getOrAwaitValue()

        assertEquals(Resource.Failure(exception), actualValue)
    }

    private fun setViewModelDependency(
        block: () -> Unit = {
            whenever(preferencesRepository.isUserAlreadyOnBoard).thenReturn(flowOf(false))
        }
    ) {
        block()
        loginViewModel =
            LoginViewModel(authRepository, preferencesRepository, firebaseAnalyticsManager)
    }
}
