package com.verindrarizya.ecommerce.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.verindrarizya.ecommerce.data.NotifRepository
import com.verindrarizya.ecommerce.expected.DummyStore
import com.verindrarizya.ecommerce.helper.MainDispatcherRule
import com.verindrarizya.ecommerce.helper.getOrAwaitValue
import com.verindrarizya.ecommerce.ui.main.notification.NotificationViewModel
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.flowOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class NotificationViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Mock
    private lateinit var notifRepository: NotifRepository

    private lateinit var notificationViewModel: NotificationViewModel

    val listNotif = listOf(DummyStore.notificationEntity)

    @Test
    fun getNotification() {
        whenever(notifRepository.allNotification).thenReturn(flowOf(listNotif))
        notificationViewModel = NotificationViewModel(notifRepository)

        val allNotification = notificationViewModel.allNotification.getOrAwaitValue()

        assertEquals(listNotif, allNotification)
    }
}
