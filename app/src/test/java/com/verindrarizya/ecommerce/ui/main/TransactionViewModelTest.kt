package com.verindrarizya.ecommerce.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.verindrarizya.ecommerce.data.PaymentRepository
import com.verindrarizya.ecommerce.expected.DummyFulfillment
import com.verindrarizya.ecommerce.helper.MainDispatcherRule
import com.verindrarizya.ecommerce.helper.getOrAwaitValue
import com.verindrarizya.ecommerce.ui.main.dashboard.transaction.TransactionViewModel
import com.verindrarizya.ecommerce.utils.Resource
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.flowOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class TransactionViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Mock
    private lateinit var paymentRepository: PaymentRepository

    private lateinit var transactionViewModel: TransactionViewModel

    @Before
    fun setup() {
        transactionViewModel = TransactionViewModel(paymentRepository)
    }

    @Test
    fun getTransactionHistoryResultLoading() {
        whenever(paymentRepository.transactionHistory()).thenReturn(flowOf(Resource.Loading))

        transactionViewModel.getTransactionHistory()

        val actualValue = transactionViewModel.transactionHistoryFlow.getOrAwaitValue()

        assertEquals(Resource.Loading, actualValue)
    }

    @Test
    fun getTransactionHistoryResultSuccess() {
        whenever(paymentRepository.transactionHistory()).thenReturn(
            flowOf(
                Resource.Success(
                    DummyFulfillment.transactionResponse
                )
            )
        )

        transactionViewModel.getTransactionHistory()

        val actualValue = transactionViewModel.transactionHistoryFlow.getOrAwaitValue()

        assertEquals(Resource.Success(DummyFulfillment.transactionResponse), actualValue)
    }

    @Test
    fun getTransactionHistoryResultError() {
        val exception = Exception("Error")
        whenever(paymentRepository.transactionHistory()).thenReturn(
            flowOf(
                Resource.Failure(
                    exception,
                )
            )
        )

        transactionViewModel.getTransactionHistory()

        val actualValue = transactionViewModel.transactionHistoryFlow.getOrAwaitValue()

        assertEquals(Resource.Failure(exception), actualValue)
    }
}
