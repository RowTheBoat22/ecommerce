package com.verindrarizya.ecommerce.ui.onboarding

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.verindrarizya.ecommerce.data.PreferencesRepository
import com.verindrarizya.ecommerce.helper.MainDispatcherRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.verify

@RunWith(MockitoJUnitRunner::class)
class OnBoardingViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var preferencesRepository: PreferencesRepository

    private lateinit var viewModel: OnBoardingViewModel

    @Before
    fun setup() {
        viewModel = OnBoardingViewModel(preferencesRepository)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun verifySetOnBoardedCalled() = runTest {
        viewModel.setOnBoarded()
        advanceUntilIdle()

        verify(preferencesRepository).setOnBoarded()
    }
}
