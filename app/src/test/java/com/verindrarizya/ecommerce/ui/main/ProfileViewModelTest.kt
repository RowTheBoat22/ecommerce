package com.verindrarizya.ecommerce.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.verindrarizya.ecommerce.data.AuthRepository
import com.verindrarizya.ecommerce.data.PreferencesRepository
import com.verindrarizya.ecommerce.expected.DummyAuth
import com.verindrarizya.ecommerce.helper.MainDispatcherRule
import com.verindrarizya.ecommerce.helper.getOrAwaitValue
import com.verindrarizya.ecommerce.ui.main.profile.ProfileViewModel
import com.verindrarizya.ecommerce.utils.Resource
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.flowOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class ProfileViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Mock
    private lateinit var preferencesRepository: PreferencesRepository

    @Mock
    private lateinit var authRepository: AuthRepository

    private lateinit var profileViewModel: ProfileViewModel

    private val username = "Indra"

    @Before
    fun setup() {
        whenever(preferencesRepository.isUserNameAlreadySet).thenReturn(flowOf(true))
        profileViewModel = ProfileViewModel(preferencesRepository, authRepository)
    }

    @Test
    fun isNameValidCaseResultTrue() {
        profileViewModel.checkName("nama saya")

        val actualValue = profileViewModel.isNameValid.getOrAwaitValue()

        assertEquals(true, actualValue)
    }

    @Test
    fun isNameBlankCaseResultFalse() {
        profileViewModel.checkName("")

        val actualValue = profileViewModel.isNameValid.getOrAwaitValue()

        assertEquals(false, actualValue)
    }

    @Test
    fun setProfileResourceLoading() {
        profileViewModel.checkName("Indra")

        whenever(authRepository.profile(username, null)).thenReturn(flowOf(Resource.Loading))

        profileViewModel.setProfile(null)

        val actualValue = profileViewModel.profileState.getOrAwaitValue()

        assertEquals(Resource.Loading, actualValue)
    }

    @Test
    fun setProfileResourceSuccess() {
        profileViewModel.checkName("Indra")

        whenever(authRepository.profile(username, null)).thenReturn(
            flowOf(
                Resource.Success(
                    DummyAuth.profileResponse
                )
            )
        )

        profileViewModel.setProfile(null)

        val actualValue = profileViewModel.profileState.getOrAwaitValue()

        assertEquals(Resource.Success(DummyAuth.profileResponse), actualValue)
    }

    @Test
    fun setProfileResourceError() {
        val exception = Exception()
        profileViewModel.checkName(username)

        whenever(authRepository.profile(username, null)).thenReturn(
            flowOf(
                Resource.Failure(
                    exception
                )
            )
        )

        profileViewModel.setProfile(null)

        val actualValue = profileViewModel.profileState.getOrAwaitValue()

        assertEquals(Resource.Failure(exception), actualValue)
    }
}
