package com.verindrarizya.ecommerce.helper

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.SocketPolicy

fun setApiCall(
    code: Int,
    responseResourcePath: String,
    socketPolicy: SocketPolicy? = null,
    mockWebServer: MockWebServer
) {
    val reader = MockResponseFileReader(responseResourcePath)

    val mockResponse = MockResponse()
        .setResponseCode(code)
        .setBody(reader.content)

    socketPolicy?.let { mockResponse.setSocketPolicy(it) }

    mockWebServer.enqueue(mockResponse)
}
