package com.verindrarizya.ecommerce.data

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.data.remote.request.FulfillmentRequest
import com.verindrarizya.ecommerce.expected.DummyFulfillment
import com.verindrarizya.ecommerce.ui.main.model.toFulfillment
import com.verindrarizya.ecommerce.utils.Resource
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class PaymentRepositoryTest {

    @Mock
    private lateinit var apiService: ApiService

    @Mock
    private lateinit var firebaseRemoteConfig: FirebaseRemoteConfig

    private lateinit var paymentRepository: PaymentRepository

    private val fulfillmentRequest = FulfillmentRequest(
        payment = "payment",
        items = listOf()
    )

    @Before
    fun setup() {
        paymentRepository = PaymentRepository(apiService, firebaseRemoteConfig)
    }

    @Test
    fun fulfillmentEmitLoading() = runTest {
        whenever(apiService.fulfillment(fulfillmentRequest)).thenReturn(DummyFulfillment.fulfillmentResponse)

        val actualResult = paymentRepository.fulfillment("payment", listOf()).first()

        assertEquals(Resource.Loading, actualResult)
    }

    @Test
    fun fulfillmentResultSuccess() = runTest {
        whenever(apiService.fulfillment(fulfillmentRequest)).thenReturn(DummyFulfillment.fulfillmentResponse)

        val actualResult = paymentRepository.fulfillment("payment", listOf()).drop(1).first()

        assertEquals(
            Resource.Success(DummyFulfillment.fulfillmentResponse.fulfillmentData.toFulfillment()),
            actualResult
        )
    }

    @Test
    fun fulfillmentResultError() = runTest {
        val exception = RuntimeException()
        whenever(apiService.fulfillment(fulfillmentRequest)).thenThrow(exception)

        val actualResult = paymentRepository.fulfillment("payment", listOf()).drop(1).first()

        assertEquals(
            Resource.Failure(exception),
            actualResult
        )
    }

    @Test
    fun transactionHistoryEmitLoading() = runTest {
        whenever(apiService.transaction()).thenReturn(DummyFulfillment.transactionResponse)

        val actualResult = paymentRepository.transactionHistory().first()

        assertEquals(Resource.Loading, actualResult)
    }

    @Test
    fun transactionHistoryResultSuccess() = runTest {
        whenever(apiService.transaction()).thenReturn(DummyFulfillment.transactionResponse)

        val actualResult = paymentRepository.transactionHistory().drop(1).first()

        assertEquals(
            Resource.Success(DummyFulfillment.transactionResponse),
            actualResult
        )
    }

    @Test
    fun transactionHistoryResultError() = runTest {
        val exception = RuntimeException()
        whenever(apiService.transaction()).thenThrow(exception)

        val actualResult = paymentRepository.transactionHistory().drop(1).first()

        assertEquals(
            Resource.Failure(exception),
            actualResult
        )
    }
}
