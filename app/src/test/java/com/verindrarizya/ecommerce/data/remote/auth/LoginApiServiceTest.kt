package com.verindrarizya.ecommerce.data.remote.auth

import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.data.remote.request.UserRequest
import com.verindrarizya.ecommerce.expected.DummyAuth
import com.verindrarizya.ecommerce.helper.setApiCall
import com.verindrarizya.ecommerce.utils.getErrorResponse
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.SocketPolicy
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.SocketException

class LoginApiServiceTest {

    private lateinit var mockWebServer: MockWebServer

    private lateinit var apiService: ApiService

    @Before
    fun startServer() {
        mockWebServer = MockWebServer()
        mockWebServer.start()

        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun stopServer() {
        mockWebServer.shutdown()
    }

    @Test
    fun loginAndResultSuccess() = runTest {
        setApiCall(
            code = 200,
            responseResourcePath = "auth/login/login.json",
            mockWebServer = mockWebServer
        )

        val userRequest = UserRequest(
            email = "test@gmail.com",
            password = "123",
            firebaseToken = "123"
        )

        val actualResponse = apiService.login(userRequest)

        val expectedResponse = DummyAuth.loginResponse

        TestCase.assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun loginEmailOrPasswordInvalid() = runTest {
        setApiCall(
            code = 400,
            responseResourcePath = "auth/login/email_or_password_is_not_valid.json",
            mockWebServer = mockWebServer
        )

        val userRequest = UserRequest(
            email = "",
            password = "",
            firebaseToken = ""
        )

        try {
            apiService.register(userRequest)
        } catch (e: HttpException) {
            val errorResponse = e.getErrorResponse()

            TestCase.assertEquals(DummyAuth.emailOrPasswordNotValid, errorResponse)
        }
    }

    @Test
    fun loginNoConnection() = runTest {
        setApiCall(
            code = 0,
            responseResourcePath = "",
            mockWebServer = mockWebServer,
            socketPolicy = SocketPolicy.DISCONNECT_AT_START
        )

        val userRequest = UserRequest(
            email = "",
            password = "",
            firebaseToken = ""
        )

        try {
            apiService.login(userRequest)
        } catch (e: Exception) {
            TestCase.assertEquals(true, e is SocketException)
        }
    }
}
