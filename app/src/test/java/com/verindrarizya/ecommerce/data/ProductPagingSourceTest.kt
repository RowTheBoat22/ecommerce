package com.verindrarizya.ecommerce.data

import androidx.paging.PagingSource
import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.data.remote.response.Product
import com.verindrarizya.ecommerce.expected.DummyStore
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class ProductPagingSourceTest {

    @Mock
    private lateinit var apiService: ApiService

    private lateinit var productPagingSource: ProductPagingSource

    private val search: String = "search"
    private val brand: String = "brand"
    private val lowest: Int = 1
    private val highest: Int = 2
    private val sort: String = "sort"

    @Before
    fun setup() {
        productPagingSource = ProductPagingSource(apiService, search, brand, lowest, highest, sort)
    }

    @Test
    fun loadReturnsPageWhenOnSuccessfulLoadPageKeyedData() = runTest {
        whenever(apiService.products(search, brand, lowest, highest, sort, 10, 1))
            .thenReturn(DummyStore.productsResponse)

        assertEquals(
            PagingSource.LoadResult.Page(
                data = DummyStore.productsResponse.data.items,
                prevKey = null,
                nextKey = 2 // nilai nextPage logic is in ProductPagingSource
            ),
            productPagingSource.load(
                PagingSource.LoadParams.Refresh(
                    key = null,
                    loadSize = 10,
                    placeholdersEnabled = false
                )
            )
        )
    }

    @Test
    fun loadReturnsPageWhenOnFailedLoadPageKeyedData() = runTest {
        val runtimeException = RuntimeException()

        whenever(apiService.products(search, brand, lowest, highest, sort, 10, 1))
            .thenThrow(runtimeException)

        assertEquals(
            PagingSource.LoadResult.Error<Int, Product>(runtimeException),
            productPagingSource.load(
                PagingSource.LoadParams.Refresh(
                    key = null,
                    loadSize = 10,
                    placeholdersEnabled = false
                )
            )
        )
    }
}
