package com.verindrarizya.ecommerce.data.remote.auth

import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.data.remote.request.UserRequest
import com.verindrarizya.ecommerce.expected.DummyAuth
import com.verindrarizya.ecommerce.helper.setApiCall
import com.verindrarizya.ecommerce.utils.getErrorResponse
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.SocketPolicy
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.SocketException

class RegisterApiServiceTest {

    private lateinit var mockWebServer: MockWebServer

    private lateinit var apiService: ApiService

    @Before
    fun startServer() {
        mockWebServer = MockWebServer()
        mockWebServer.start()

        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun stopServer() {
        mockWebServer.shutdown()
    }

    @Test
    fun registerAndResultSuccess() = runTest {
        setApiCall(
            code = 200,
            responseResourcePath = "auth/register/register.json",
            mockWebServer = mockWebServer
        )

        val userRequest = UserRequest(
            email = "test@gmail.com",
            password = "123",
            firebaseToken = "123"
        )

        val actualResponse = apiService.register(userRequest)

        val expectedResponse = DummyAuth.registerResponse

        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun registerEmailIsAlreadyTaken() = runTest {
        setApiCall(
            code = 400,
            responseResourcePath = "auth/register/email_is_already_taken.json",
            mockWebServer = mockWebServer
        )

        val userRequest = UserRequest(
            email = "test@gmail.com",
            password = "123",
            firebaseToken = "123"
        )

        try {
            apiService.register(userRequest)
        } catch (e: HttpException) {
            val errorResponse = e.getErrorResponse()

            assertEquals(DummyAuth.emailIsTakenResponse, errorResponse)
        }
    }

    @Test
    fun registerNoConnection() = runTest {
        setApiCall(
            code = 0,
            responseResourcePath = "",
            mockWebServer = mockWebServer,
            socketPolicy = SocketPolicy.DISCONNECT_AT_START
        )

        val userRequest = UserRequest(
            email = "",
            password = "",
            firebaseToken = ""
        )

        try {
            apiService.register(userRequest)
        } catch (e: Exception) {
            assertEquals(true, e is SocketException)
        }
    }
}
