package com.verindrarizya.ecommerce.data.remote.store

import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.expected.DummyStore
import com.verindrarizya.ecommerce.helper.setApiCall
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class StoreApiServiceTest {

    private lateinit var mockWebServer: MockWebServer

    private lateinit var apiService: ApiService

    @Before
    fun startServer() {
        mockWebServer = MockWebServer()
        mockWebServer.start()

        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun stopServer() {
        mockWebServer.shutdown()
    }

    @Test
    fun searchProductResultSuccess() = runTest {
        setApiCall(
            code = 200,
            responseResourcePath = "store/search_product_response.json",
            mockWebServer = mockWebServer
        )

        val actualResponse = apiService.search("Lenovo")

        val expectedResponse = DummyStore.searchProductResponse

        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun productsResultSuccess() = runTest {
        setApiCall(
            code = 200,
            responseResourcePath = "store/products_response.json",
            mockWebServer = mockWebServer
        )

        val actualResponse = apiService.products(limit = 10, page = 4)

        val expectedResponse = DummyStore.productsResponse

        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun productDetail() = runTest {
        setApiCall(
            code = 200,
            responseResourcePath = "store/product_detail_response.json",
            mockWebServer = mockWebServer
        )

        val actualResponse = apiService.productDetail("1")

        val expectedResponse = DummyStore.productDetailResponse

        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun productReview() = runTest {
        setApiCall(
            code = 200,
            responseResourcePath = "store/product_review_response.json",
            mockWebServer = mockWebServer
        )

        val actualResponse = apiService.productReview("1")

        val expectedResponse = DummyStore.productReview

        assertEquals(expectedResponse, actualResponse)
    }
}
