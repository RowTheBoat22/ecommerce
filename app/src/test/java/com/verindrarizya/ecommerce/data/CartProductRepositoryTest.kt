package com.verindrarizya.ecommerce.data

import com.verindrarizya.ecommerce.data.local.dao.CartProductDao
import com.verindrarizya.ecommerce.data.local.entity.CartProductEntity
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.never
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class CartProductRepositoryTest {

    @Mock
    private lateinit var cartProductDao: CartProductDao

    private lateinit var cartProductRepository: CartProductRepository

    private val cartProductEntity = CartProductEntity(
        productId = "1",
        image = "image",
        productName = "name",
        productVariantName = "varian name",
        stock = 2,
        price = 1,
        quantity = 1,
    )

    @Before
    fun setup() {
        cartProductRepository = CartProductRepository(cartProductDao)
    }

    @Test
    fun insertCartAndNotYetExistInDb() = runTest {
        whenever(cartProductDao.insertToCart(cartProductEntity)).thenReturn(1)

        cartProductRepository.insertToCart(cartProductEntity)

        verify(cartProductDao).insertToCart(cartProductEntity)
    }

    @Test
    fun insertToCartAndUpdateGotCalledInstead() = runTest {
        whenever(cartProductDao.insertToCart(cartProductEntity)).thenReturn(-1L)
        whenever(cartProductDao.getCartProductById(cartProductEntity.productId)).thenReturn(
            cartProductEntity
        )

        cartProductRepository.insertToCart(cartProductEntity)

        verify(cartProductDao).updateProduct(cartProductEntity.copy(quantity = cartProductEntity.quantity + 1))
    }

    @Test
    fun insertToCartAndUpdateNotCalledCauseQuantityIsSameAsStock() = runTest {
        val cartProductEntityQuantityMax =
            cartProductEntity.copy(quantity = cartProductEntity.stock)

        whenever(cartProductDao.insertToCart(cartProductEntityQuantityMax)).thenReturn(1L)
        whenever(cartProductDao.getCartProductById(cartProductEntity.productId)).thenReturn(
            cartProductEntityQuantityMax
        )

        cartProductRepository.insertToCart(cartProductEntityQuantityMax)

        verify(
            cartProductDao,
            never()
        ).updateProduct(cartProductEntityQuantityMax.copy(quantity = cartProductEntityQuantityMax.quantity + 1))
    }
}
