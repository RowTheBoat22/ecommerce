package com.verindrarizya.ecommerce.data.remote.auth

import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.expected.DummyAuth
import com.verindrarizya.ecommerce.helper.setApiCall
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ProfileApiServiceTest {

    private lateinit var mockWebServer: MockWebServer

    private lateinit var apiService: ApiService

    @Before
    fun startServer() {
        mockWebServer = MockWebServer()
        mockWebServer.start()

        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun stopServer() {
        mockWebServer.shutdown()
    }

    @Test
    fun profileAndResultSuccess() = runTest {
        setApiCall(
            code = 200,
            responseResourcePath = "auth/profile/profile.json",
            mockWebServer = mockWebServer
        )

        val username = "bambang".toRequestBody("text/plain".toMediaTypeOrNull())

        val actualResponse = apiService.profile(username, null)

        val expectedResponse = DummyAuth.profileResponse

        assertEquals(expectedResponse, actualResponse)
    }
}
