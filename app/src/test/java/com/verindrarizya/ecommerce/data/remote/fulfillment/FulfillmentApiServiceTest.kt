package com.verindrarizya.ecommerce.data.remote.fulfillment

import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.data.remote.request.FulfillmentRequest
import com.verindrarizya.ecommerce.data.remote.request.RatingRequest
import com.verindrarizya.ecommerce.expected.DummyFulfillment
import com.verindrarizya.ecommerce.helper.setApiCall
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class FulfillmentApiServiceTest {

    private lateinit var mockWebServer: MockWebServer

    private lateinit var apiService: ApiService

    @Before
    fun startServer() {
        mockWebServer = MockWebServer()
        mockWebServer.start()

        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun stopServer() {
        mockWebServer.shutdown()
    }

    @Test
    fun fulfillmentResultSuccess() = runTest {
        setApiCall(
            code = 200,
            responseResourcePath = "fulfillment/fulfillment_response.json",
            mockWebServer = mockWebServer
        )

        val fulfillmentRequest = FulfillmentRequest(
            payment = "",
            items = listOf()
        )

        val actualResponse = apiService.fulfillment(fulfillmentRequest)

        val expectedResponse = DummyFulfillment.fulfillmentResponse

        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun transactionResultSuccess() = runTest {
        setApiCall(
            code = 200,
            responseResourcePath = "fulfillment/transaction_response.json",
            mockWebServer = mockWebServer
        )

        val actualResponse = apiService.transaction()

        val expectedResponse = DummyFulfillment.transactionResponse

        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun ratingResultSuccess() = runTest {
        setApiCall(
            code = 200,
            responseResourcePath = "fulfillment/rating_response.json",
            mockWebServer = mockWebServer
        )

        val ratingRequest = RatingRequest(
            invoiceId = "1",
            rating = 1,
            review = ""
        )

        val actualResponse = apiService.rating(ratingRequest)

        val expectedResponse = DummyFulfillment.ratingResponse

        assertEquals(expectedResponse, actualResponse)
    }
}
