package com.verindrarizya.ecommerce.data

import com.google.android.gms.tasks.Tasks
import com.google.firebase.messaging.FirebaseMessaging
import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.data.remote.request.UserRequest
import com.verindrarizya.ecommerce.expected.DummyAuth
import com.verindrarizya.ecommerce.utils.Resource
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class AuthRepositoryTest {

    @Mock
    private lateinit var apiService: ApiService

    @Mock
    private lateinit var firebaseMessaging: FirebaseMessaging

    private lateinit var authRepository: AuthRepository

    private val userRequest = UserRequest(
        email = "",
        password = "",
        firebaseToken = ""
    )

    private val username = ""
    private val usernameRequestBody = username.toRequestBody("text/plain".toMediaTypeOrNull())

    @Before
    fun setup() {
        firebaseMessaging = mock {
            on { token }.doReturn(Tasks.forResult(""))
        }
        authRepository = AuthRepository(apiService, firebaseMessaging)
    }

    @Test
    fun registerEmitLoading() = runTest {
        whenever(apiService.register(userRequest)).thenReturn(DummyAuth.registerResponse)
        val actualResult = authRepository.register(email = "", password = "").first()

        assertEquals(Resource.Loading, actualResult)
    }

    @Test
    fun registerResultSuccess() = runTest {
        whenever(apiService.register(userRequest)).thenReturn(DummyAuth.registerResponse)
        val actualResult = authRepository.register(email = "", password = "").drop(1).first()

        assertEquals(true, actualResult is Resource.Success)
    }

    @Test
    fun registerResultError() = runTest {
        val runTimeException = RuntimeException()
        whenever(apiService.register(userRequest)).thenThrow(runTimeException)
        val actualResult = authRepository.register(email = "", password = "").drop(1).first()

        assertEquals(Resource.Failure(runTimeException), actualResult)
    }

    @Test
    fun loginEmitLoading() = runTest {
        whenever(apiService.login(userRequest)).thenReturn(DummyAuth.loginResponse)

        val actualResult = authRepository.login("", "").first()

        assertEquals(Resource.Loading, actualResult)
    }

    @Test
    fun loginResultSuccess() = runTest {
        whenever(apiService.login(userRequest)).thenReturn(DummyAuth.loginResponse)

        val actualResult = authRepository.login("", "").drop(1).first()

        assertEquals(Resource.Success(DummyAuth.loginResponse), actualResult)
    }

    @Test
    fun loginResultError() = runTest {
        val runtimeException = RuntimeException()
        whenever(apiService.login(userRequest)).thenThrow(runtimeException)

        val actualResult = authRepository.login("", "").drop(1).first()

        assertEquals(Resource.Failure(runtimeException), actualResult)
    }

    @Test
    fun profileEmitLoading() = runTest {
        val actualResult = authRepository.profile("", null).first()

        assertEquals(Resource.Loading, actualResult)
    }
}
