package com.verindrarizya.ecommerce.data

import com.verindrarizya.ecommerce.data.remote.ApiService
import com.verindrarizya.ecommerce.data.remote.request.RatingRequest
import com.verindrarizya.ecommerce.expected.DummyFulfillment
import com.verindrarizya.ecommerce.expected.DummyStore
import com.verindrarizya.ecommerce.utils.Resource
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class ProductRepositoryTest {

    @Mock
    private lateinit var apiService: ApiService

    private lateinit var productRepository: ProductRepository

    @Before
    fun setUp() {
        productRepository = ProductRepository(apiService)
    }

    private val productName = "lenovo"
    private val productId = "123"
    private val invoiceId = "1234"
    private val rating = 1
    private val review = "review"

    private val ratingRequest = RatingRequest(
        review,
        rating,
        invoiceId
    )

    @Test
    fun searchEmitLoading() = runTest {
        whenever(apiService.search(productName)).thenReturn(DummyStore.searchProductResponse)

        val actualResult = productRepository.search(productName).first()

        assertEquals(Resource.Loading, actualResult)
    }

    @Test
    fun searchResultSuccess() = runTest {
        whenever(apiService.search(productName)).thenReturn(DummyStore.searchProductResponse)

        val actualResult = productRepository.search(productName).drop(1).first()

        assertEquals(Resource.Success(DummyStore.searchProductResponse), actualResult)
    }

    @Test
    fun searchResultError() = runTest {
        val runtimeException = RuntimeException()
        whenever(apiService.search(productName)).thenThrow(runtimeException)

        val actualResult = productRepository.search(productName).drop(1).first()

        assertEquals(Resource.Failure(runtimeException), actualResult)
    }

    @Test
    fun productDetailEmitLoading() = runTest {
        whenever(apiService.productDetail(productId)).thenReturn(DummyStore.productDetailResponse)

        val actualResult = productRepository.productDetail(productId).first()

        assertEquals(Resource.Loading, actualResult)
    }

    @Test
    fun productDetailResultSuccess() = runTest {
        whenever(apiService.productDetail(productId)).thenReturn(DummyStore.productDetailResponse)

        val actualResult = productRepository.productDetail(productId).drop(1).first()

        assertEquals(Resource.Success(DummyStore.productDetailResponse), actualResult)
    }

    @Test
    fun productDetailResultError() = runTest {
        val runtimeException = RuntimeException()
        whenever(apiService.productDetail(productId)).thenThrow(runtimeException)

        val actualResult = productRepository.productDetail(productId).drop(1).first()

        assertEquals(Resource.Failure(runtimeException), actualResult)
    }

    @Test
    fun productReviewEmitLoading() = runTest {
        whenever(apiService.productReview(productId)).thenReturn(DummyStore.productReview)

        val actualResult = productRepository.productReview(productId).first()

        assertEquals(Resource.Loading, actualResult)
    }

    @Test
    fun productReviewResultSuccess() = runTest {
        whenever(apiService.productReview(productId)).thenReturn(DummyStore.productReview)

        val actualResult = productRepository.productReview(productId).drop(1).first()

        assertEquals(Resource.Success(DummyStore.productReview), actualResult)
    }

    @Test
    fun productReviewResultError() = runTest {
        val runtimeException = RuntimeException()
        whenever(apiService.productReview(productId)).thenThrow(runtimeException)

        val actualResult = productRepository.productReview(productId).drop(1).first()

        assertEquals(Resource.Failure(runtimeException), actualResult)
    }

    @Test
    fun ratingEmitLoading() = runTest {
        whenever(apiService.rating(ratingRequest)).thenReturn(DummyFulfillment.ratingResponse)

        val actualResult = productRepository.rating(invoiceId, rating, review).first()

        assertEquals(Resource.Loading, actualResult)
    }

    @Test
    fun ratingResultSuccess() = runTest {
        whenever(apiService.rating(ratingRequest)).thenReturn(DummyFulfillment.ratingResponse)

        val actualResult = productRepository.rating(invoiceId, rating, review).drop(1).first()

        assertEquals(Resource.Success(DummyFulfillment.ratingResponse), actualResult)
    }

    @Test
    fun ratingResultError() = runTest {
        val runtimeException = RuntimeException()
        whenever(apiService.rating(ratingRequest)).thenThrow(runtimeException)

        val actualResult = productRepository.rating(invoiceId, rating, review).drop(1).first()

        assertEquals(Resource.Failure(runtimeException), actualResult)
    }
}
