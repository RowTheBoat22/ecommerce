package com.verindrarizya.ecommerce.expected

import com.verindrarizya.ecommerce.data.remote.response.FulfillmentData
import com.verindrarizya.ecommerce.data.remote.response.FulfillmentResponse
import com.verindrarizya.ecommerce.data.remote.response.RatingResponse
import com.verindrarizya.ecommerce.data.remote.response.TransactionData
import com.verindrarizya.ecommerce.data.remote.response.TransactionItem
import com.verindrarizya.ecommerce.data.remote.response.TransactionResponse

object DummyFulfillment {

    val fulfillmentResponse = FulfillmentResponse(
        code = 200,
        message = "OK",
        fulfillmentData = FulfillmentData(
            invoiceId = "1",
            status = true,
            date = "",
            time = "",
            payment = "",
            total = 1
        )
    )

    val transactionResponse = TransactionResponse(
        code = 200,
        message = "OK",
        data = listOf(
            TransactionData(
                invoiceId = "1",
                status = true,
                date = "",
                time = "",
                payment = "",
                total = 1,
                items = listOf(
                    TransactionItem(
                        productId = "1",
                        variantName = "1",
                        quantity = 1
                    )
                ),
                rating = 1,
                review = "",
                image = "",
                name = ""
            )
        )
    )

    val ratingResponse = RatingResponse(
        code = 200,
        message = "Fulfillment rating and review success"
    )
}
