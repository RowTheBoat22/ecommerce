package com.verindrarizya.ecommerce.expected

import com.verindrarizya.ecommerce.data.remote.response.AuthData
import com.verindrarizya.ecommerce.data.remote.response.AuthResponse
import com.verindrarizya.ecommerce.data.remote.response.ErrorResponse
import com.verindrarizya.ecommerce.data.remote.response.LoginData
import com.verindrarizya.ecommerce.data.remote.response.LoginResponse
import com.verindrarizya.ecommerce.data.remote.response.ProfileData
import com.verindrarizya.ecommerce.data.remote.response.ProfileResponse

object DummyAuth {

    val registerResponse = AuthResponse(
        code = 200,
        message = "OK",
        data = AuthData(
            accessToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTcyLjE3LjIwLjEwMTo4MDgwLyIsInVzZXJJZCI6IjQ4OGU4YzEwLWUyZTItNDg4Ny1iZDczLTkzZmEzZDI3NDMxMCIsInRva2VuVHlwZSI6ImFjY2Vzc1Rva2VuIiwiZXhwIjoxNjg3MjM0NDYzfQ.kTzuGNAW8ZswT7D3QmJb-BI_n6mPaoev9ZKXH0kqi8ULWLncTkRYeLwwsuYAPoT62nvZFiJCbny2xiMpQDQVug",
            refreshToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTcyLjE3LjIwLjEwMTo4MDgwLyIsInVzZXJJZCI6IjQ4OGU4YzEwLWUyZTItNDg4Ny1iZDczLTkzZmEzZDI3NDMxMCIsInRva2VuVHlwZSI6InJlZnJlc2hUb2tlbiIsImV4cCI6MTY4NzIzNzQ2M30.VMrCKxOG4VeXAFlqv40ftDVeg8YtAWwdYTAy5VUADYPP41_twITxJZQ2dyZf9E9NvCjKbWG6IgTRtOOqjJsoMw",
            expiresAt = 600
        )
    )

    val loginResponse = LoginResponse(
        code = 200,
        message = "OK",
        data = LoginData(
            userName = "",
            userImage = "",
            accessToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTcyLjE3LjIwLjEwMTo4MDgwLyIsInVzZXJJZCI6IjQ4OGU4YzEwLWUyZTItNDg4Ny1iZDczLTkzZmEzZDI3NDMxMCIsInRva2VuVHlwZSI6ImFjY2Vzc1Rva2VuIiwiZXhwIjoxNjg3MjM3MDc0fQ.Gs3Z2T_B5_EpThhcC2p-fiCucP2molqxp95IzwOJdt1XdcOqx0TeCHBrlH1DCtFCL-wm1ysKHNqW6wsLdr6WTg",
            refreshToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlY29tbWVyY2UtYXVkaWVuY2UiLCJpc3MiOiJodHRwOi8vMTcyLjE3LjIwLjEwMTo4MDgwLyIsInVzZXJJZCI6IjQ4OGU4YzEwLWUyZTItNDg4Ny1iZDczLTkzZmEzZDI3NDMxMCIsInRva2VuVHlwZSI6InJlZnJlc2hUb2tlbiIsImV4cCI6MTY4NzI0MDA3NH0.UVPvv-cMJGErtdqQTVgg0aVYHx2r1k1QFozOOWTBGVyznzaN-dmWR-Ul5hg0z0SDEnobpcKP4LGN9Pk4bKNliA",
            expiresAt = 600
        )
    )

    val emailIsTakenResponse = ErrorResponse(
        code = 400,
        message = "Email is already taken"
    )

    val emailOrPasswordNotValid = ErrorResponse(
        code = 400,
        message = "Email or password is not valid"
    )

    val profileResponse = ProfileResponse(
        code = 200,
        message = "OK",
        data = ProfileData(
            userName = "Test",
            userImage = "image"
        )
    )

    val refreshResponse = AuthResponse(
        code = 200,
        message = "OK",
        data = AuthData(
            accessToken = "123",
            refreshToken = "123",
            expiresAt = 600
        )
    )
}
