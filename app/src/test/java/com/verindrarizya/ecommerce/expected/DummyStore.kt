package com.verindrarizya.ecommerce.expected

import com.verindrarizya.ecommerce.data.local.entity.NotificationEntity
import com.verindrarizya.ecommerce.data.remote.response.Product
import com.verindrarizya.ecommerce.data.remote.response.ProductDetail
import com.verindrarizya.ecommerce.data.remote.response.ProductDetailResponse
import com.verindrarizya.ecommerce.data.remote.response.ProductReview
import com.verindrarizya.ecommerce.data.remote.response.ProductReviewResponse
import com.verindrarizya.ecommerce.data.remote.response.ProductVariantItem
import com.verindrarizya.ecommerce.data.remote.response.ProductsData
import com.verindrarizya.ecommerce.data.remote.response.ProductsResponse
import com.verindrarizya.ecommerce.data.remote.response.StoreSearchResponse
import com.verindrarizya.ecommerce.data.remote.response.toCartProductEntity

object DummyStore {

    val searchProductResponse = StoreSearchResponse(
        code = 200,
        message = "OK",
        data = listOf(
            "Lenovo Ideapad Gaming"
        )
    )

    val productsResponse = ProductsResponse(
        code = 200,
        message = "OK",
        data = ProductsData(
            itemsPerPage = 10,
            currentItemCount = 10,
            pageIndex = 1,
            totalPages = 4,
            items = listOf(
                Product(
                    productId = "",
                    productName = "",
                    productPrice = 1,
                    image = "",
                    brand = "",
                    store = "",
                    sale = 1,
                    productRating = 1.0
                )
            )
        )
    )

    val productDetailResponse = ProductDetailResponse(
        code = 200,
        message = "OK",
        data = ProductDetail(
            productId = "1",
            productName = "laptop",
            productPrice = 1,
            image = listOf(
                "gambar"
            ),
            brand = "asus",
            description = "deskripsi",
            store = "asus store",
            sale = 1,
            stock = 1,
            totalRating = 1,
            totalReview = 1,
            totalSatisfaction = 1,
            productRating = 1.0,
            productVariant = listOf(
                ProductVariantItem(
                    variantName = "varian",
                    variantPrice = 0
                )
            )
        )
    )

    val productReview = ProductReviewResponse(
        code = 200,
        message = "OK",
        data = listOf(
            ProductReview(
                userName = "John",
                userImage = "",
                userRating = 1,
                userReview = ""
            )
        )
    )

    val cartProductEntity = productDetailResponse.data.toCartProductEntity()

    val notificationEntity = NotificationEntity(
        id = 1,
        image = "image",
        title = "title",
        time = "time",
        date = "date",
        isRead = false,
        type = "type",
        desc = "desc"
    )
}
